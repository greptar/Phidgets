include $(TOPDIR)/rules.mk

PKG_NAME:=run-firetable
PKG_RELEASE:=1

include $(INCLUDE_DIR)/package.mk

define Package/run-firetable
	SECTION:=utils
	CATEGORY:=Schafer
	TITLE:=Uses an ACCESIO board to fire motors
endef

define Build/Prepare
	mkdir -p $(PKG_BUILD_DIR)
	$(CP) ./src/* $(PKG_BUILD_DIR)/
endef

define Build/Configure
endef

define Build/Compile
	$(MAKE) -C $(PKG_BUILD_DIR) $(TARGET_CONFIGURE_OPTS) run-firetable
	$(MAKE) -C $(PKG_BUILD_DIR) $(TARGET_CONFIGURE_OPTS) matrix-bit
	$(MAKE) -C $(PKG_BUILD_DIR) $(TARGET_CONFIGURE_OPTS) can2udp
	$(MAKE) -C $(PKG_BUILD_DIR) $(TARGET_CONFIGURE_OPTS) hcantool
endef

define Package/run-firetable/install
	$(INSTALL_DIR) $(1)/bin
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/run-firetable $(1)/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/matrix-bit $(1)/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/can2udp $(1)/bin/
	$(INSTALL_BIN) $(PKG_BUILD_DIR)/hcantool $(1)/bin/
endef

$(eval $(call BuildPackage,run-firetable))

