//=============================================================================
// (c) Copyright 2009 Diamond Systems Corporation. Use of this source code
// is subject to the terms of Diamond Systems' Software License Agreement.
// Diamond Systems provides no warranty of proper performance if this
// source code is modified.
//
// File: DSCADSampleInt.c   v1.00
//
// Depedency :	Universal Driver version 6.02
//				DSCUD.H - Include file.
//				DSCUDBCL.LIB - Library file for MS-DOS
//						( USE BC 5.02 compiler )
//				LIBDSCUD_6.02_1.a	- Library file for Linux 2.6.23
//						( USE GCC 3.xx compiler )
//				DSCUD.LIB	- Static library file for Windows XP
//						( USE VC++ 6.0 or Visual Studio 2005 )
//				DSCUD_API.LIB	- Static library for Windows CE 6.0
//						( Use Visual Studio 2005 with Platform Builder 6.0 )
// Description: This sample code demonstrates the usage of UD function
//				dscADSampleInt(). This sample code describes the usage
//				for Helios SBC. The driver will use the incumbent OS
//              in conjunction with the interrupt selected to transfer many
//              values from the A/D converter to user space, and once stopped,
//              will display a single set of those channel values,
//              interpretting them based on the mode given by the user.
// ************************* History ********************
// Version		Engineer		Date		Description
// 2.0                  Nathan Wharton          12/12/11        Schafer version
// 1.0			James Moore		07/31/09	Created.
//=============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>

#include "dscud.h"

#define HELIOS_DEFAULT_BASE_ADDRESS 0x280
#define HELIOS_DEFAULT_IRQ 5
#define HELIOS_VOLTAGE_RANGE RANGE_10
#define FIFO_MIN 8
#define FIFO_MAX 2040
#define FIFO_FACTOR 8
#define FIFO_NOFACTOR_THRESH 48

// command line variables.  Set to example values
static int trigger_channel = 0;
static float trigger_value = 1.85;
static int low_channel = 0;
static int high_channel = 2;
static int hz = 1000;
static int pre_seconds = 1;
static int post_seconds = 12;
static char* sendto_address = "224.0.0.100";
static int sendto_port = 9942;
static int polarity = 0;
static int gain = 0;

// calculated from command line
static unsigned int fifo_size;
static unsigned int num_channels;
static unsigned int num_conversions;
static unsigned int num_scans_in_buffer;
static unsigned int scans_to_send;
static unsigned int scans_behind;
static DSCSAMPLE *scan_buffer;

/*	Possible new header:
        unsigned int packetNumber;
        unsigned int startScan;
        unsigned int scansInBatch;
        unsigned int hz;
        unsigned short channelsInScan;
        unsigned short scansInPacket;
        unsigned short retryCount;
*/

typedef struct PacketHeader
{
	DSCSAMPLE packet_count;
	DSCSAMPLE num_channels;
	DSCSAMPLE num_scans;
} PacketHeader;

typedef struct Packet
{
	PacketHeader header;
	DSCSAMPLE samples[FIFO_MAX];
} Packet;

static Packet packet;
static DSCSAMPLE packet_count = 0;
static int cancel = 0;

static void sighandle(int signum)
{
	cancel = 1;
}

static int irq_count = 0;
void MyUserInterruptFunction(void* param)
{
	irq_count++;
}

static unsigned int send_scans(unsigned int start_scan, unsigned int finish_scan)
{
        static int sd = 0;
	static struct sockaddr_in saddr;
	ssize_t bytes_to_send, sendto_result;

        if (!sd)
        {
                if ((sd = socket(AF_INET,SOCK_DGRAM,0)) <  0)
                {
                        perror("send socket");
                        exit(1);
                }
                memset(&saddr,0,sizeof(saddr));
                saddr.sin_family = AF_INET;
                saddr.sin_addr.s_addr = inet_addr(sendto_address);
                saddr.sin_port = htons(sendto_port);
        }
	if (!sd) return 0;

	packet.header.packet_count = packet_count;
	packet.header.num_channels = num_channels;

	if (finish_scan >= start_scan)
	{
		packet.header.num_scans = finish_scan - start_scan + 1;
		memcpy(packet.samples, scan_buffer + start_scan*num_channels, sizeof(DSCSAMPLE)*(packet.header.num_scans*num_channels));
	}
	else
	{
		packet.header.num_scans = num_scans_in_buffer + finish_scan - start_scan + 1;
		memcpy(packet.samples, scan_buffer + start_scan*num_channels, sizeof(DSCSAMPLE)*(num_scans_in_buffer-start_scan)*num_channels);
		memcpy(packet.samples + (num_scans_in_buffer-start_scan)*num_channels, scan_buffer, sizeof(DSCSAMPLE)*(finish_scan*num_channels));
	}
	bytes_to_send = sizeof(PacketHeader) + sizeof(DSCSAMPLE)*(num_channels*packet.header.num_scans);
	sendto_result = sendto(sd, &packet, bytes_to_send, 0, (struct sockaddr *)&saddr, sizeof saddr);
	if (sendto_result != bytes_to_send)
	{
		perror("sendto");
		exit(1);
	}

	packet_count++;
	return packet.header.num_scans;
}

#if 0
static void resend_packet(int packetNum)
{
	
}
#endif

int main(int argc, char **argv)
{
	BYTE result;                 // returned error code
	DSCB dscb;                   // handle used to refer to the board
	DSCCB dsccb;                 // structure containing board settings (IRQ and Address)
	DSCADSETTINGS dscadsettings; // structure containing A/D conversion settings
	DSCAIOINT dscaioint;         // structure containing interrupt based acquire settings
	DSCS dscs;                   // status of interrupts.
	DFLOAT voltage;
	ERRPARAMS errorParams;       // structure for returning error code and error string
	DSCUSERINTFUNCTION dscuserintfunction;
	struct timeval *current_tv;
	struct timeval *trigger_tv;
	struct timeval *last_tv;
	struct timeval *fifo_tv;
	struct timeval *result_tv;
	int i = 0;                   // miscellaneous counter
	int fifos_filled;

	int bad_command_line = 0;
	int has_looped = 0;
	int has_triggered = 0;
        unsigned int trigger_scan;
	unsigned int last_transfers;
	unsigned int real_transfers = 0;
	//struct timespec req, rem;

        int rd, fromlen;
        char recv_buf[256];
        struct sockaddr_in raddr;
        struct ip_mreq imreq;

	signal(SIGINT, &sighandle);

	current_tv = malloc(sizeof(struct timeval));
	trigger_tv = malloc(sizeof(struct timeval));
	last_tv = malloc(sizeof(struct timeval));
	fifo_tv = malloc(sizeof(struct timeval));
	result_tv = malloc(sizeof(struct timeval));

        //=========================================================================
        // 0. Read Command Line
        //=========================================================================

	if (argc != 12)
	{
		bad_command_line++;
	}
	else
	{
		if (sscanf(argv[1], "%d", &trigger_channel) != 1)
			bad_command_line++;
		else
			if (trigger_channel < -1 || trigger_channel > 15)
				bad_command_line++;
	
		if (sscanf(argv[2], "%f", &trigger_value) != 1)
			bad_command_line++;
		else
			if (trigger_value < -10 || trigger_value > 10)
				bad_command_line++;

        	if (sscanf(argv[3], "%d", &low_channel) != 1)
                	bad_command_line++;
        	else
                	if (low_channel < 0 || low_channel > 15)
                        	bad_command_line++;

        	if (sscanf(argv[4], "%d", &high_channel) != 1)
                	bad_command_line++;
        	else
                	if (high_channel < 0 || high_channel > 15)
                        	bad_command_line++;

        	if (sscanf(argv[5], "%d", &hz) != 1)
                	bad_command_line++;
        	else
                	if (hz < 1 || hz > 1000)
                        	bad_command_line++;

        	if (sscanf(argv[6], "%d", &pre_seconds) != 1)
                	bad_command_line++;
        	else
                	if (pre_seconds < 0 || pre_seconds > 15)
                        	bad_command_line++;

        	if (sscanf(argv[7], "%d", &post_seconds) != 1)
                	bad_command_line++;
        	else
                	if (post_seconds < 0 || post_seconds > 15)
                        	bad_command_line++;

		sendto_address = argv[8];
		if (inet_addr(sendto_address) == INADDR_NONE)
			bad_command_line++;

        	if (sscanf(argv[9], "%d", &sendto_port) != 1)
                	bad_command_line++;
        	else
                	if (sendto_port < 0 || sendto_port > 65534)
                        	bad_command_line++;

		if (sscanf(argv[10], "%d", &polarity) != 1)
			bad_command_line++;
		else
			if (polarity < 0 || polarity > 1)
				bad_command_line++;

                if (sscanf(argv[11], "%d", &gain) != 1)
                        bad_command_line++;
                else
                        if (gain < 0 || gain > 3)
                                bad_command_line++;
	}
	if (bad_command_line)
	{
		fprintf(stderr, "Usage: %s <trigger_channel> <trigger_value> <low_channel> <high_channel> <hz> <seconds_pretrigger> <seconds_posttrigger> <ip_address> <port> <polarity> <gain>\n", argv[0]);
		return 1;
	}

        //=========================================================================
	// 0.1 Calculations from command line
        //=========================================================================
	num_channels = high_channel - low_channel + 1;

	fifo_size = num_channels * hz; // Try to get 1 second per fifo
	if (fifo_size > FIFO_NOFACTOR_THRESH)
	{
		fifo_size -= fifo_size % FIFO_FACTOR; // Make it a multiple of FIFO_FACTOR
		if (fifo_size < FIFO_MIN) fifo_size = FIFO_MIN;
		if (fifo_size > FIFO_MAX) fifo_size = FIFO_MAX;
	}

	fifo_tv->tv_sec = 0;
	fifo_tv->tv_usec = 1000000 * fifo_size / num_channels / hz;
	while (fifo_tv->tv_usec > 1000000) fifo_tv->tv_usec -= 1000000, fifo_tv->tv_sec += 1;

        num_conversions = fifo_size; // Start with at least 1 FIFO
        num_conversions += num_channels * hz * pre_seconds; // Add to it enough to hold pre-trigger data
        num_conversions += (num_conversions % fifo_size) ? (fifo_size - num_conversions % fifo_size) : 0; // Make it a multiple of the fifo size

        while (num_conversions % num_channels != 0)
               num_conversions += fifo_size; // Grow the buffer until it is a multiple of the number of channels

	num_scans_in_buffer = num_conversions / num_channels;
	scans_to_send = hz * (pre_seconds+post_seconds);
	scans_behind = hz * pre_seconds;

	scan_buffer = calloc(num_conversions, sizeof(DSCSAMPLE));
        if (!scan_buffer)
        {
                fprintf(stderr, "Could not allocate memory.\n");
                return 1;
        }
        
	//=========================================================================
	// 0.2 Set Up Multicast Receive
	//=========================================================================

        if ((rd = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP)) <  0) {
                perror("recv socket");
                exit(1);
        }
        memset(&raddr, 0, sizeof(raddr));
        raddr.sin_family = AF_INET;
        raddr.sin_addr.s_addr = htonl(INADDR_ANY);
        raddr.sin_port = htons(sendto_port+1);
        if (bind(rd, (struct sockaddr *)&raddr, sizeof(struct sockaddr_in)) < 0) {
                perror("recv socket");
                exit(1);
        }

        imreq.imr_multiaddr.s_addr = inet_addr(sendto_address);
        imreq.imr_interface.s_addr = INADDR_ANY; // use DEFAULT interface
        // JOIN multicast group on default interface
        if(setsockopt(rd, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                (const void *)&imreq, sizeof(struct ip_mreq)) < 0) {
                        perror("recv socket");
                        exit(1);
        }

        fromlen = sizeof raddr;
	if (fcntl(rd, F_SETFL, O_NONBLOCK))
	{
		perror("Setting nonblock on receiving socket");
		exit(1);
	}

	//=========================================================================
	// I. DRIVER INITIALIZATION
	//
	//    Initializes the DSCUD library.
	//
	//=========================================================================

	if ( dscInit ( DSC_VERSION ) != DE_NONE )
	{
		dscGetLastError(&errorParams);
		fprintf ( stderr, "dscInit error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring );
		return 1;
	}

	//=========================================================================
	// II. BOARD INITIALIZATION
	//
	//	   Initialize the HELIOS board. This function passes the various
	//	   hardware parameters to the driver and resets the hardware.
	//
	//=========================================================================

	dsccb.base_address = HELIOS_DEFAULT_BASE_ADDRESS;
        dsccb.int_level = HELIOS_DEFAULT_IRQ;

	if ( dscInitBoard ( DSC_HELIOS, &dsccb, &dscb ) != DE_NONE )
	{
		dscGetLastError(&errorParams);
		fprintf ( stderr, "dscInitBoard error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring );
		return 1;
	}

	//=========================================================================
	// III. AD SETTINGS INITIALIZATION
	//
	//	    Initialize the structure containing the AD conversion settings and
	//		then pass it to the driver.
	//
	//=========================================================================

	memset(&dscadsettings, 0, sizeof(DSCADSETTINGS));

        // printf ( "Enter the polarity (0 for BIPOLAR, 1 for UNIPOLAR; default: 0): " );
    	dscadsettings.polarity = polarity;
	// The Helios SBC only has a 10V physical range.
	dscadsettings.range = HELIOS_VOLTAGE_RANGE;
	// printf ( "Enter the gain\n(0 for GAIN 1, 1 for GAIN 2, 2 for GAIN 4, 3 for GAIN 8; default: 0): " );
    	dscadsettings.gain = gain;

	dscadsettings.load_cal = 0;
	dscadsettings.current_channel = 0;

	dscadsettings.scan_interval = SCAN_INTERVAL_5;
	dscadsettings.addiff = SINGLE_ENDED;

	if ( ( result = dscADSetSettings ( dscb, &dscadsettings ) ) != DE_NONE )
	{
		dscGetLastError(&errorParams);
		fprintf ( stderr, "dscADSetSettings error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring );
		return 1;
        }

	//=========================================================================
	// IV. I/O INTERRUPT SETTINGS INITIALIZATION
	//
	//	   Initialize the structure containing the analog I/O interrupt
	//	   settings.
	//
	//	   NOTE: You must allocate space for the buffer holding the returned
	//		     sample values. Also, be generous in allocating storage.
	//			 Allocating insufficient memory to hold sample data will result
	//			 in improper behavior of the driver, such as hanging interrupt
	//			 operations or assertion errors.
	//
	//=========================================================================

	memset(&dscaioint, 0, sizeof(DSCAIOINT));

        dscaioint.num_conversions = num_conversions;
    	dscaioint.conversion_rate = hz;
    	dscaioint.cycle = 1;
    	dscaioint.internal_clock  = 1;
    	dscaioint.low_channel = low_channel;
    	dscaioint.high_channel = high_channel;

	dscaioint.external_gate_enable = 0; // can enable it if need be
	dscaioint.internal_clock_gate = 0;   // can enable it if need be
	dscaioint.fifo_enab = 1;

    	dscaioint.fifo_depth = fifo_size;
    	dscaioint.dump_threshold = fifo_size;
        dscaioint.sample_values = scan_buffer;

	//=========================================================================
	// V. SAMPLING AND OUTPUT
	//
	//    Perform the actual sampling and then output the results. To calculate
	//	  the actual input voltages, we must convert the sample code (which
	//	  must be cast to a short to get the correct code) and then plug it
	//	  into one of the formulas located in the manual for your board (under
	//	  "A/D Conversion Formulas").
	//=========================================================================

        if (trigger_channel == -1)
        {
            has_triggered = 1;
            trigger_scan = 0;
        }

	dscuserintfunction.func = (DSCUserInterruptFunction) MyUserInterruptFunction;
	dscuserintfunction.int_mode = USER_INT_AFTER;
	// dscSetUserInterruptFunction(dscb, &dscuserintfunction);

	gettimeofday(current_tv, NULL);
	if ( ( result = dscADScanInt ( dscb, &dscaioint ) ) != DE_NONE )
	{
		dscGetLastError(&errorParams);
		fprintf ( stderr, "dscADScanInt error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring );
		free( scan_buffer ); // remember to deallocate malloc() memory
		return 1;
	}
	dscs.transfers = 0;
	dscs.overflows = 0;
	dscs.op_type = OP_TYPE_INT;

	last_transfers = 0;
	do
       	{
		memcpy(last_tv, current_tv, sizeof(struct timeval));
       		dscSleep(1); // Actually sleeps about 10 ms.
		dscGetStatus(dscb, &dscs);
		gettimeofday(current_tv, NULL);
		if (dscs.overflows)
		{
                    fprintf(stderr, "FIFO overflow.\n");
                    break;
                }

		if (!real_transfers && !dscs.transfers) // Both start at 0.  When a FIFO is first filled, dscs.transfers goes to fifo_size
		    continue; // Haven't got any data at all yet

		// We want real_transfers to be 0 only at beginning before no data.
		// After that when dscs.transfers is zero, we want real_transfers to be
		// dscaioint.num_conversions showing that the end of the buffer was just filled

		real_transfers = dscs.transfers ? dscs.transfers : dscaioint.num_conversions;

		if (real_transfers == last_transfers)
		    continue;

		// At this point we have new data

		if (dscs.transfers == 0)
		    has_looped = 1;

		if (real_transfers > last_transfers)
			fifos_filled = (real_transfers-last_transfers)/fifo_size;
		else
			fifos_filled = (num_conversions+real_transfers-last_transfers)/fifo_size;

		//fprintf(stderr, "Last %d.%06d ", (int)last_tv->tv_sec, (int)last_tv->tv_usec);
		//fprintf(stderr, "Current %d.%06d ", (int)current_tv->tv_sec, (int)current_tv->tv_usec);
		// last_tv is currently the time of the last check to see if there was data.
		// New data came between last_tv and current_tv
		// Now try to set last_tv to time at beginning of first fifo filled this batch
		// First set it to the average of the current time and last time
		timersub(current_tv, last_tv, result_tv);
		result_tv->tv_usec /= 2;
		if (result_tv->tv_sec % 2) result_tv->tv_usec += 500000;
		result_tv->tv_sec /= 2;
		timersub(current_tv, result_tv, last_tv);
		//fprintf(stderr, "Mid %d.%06d ", (int)last_tv->tv_sec, (int)last_tv->tv_usec);
		// Next, back it up fifo_time * fifos_filled
		for (i=0;i<fifos_filled;i++)
		{
			timersub(last_tv, fifo_tv, result_tv);
			memcpy(last_tv, result_tv, sizeof(struct timeval));
		}
		//fprintf(stderr, "Adjusted %d fifos back %d.%06d\n", fifos_filled, (int)last_tv->tv_sec, (int)last_tv->tv_usec);

		if (!has_triggered)
		{
		    if (real_transfers > last_transfers)
                    {
                        for (i = last_transfers ; i < real_transfers; i++)
                        {
			    if (i % num_channels == trigger_channel)
			    {
                                if ( dscADCodeToVoltage ( dscb, dscadsettings, dscaioint.sample_values[i], &voltage ) != DE_NONE)
                                {
                                    dscGetLastError(&errorParams);
                                    fprintf( stderr, "dscADCodeToVoltage error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring );
                                    free ( scan_buffer );
                                    return 1;
                                }
                                if (voltage >= trigger_value)
                                {
                                    has_triggered = 1;
                                    trigger_scan = i / num_channels;
                                    break;
                                }
                                i += num_channels - 1; // This is to speed up the loop and only look at the samples of the trigger channel
                            }
                        }
                    }
                    else
                    {
                        for ( i = last_transfers; i < dscaioint.num_conversions; i++ )
                        {
                            if (i % num_channels == trigger_channel)
                            {
                                if ( dscADCodeToVoltage ( dscb, dscadsettings, dscaioint.sample_values[i], &voltage ) != DE_NONE)
                                {
                                    dscGetLastError(&errorParams);
                                    fprintf( stderr, "dscADCodeToVoltage error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring );
                                    free ( scan_buffer );
                                    return 1;
                                }
                                if (voltage >= trigger_value)
                                {
                                    has_triggered = 1;
                                    trigger_scan = i / num_channels;
                                    break;
                                }
                                i += num_channels - 1; // This is to speed up the loop and only look at the samples of the trigger channel
                            }
                        }
                        if (!has_triggered) for ( i = 0; i < real_transfers; i++)
                        {
                            if (i % num_channels == trigger_channel)
                            {
                                if ( dscADCodeToVoltage ( dscb, dscadsettings, dscaioint.sample_values[i], &voltage ) != DE_NONE)
                                {
                                    dscGetLastError(&errorParams);
                                    fprintf( stderr, "dscADCodeToVoltage error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring );
                                    free ( scan_buffer );
                                    return 1;
                                }
                                if (voltage >= trigger_value)
                                {
                                    has_triggered = 1;
                                    trigger_scan = i / num_channels;
                                    break;
                                }
                                i += num_channels - 1; // This is to speed up the loop and only look at the samples of the trigger channel
                            }
                        }
                    }
                    if (has_triggered)
                    {
			// This will run when trigger scan first found
                        if (!has_looped && trigger_scan < scans_behind)
			{
			    scans_to_send -= scans_behind - trigger_scan;
                            scans_behind = trigger_scan;
			}
			memcpy(trigger_tv, current_tv, sizeof(struct timeval));
			if (trigger_scan >= last_transfers/num_channels)
			{
			    trigger_tv->tv_usec += 1000000 / hz * (trigger_scan - (last_transfers/num_channels));
			}
			else
			{
			    trigger_tv->tv_usec += 1000000 / hz * (trigger_scan + num_scans_in_buffer - (last_transfers/num_channels));
			}
			while (trigger_tv->tv_usec >= 1000000) trigger_tv->tv_usec -= 1000000 , trigger_tv->tv_sec += 1;
			fprintf(stderr, "Adjusted %d fifos back %d.%06d\n", fifos_filled, (int)last_tv->tv_sec, (int)last_tv->tv_usec);
                        fprintf(stderr, "Time of trigger at voltage %f is %d.%06d at scan number %d of %lu\n", voltage,
				(int)trigger_tv->tv_sec,
				(int)trigger_tv->tv_usec,
				trigger_scan, dscaioint.num_conversions / num_channels);
                    }
		}
                if (has_triggered) 
                {
		    static int first_pass = 1;
                    int start_scan = (last_transfers / num_channels) - scans_behind;
                    int finish_scan = (real_transfers / num_channels)-1 - scans_behind;
		    int scans_going_to_send;

		    if (first_pass)
		    {
			first_pass = 0;
			start_scan = trigger_scan - scans_behind;
		    }
		    // fprintf(stderr, "A)Send scans %d to %d\n", start_scan, finish_scan);

		    if (start_scan == num_scans_in_buffer)
			start_scan = 0;
		    else if (start_scan < 0)
			start_scan += num_scans_in_buffer;
		    if (finish_scan < 0)
			finish_scan += num_scans_in_buffer;

		    scans_going_to_send = finish_scan - start_scan + 1;
		    if (scans_going_to_send < 0)
			scans_going_to_send += num_scans_in_buffer;
		    if (scans_going_to_send > scans_to_send)
			finish_scan -= scans_going_to_send - scans_to_send;
		    if (finish_scan < 0)
			finish_scan += num_scans_in_buffer;

		    if (trigger_channel != -1)
			scans_to_send -= send_scans(start_scan, finish_scan);
		    else
			send_scans(start_scan, finish_scan);
                }

                last_transfers = real_transfers;

	}while (dscs.op_type != OP_TYPE_NONE && !cancel && scans_to_send > 0);

	// cancel interrupts manually if interrupts are still running
	if ( dscs.op_type != OP_TYPE_NONE)
	{
		//fprintf(stderr, "Canceling data collection...\n");
		if ( (result = dscCancelOp(dscb)) != DE_NONE)
		{
			dscGetLastError(&errorParams);
			fprintf ( stderr, "dscCancelOp error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring );
			free( scan_buffer ); // remember to deallocate malloc() memory
			return 1;
		}
	}
	sleep(1);
	while (recvfrom(rd, recv_buf, 256, 0, (struct sockaddr *)&raddr, (socklen_t *)&fromlen) > 0)
		fprintf(stderr, "Request for resend\n");

	free( scan_buffer );
	dscFree();

	return 0;
} // end main()
