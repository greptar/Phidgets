#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <string.h>

#include "uStrainBS.h"
#include "sensor-packet.h"

#define RECVLEN 256 // Should be at least twice the biggest packet size

typedef struct NodeBuffer
{
	unsigned short modelNumber;
        unsigned short nodeAddress;
        unsigned char numBufferedPackets;
        unsigned char lastPacketNumber;
        unsigned char sensorCount;
        double **sensorValues;
} NodeBuffer;

typedef struct _MicroStrainBS
{
	int              serialFD;
	int              socketFD;
	struct sockaddr_in socketADDR;
	pthread_t        listenThread;
	int              exitFlag;
	void           (*callBack)(struct timeval timestamp, double *values, int count);
	pthread_mutex_t  mutex;
	NodeBuffer      *nodeBuffers;
	int              numAllNodes;
	int              numLogNodes;
	char           **logNames;
	char           **logUnits;
	double          *logDoubles;
	unsigned char    recv_buf[RECVLEN];
	unsigned short   recv_buf_pos;
	int		 ignoreNewSensors;
} _MicroStrainBS;

typedef struct MessageType
{
	unsigned char startByte;
	int (*matchFunction)(MicroStrainBS us, unsigned char *buffer, int bytesAvailable);
		// return 0 if no match, num bytes if match, -1 if needs more data
} MessageType;

static int pause_and_consume_data(MicroStrainBS uStrainBS)
{
        unsigned char pingCmd  = 0x01;
        unsigned char response = 0x00;

        write(uStrainBS->serialFD, &pingCmd, 1);
        usleep(500000); // Wait a half a second for the data to stop and ping response to happen
        while (read(uStrainBS->serialFD, &response, 1)>0);

        return (response == pingCmd);
}

static int matchReadEEPROM(MicroStrainBS us, unsigned char *buffer, int bytesAvailable)
{
	if (bytesAvailable >= 6)
	{
		if (buffer[1] == 0x00 && buffer[2] == 0x00 && buffer[5] == 0x02)
		{
			if (bytesAvailable >= 12)
			{
				unsigned short i, nodeAddress, modelNumber;
				unsigned char ldCmd[10] = {     0xAA,     //  SOP
								0x05,     //  Message Type
								0x00,     //  Address Mode
								buffer[3],  //  MSB of Node Address
								buffer[4],  //  LSB of Node Address
								0x02,     //  Command Length
								0x00,     //  MSB of Command Byte
								0x38,     //  LSB of Command Byte 38
								(0x3F + buffer[3] + buffer[4]) >> 8,    //  MSB of the checksum
								(0x3F + buffer[3] + buffer[4]) & 0xFF   //  LSB of the checksum
							  };
				nodeAddress = buffer[3];
				nodeAddress = nodeAddress << 8;
				nodeAddress += buffer[4];
				modelNumber = buffer[6];
				modelNumber = modelNumber << 8;
				modelNumber += buffer[7];

				if (us->ignoreNewSensors) return 12;
				for (i=0; i<us->numAllNodes; i++)
					if (us->nodeBuffers[i].modelNumber == modelNumber && us->nodeBuffers[i].nodeAddress == nodeAddress) break;
				if (i == us->numAllNodes)
				{
					us->numAllNodes++;
					us->nodeBuffers = (NodeBuffer *)realloc(us->nodeBuffers, sizeof(NodeBuffer) * us->numAllNodes);
					us->nodeBuffers[i].modelNumber = modelNumber;
					us->nodeBuffers[i].nodeAddress = nodeAddress;
				}
				fprintf(stdout, "EEProm packet of %hu-%04hu\n", modelNumber, nodeAddress);
                                if (pause_and_consume_data(us))
                                {
                                        unsigned char recv=0x00;
                                        write(us->serialFD, ldCmd, 10);
                                        usleep(100000);
                                        read(us->serialFD, &recv, 1);
                                        if (recv == 0xaa)
                                                fprintf(stdout, "LDC initialization of %hu-%04hu\n", modelNumber, nodeAddress);
                                        else
                                                fprintf(stderr, "LDC initialization failure of %hu-%04hu\n", modelNumber, nodeAddress);
                                }
                                else
                                {
                                        fprintf(stderr, "Comm failure trying to start LDC on %hu-%04hu\n", modelNumber, nodeAddress);
                                }
				return bytesAvailable;
			}
			else
			{
				return -1;
			}
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return -1;
	}
}

static int matchDiscovery(MicroStrainBS us, unsigned char *buffer, int bytesAvailable)
{
	if (bytesAvailable >= 6)
	{
		if ((buffer[1] & 0x08) == 0 && buffer[2] == 0x00 && buffer[5] == 0x03)
		{
			if (bytesAvailable >= 13)
			{
				unsigned short i, nodeAddress, modelNumber;
				unsigned char ldCmd[10] = {     0xAA,     //  SOP
								0x05,     //  Message Type
								0x00,     //  Address Mode
								buffer[3],  //  MSB of Node Address
								buffer[4],  //  LSB of Node Address
								0x02,     //  Command Length
								0x00,     //  MSB of Command Byte
								0x38,     //  LSB of Command Byte 38
								(0x3F + buffer[3] + buffer[4]) >> 8,    //  MSB of the checksum
								(0x3F + buffer[3] + buffer[4]) & 0xFF   //  LSB of the checksum
							  };
				nodeAddress = buffer[3];
				nodeAddress = nodeAddress << 8;
				nodeAddress += buffer[4];
				modelNumber = buffer[7];
				modelNumber = modelNumber << 8;
				modelNumber += buffer[8];

				if (us->ignoreNewSensors) return 13;
				for (i=0; i<us->numAllNodes; i++)
					if (us->nodeBuffers[i].modelNumber == modelNumber && us->nodeBuffers[i].nodeAddress == nodeAddress) break;
				if (i == us->numAllNodes)
				{
					us->numAllNodes++;
					us->nodeBuffers = (NodeBuffer *)realloc(us->nodeBuffers, sizeof(NodeBuffer) * us->numAllNodes);
					us->nodeBuffers[i].modelNumber = modelNumber;
					us->nodeBuffers[i].nodeAddress = nodeAddress;
				}
				fprintf(stdout, "Discovery packet of %hu-%04hu\n", modelNumber, nodeAddress);
				if (pause_and_consume_data(us))
				{
					unsigned char recv=0x00;
					write(us->serialFD, ldCmd, 10);
					usleep(100000);
					read(us->serialFD, &recv, 1);
					if (recv == 0xaa)
						fprintf(stdout, "LDC initialization of %hu-%04hu\n", modelNumber, nodeAddress);
					else
						fprintf(stderr, "LDC initialization failure of %hu-%04hu\n", modelNumber, nodeAddress);
				}
				else
				{
					fprintf(stderr, "Comm failure trying to start LDC on %hu-%04hu\n", modelNumber, nodeAddress);
				}
				return bytesAvailable;
			}
			else
			{
				return -1;
			}
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return -1;
	}
}

static int matchLowDutyCycle(MicroStrainBS us, unsigned char *buffer, int bytesAvailable)
{
	static SensorPacketHeader *packet = NULL;
	float *floatBuf;
	unsigned short *ushortBuf;

	if (!packet)
		packet = (SensorPacketHeader *)malloc(sizeof(SensorPacketHeader) + 32);

	floatBuf = (float *)(packet+1);
	ushortBuf = (unsigned short *)(packet+1);

        if (bytesAvailable >= 7)
        {
                if (buffer[1] == 0x07 && buffer[2] == 0x04 && buffer[6] == 0x02)
                {
                        if (bytesAvailable >= 10)
                        {
				int i, neededBytes=0, numChannels=0, bytesPerChannel, dataType;
				for (i=0; i<8; i++)
					if (buffer[7] & (1 << i)) neededBytes++, numChannels++;
				if (buffer[9] == 0x02)
					neededBytes *= 4, bytesPerChannel=4, dataType=1;
				else
					neededBytes *= 2, bytesPerChannel=2, dataType=0;
				neededBytes += 16;
				if (bytesAvailable >= neededBytes)
				{
                                	unsigned short i, nodeAddress;
                                	nodeAddress = buffer[3];
                                	nodeAddress = nodeAddress << 8;
                                	nodeAddress += buffer[4];
	
                                	for (i=0; i<us->numAllNodes; i++)
                                        	if (us->nodeBuffers[i].nodeAddress == nodeAddress) break;
                                	if (i == us->numAllNodes)
                                	{
						unsigned char readCmd[12]= {0xaa, 0x05, 0x00,
									buffer[3],  //  MSB of the node address
									buffer[4],  //  LSB of the node address
									0x04, 0x00, 0x03, 0x00, 0x70,
									(buffer[3] + buffer[4] + 0x7c)>> 8,  //  MSB of checksum
									(buffer[3] + buffer[4] + 0x7c)& 0xFF //  LSB of checksum
									};
						unsigned char stopCmd[10]= {0xaa, 0xfe, 0x00,
									buffer[3],  //  MSB of the node address
									buffer[4],  //  LSB of the node address
									0x02, 0x00, 0x90,
									(buffer[3] + buffer[4] + 0x190)>> 8,  //  MSB of checksum
									(buffer[3] + buffer[4] + 0x190)& 0xFF //  LSB of checksum
									};
						if (us->ignoreNewSensors) return neededBytes;
                                        	fprintf(stdout, "%d byte LDC of undiscovered node address %04hu.\n", neededBytes, nodeAddress);
                                		if (pause_and_consume_data(us))
                                		{
                                        		unsigned char recv=0x00;
                                        		unsigned char recv2=0x00;
                                        		write(us->serialFD, stopCmd, 10);
                                        		while(read(us->serialFD, &recv, 1)<1);
                                        		if (recv == 0xaa)
                                                		fprintf(stdout, "Stop Request of %04hu\n", nodeAddress);
                                        		else
                                                		fprintf(stderr, "Stop Request failure of %04hu\n", nodeAddress);
							while(read(us->serialFD, &recv, 1) < 1);
							while(read(us->serialFD, &recv2, 1) < 1);
							if (recv == 0x90 && recv2 == 0x01)
								fprintf(stdout, "Stop Success of %04hu\n", nodeAddress);
							else
								fprintf(stderr, "Stop Failure of %04hu\n", nodeAddress);

							write(us->serialFD, readCmd, 12);
							while(read(us->serialFD, &recv, 1)<1);
							if (recv == 0xaa)
								fprintf(stdout, "ReadEEProm Request of %04hu\n", nodeAddress);
							else
								fprintf(stderr, "ReadEEProm Request failure of %04hu\n", nodeAddress);
                                		}
                                		else
                                		{
                                        		fprintf(stderr, "Comm failure trying to start LDC on %04hu\n", nodeAddress);
                                		}
						return bytesAvailable;
                                	}
                                	else
					{
						unsigned char j, k=12, l=0;
						struct timeval tv;

						gettimeofday(&tv, NULL);
						packet->time_sec = tv.tv_sec;
						packet->time_usec = tv.tv_usec;
						packet->packet_type = SP_TYPE_USTRAIN;
						packet->packet_cmd = SP_CMD_DATA;
						packet->packet_num = 0;
						packet->num_buffered_packets = 30;
						packet->serialNumber = us->nodeBuffers[i].modelNumber * 10000 + nodeAddress;
						packet->deviceClass = us->nodeBuffers[i].modelNumber;
						packet->channelMask = 0;
						packet->channelMask = packet->channelMask | buffer[7];
						packet->dataType = dataType;
						packet->dataSize = bytesPerChannel;
						packet->dataLength = bytesPerChannel * numChannels;

						for (j=0; j<8; j++)
						{
							if (buffer[7] & (1 << j))
							{
								if (buffer[9] == 0x01)
								{
									ushortBuf[l] = ((buffer[k] << 8) + buffer[k+1]) >> 1;
									k+=2;
								}
								else if (buffer[9] == 0x02)
								{
									unsigned char fbuf[4];
									float *f = (float *)fbuf;
									// Hard coded for little endian here
									fbuf[0] = buffer[k+3];
									fbuf[1] = buffer[k+2];
									fbuf[2] = buffer[k+1];
									fbuf[3] = buffer[k];
									floatBuf[l] = *f;
									k+=4;
								}
								else if (buffer[9] == 0x03)
								{
									ushortBuf[l] = ((buffer[k] << 8) + buffer[k+1]);
									k+=2;
								}
								else
								{
									ushortBuf[l] = 0;
									k+=2;
								}
								l++;
							}
						}
						sendto(us->socketFD, packet, sizeof(SensorPacketHeader) + packet->dataLength, 0,
							(struct sockaddr *)&us->socketADDR, sizeof(us->socketADDR));
						return neededBytes;
					}
				}
				else
				{
					return -1;
				}
                        }
                        else
                        {
                                return -1;
                        }
                }
                else
                {
                        return 0;
                }
        }
        else
        {
                return -1;
        }
}

static int matchXBeeSampleRx(MicroStrainBS us, unsigned char *buffer, int bytesAvailable)
{
	static SensorPacket *packet = NULL;
	struct timeval tv;

	gettimeofday(&tv, NULL);
	if (!packet)
	{
		packet = (SensorPacket *)malloc(sizeof(SensorPacket));
		if (!packet) return 0;
		packet->header.packet_type = SP_TYPE_XBEE;
		packet->header.packet_cmd = SP_CMD_DATA;
		packet->header.packet_num = 0;
		packet->header.num_buffered_packets = 30;
		packet->header.deviceClass = 0;
		packet->header.dataType = 1;
		packet->header.dataSize = 4;
	}
	if (bytesAvailable >= 4)
	{
		if (buffer[3] == 0x92)
		{
			int neededBytes = 256 * buffer[1] + buffer[2] + 4;
			if (bytesAvailable >= neededBytes)
			{
				unsigned int nodeAddress = buffer[8] << 24 | buffer[9] << 16 | buffer[10] << 8 | buffer[11];
				unsigned int i, j=0, numD=0, numA=0;
				
				for (i=0; i<8; i++) if (buffer[16] & (1<<i)) numD++;
				for (i=0; i<8; i++) if (buffer[17] & (1<<i)) numD++;
				for (i=0; i<8; i++) if (buffer[18] & (1<<i)) numA++;

				packet->header.time_sec = tv.tv_sec;
				packet->header.time_usec = tv.tv_usec;
				packet->header.serialNumber = nodeAddress;
				packet->header.channelMask = buffer[18];
				packet->header.dataLength = numA * packet->header.dataSize;

				for (i = 19 + 2 * numD; i < neededBytes - 1; i += 2)
				{
					unsigned short bits = buffer[i] << 8 | buffer[i+1];
					float volts = (bits * 1.2) / 1023;
					float temperatureCelsius = (volts - 0.5) * 100;
					temperatureCelsius *= 3.3 / 2.7;
					packet->data.floats[j] = temperatureCelsius;
					j++;
				}
				sendto(us->socketFD, packet, sizeof(SensorPacketHeader) + packet->header.dataLength, 0,
					(struct sockaddr *)&us->socketADDR, sizeof(us->socketADDR));
				return neededBytes;
			}
			else
			{
				return -1;
			}
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return -1;
	}
}

static MessageType messageTypes[] =
{
	{0xAA, matchLowDutyCycle},
	{0xAA, matchDiscovery},
	{0xAA, matchReadEEPROM},
	{0x7E, matchXBeeSampleRx},
	{0x00, NULL},
};

static ssize_t parseBuffer(MicroStrainBS us)
{
	ssize_t i;

	for (i=0; i<us->recv_buf_pos; )
	{
		int j, messageTooLongFound=0;
		for (j=0; messageTypes[j].startByte; j++)
		{
			if (us->recv_buf[i] == messageTypes[j].startByte)
			{
				int matchResult = messageTypes[j].matchFunction(us, us->recv_buf + i, us->recv_buf_pos - i);
				if (matchResult > 0)
				{
					i += matchResult;
					break;
				}
				else if (matchResult < 0)
				{
					messageTooLongFound = 1;
				}
			}
		}
		if (!messageTypes[j].startByte) // Gone through all message types and i not incremented
					        // so either need to increment i or break
		{
			if (messageTooLongFound)
			{
				break;
			}
			else
			{
				fprintf(stderr, "skip %02x\n", us->recv_buf[i]);
				i++;
			}
		}
	}

	return i;
}

static void *listenThread(void *arg)
{
        MicroStrainBS us = (MicroStrainBS)arg;
	ssize_t i, numBytes, bytesParsed;

        while (!us->exitFlag)
        {
		usleep(10000);
		pthread_mutex_lock(&us->mutex);
		if ((numBytes = read(us->serialFD, us->recv_buf + us->recv_buf_pos, RECVLEN - us->recv_buf_pos)) > 0)
		{
			us->recv_buf_pos += numBytes;
			bytesParsed = parseBuffer(us);
			if (bytesParsed)
				for (i=0; i<(us->recv_buf_pos - bytesParsed); i++)
					us->recv_buf[i] = us->recv_buf[i+bytesParsed];
			us->recv_buf_pos -= bytesParsed;
		}
		if (us->recv_buf_pos >= RECVLEN) // so long we have to drop it
		{
			fprintf(stderr, "Data Dropped:\n");
			for (i=0; i<RECVLEN; i++)
				fprintf(stderr, "%02x:", us->recv_buf[i]);
			fprintf(stderr, "\n");
			us->recv_buf_pos = 0;
		}
		pthread_mutex_unlock(&us->mutex);
	}
	return NULL;
}

MicroStrainBS MicroStrainBS_create(char *deviceName, char *ipAddress, unsigned short port)
{
        MicroStrainBS us = (MicroStrainBS)malloc(sizeof(_MicroStrainBS));
        us->exitFlag = 0;
        us->callBack = NULL;
        pthread_mutex_init(&us->mutex, NULL);
        us->nodeBuffers = NULL;
        us->numAllNodes = 0;
        us->numLogNodes = 0;
        us->logNames = NULL;
        us->logUnits = NULL;
        us->logDoubles = NULL;
	us->recv_buf_pos = 0;
	us->ignoreNewSensors = 0;
        if ((us->serialFD = open(deviceName, O_RDWR | O_NOCTTY | O_NONBLOCK)) <  0)
        {
                perror("MicroStrainBS_create:Opening Device");
                free(us);
                return NULL;
        }
	if ((us->socketFD = socket(AF_INET,SOCK_DGRAM,0)) <  0)
	{
		perror("Creating UDP Socket");
		free(us);
		return NULL;
	}
	memset(&us->socketADDR,0,sizeof(us->socketADDR));
	us->socketADDR.sin_family = AF_INET;
	us->socketADDR.sin_addr.s_addr = inet_addr(ipAddress);
	us->socketADDR.sin_port = htons(port);

	if (!pause_and_consume_data(us))
	{
		fprintf(stderr, "Don't see a Microstrain Base Station on %s. Might be an XBee.\n", deviceName);
	}
        if (pthread_create(&us->listenThread, NULL, listenThread, us) != 0)
        {
                perror("MicroStrainBS_create:Creating Thread");
                free(us);
                return NULL;
        }
        return us;
}

void MicroStrainBS_destroy(MicroStrainBS ustrain)
{
        ustrain->exitFlag = 1;
        pthread_join(ustrain->listenThread, NULL);
        close(ustrain->serialFD);
        // Need to free memory inside
        free(ustrain);
}

void MicroStrainBS_allowNewSensors(MicroStrainBS ustrain)
{
        pthread_mutex_lock(&ustrain->mutex);
        ustrain->ignoreNewSensors = 0;
        pthread_mutex_unlock(&ustrain->mutex);
}

void MicroStrainBS_ignoreNewSensors(MicroStrainBS ustrain)
{
	pthread_mutex_lock(&ustrain->mutex);
	ustrain->ignoreNewSensors = 1;
	pthread_mutex_unlock(&ustrain->mutex);
}
