#ifndef LASER_POSITION_H
#define LASER_POSITION_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _Laser_Position *Laser_Position;

Laser_Position Laser_Position_create(char *ipAddress, unsigned short port);
void           Laser_Position_send(Laser_Position laser_position, float m1, float m2, float m3, float m4);
void           Laser_Position_destroy(Laser_Position laser_position);

#ifdef __cplusplus
}
#endif

#endif
