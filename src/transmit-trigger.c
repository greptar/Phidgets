//=============================================================================
// (c) Copyright 2009 Diamond Systems Corporation. Use of this source code
// is subject to the terms of Diamond Systems' Software License Agreement.
// Diamond Systems provides no warranty of proper performance if this
// source code is modified.
//
// File: DSCADSampleInt.c   v1.00
//
// Depedency :	Universal Driver version 6.02
//				DSCUD.H - Include file.
//				DSCUDBCL.LIB - Library file for MS-DOS
//						( USE BC 5.02 compiler )
//				LIBDSCUD_6.02_1.a	- Library file for Linux 2.6.23
//						( USE GCC 3.xx compiler )
//				DSCUD.LIB	- Static library file for Windows XP
//						( USE VC++ 6.0 or Visual Studio 2005 )
//				DSCUD_API.LIB	- Static library for Windows CE 6.0
//						( Use Visual Studio 2005 with Platform Builder 6.0 )
// Description: This sample code demonstrates the usage of UD function
//				dscADSampleInt(). This sample code describes the usage
//				for Helios SBC. The driver will use the incumbent OS
//              in conjunction with the interrupt selected to transfer many
//              values from the A/D converter to user space, and once stopped,
//              will display a single set of those channel values,
//              interpretting them based on the mode given by the user.
// ************************* History ********************
// Version		Engineer		Date		Description
// 2.0                  Nathan Wharton          12/12/11        Schafer version
// 1.0			James Moore		07/31/09	Created.
//=============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <signal.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "dscud.h"
#include "sensor-packet.h"

#define HELIOS_DEFAULT_BASE_ADDRESS 0x280
#define HELIOS_DEFAULT_IRQ 5
#define HELIOS_VOLTAGE_RANGE RANGE_10

#define HELIOS_HZ 10000
#define HELIOS_FIFO_SIZE 2000 // 200 ms time length of fifo

static int trigger_channel = 0;
static float trigger_value = 2.5;
static char *sendto_address = "127.0.0.1";
static int sendto_port=1027;
static int socketFD;
static struct sockaddr_in socketADDR;
static TransmitPhidgetsHeader packet;

static int cancel = 0;
static void sighandle(int signum)
{
	cancel = 1;
}

static DSCB dscb;                   // handle used to refer to the board
static DSCADSETTINGS dscadsettings; // structure containing A/D conversion settings
static DSCAIOINT dscaioint;         // structure containing interrupt based acquire settings

static void MyUserInterruptFunction(void* param)
{
	DFLOAT voltage;
	ERRPARAMS errorParams;       // structure for returning error code and error string
	struct timeval irq_tv, trigger_tv;
	int i;
	static int has_triggered = 0;

	gettimeofday(&irq_tv, NULL);
	irq_tv.tv_usec -= 1000000 / HELIOS_HZ * HELIOS_FIFO_SIZE + 4353;
	while (irq_tv.tv_usec < 0) irq_tv.tv_usec += 1000000 , irq_tv.tv_sec -= 1;
        for (i = 0 ; i < HELIOS_FIFO_SIZE; i++)
        {
                if (dscADCodeToVoltage(dscb, dscadsettings, dscaioint.sample_values[i], &voltage) != DE_NONE)
                {
                        dscGetLastError(&errorParams);
                        fprintf(stderr, "dscADCodeToVoltage error: %s %s\n",
				dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
			cancel = 1;
                }
		else
		{
                	if (has_triggered)
                	{
                        	if (voltage < trigger_value)
                        	{
                                	has_triggered = 0;
                        	}
                	}
                	else
                	{
                        	if (voltage >= trigger_value)
                        	{
                                	has_triggered = 1;
					trigger_tv = irq_tv;
                                	trigger_tv.tv_usec += 1000000 / HELIOS_HZ * i;
                                	while  (trigger_tv.tv_usec >= 1000000)
						trigger_tv.tv_usec -= 1000000 , trigger_tv.tv_sec += 1;
                                	// fprintf(stdout, "Trigger scan % 6d at time %d.%06d\n", i, (int)trigger_tv.tv_sec, (int)trigger_tv.tv_usec);
					packet.time = trigger_tv;
					sendto(socketFD, &packet, sizeof(TransmitPhidgetsHeader), 0, (struct sockaddr *)&socketADDR, sizeof(socketADDR));
                        	}
                	}
		}
        }
}

int main(int argc, char **argv)
{
	DSCCB dsccb;                 // structure containing board settings (IRQ and Address)
	ERRPARAMS errorParams;       // structure for returning error code and error string
	DSCUSERINTFUNCTION dscuserintfunction;
	DSCSAMPLE *scan_buffer;
	int bad_command_line = 0;

	signal(SIGINT, &sighandle);

        //=========================================================================
        // 0. Read Command Line
        //=========================================================================

	if (argc != 4)
	{
		bad_command_line++;
	}
	else
	{
		if (sscanf(argv[1], "%d", &trigger_channel) != 1)
			bad_command_line++;
		else
			if (trigger_channel < -1 || trigger_channel > 15)
				bad_command_line++;
	
		if (sscanf(argv[2], "%f", &trigger_value) != 1)
			bad_command_line++;
		else
			if (trigger_value < -10 || trigger_value > 10)
				bad_command_line++;

                sendto_address = argv[3];
                if (inet_addr(sendto_address) == INADDR_NONE)
                        fprintf(stderr, "Bad IP Address\n"), bad_command_line++;
	}
	if (bad_command_line)
	{
		fprintf(stderr, "Usage: %s <trigger_channel> <trigger_value> <IP>\n", argv[0]);
		return 1;
	}

        //=========================================================================
	// 0.1 Calculations from command line
        //=========================================================================
	scan_buffer = calloc(HELIOS_FIFO_SIZE, sizeof(DSCSAMPLE));
        if (!scan_buffer)
        {
                fprintf(stderr, "Could not allocate memory.\n");
                return 1;
        }
        if ((socketFD = socket(AF_INET,SOCK_DGRAM,0)) <  0)
        {
                perror("Creating UDP Socket");
                exit(1);
        }
        memset(&socketADDR,0,sizeof(socketADDR));
        socketADDR.sin_family = AF_INET;
        socketADDR.sin_addr.s_addr = inet_addr(sendto_address);
        socketADDR.sin_port = htons(sendto_port);

	packet.packet_type = TP_TYPE_PULSE_TIME;
	packet.packet_subtype = 0;
	packet.packet_num = 0;
	packet.num_buffered_packets = 1;
	packet.deviceClass = 0;
	packet.dataLength = 0;
        
	//=========================================================================
	// I. DRIVER INITIALIZATION
	//
	//    Initializes the DSCUD library.
	//
	//=========================================================================

	if (dscInit(DSC_VERSION) != DE_NONE)
	{
		dscGetLastError(&errorParams);
		fprintf(stderr, "dscInit error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
		return 1;
	}

	//=========================================================================
	// II. BOARD INITIALIZATION
	//
	//	   Initialize the HELIOS board. This function passes the various
	//	   hardware parameters to the driver and resets the hardware.
	//
	//=========================================================================

	dsccb.base_address = HELIOS_DEFAULT_BASE_ADDRESS;
        dsccb.int_level = HELIOS_DEFAULT_IRQ;

	if (dscInitBoard(DSC_HELIOS, &dsccb, &dscb) != DE_NONE)
	{
		dscGetLastError(&errorParams);
		fprintf(stderr, "dscInitBoard error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
		return 1;
	}

	//=========================================================================
	// III. AD SETTINGS INITIALIZATION
	//
	//	    Initialize the structure containing the AD conversion settings and
	//		then pass it to the driver.
	//
	//=========================================================================

	memset(&dscadsettings, 0, sizeof(DSCADSETTINGS));

        // printf ( "Enter the polarity (0 for BIPOLAR, 1 for UNIPOLAR; default: 0): " );
    	dscadsettings.polarity = 0;
	// The Helios SBC only has a 10V physical range.
	dscadsettings.range = HELIOS_VOLTAGE_RANGE;
	// printf ( "Enter the gain\n(0 for GAIN 1, 1 for GAIN 2, 2 for GAIN 4, 3 for GAIN 8; default: 0): " );
    	dscadsettings.gain = 0;

	dscadsettings.load_cal = 1;
	dscadsettings.current_channel = 0;

	dscadsettings.scan_interval = SCAN_INTERVAL_20;
	//dscadsettings.addiff = SINGLE_ENDED;
	dscadsettings.addiff = DIFFERENTIAL;

	if (dscADSetSettings(dscb, &dscadsettings) != DE_NONE)
	{
		dscGetLastError(&errorParams);
		fprintf(stderr, "dscADSetSettings error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
		return 1;
        }

	//=========================================================================
	// IV. I/O INTERRUPT SETTINGS INITIALIZATION
	//
	//	   Initialize the structure containing the analog I/O interrupt
	//	   settings.
	//
	//	   NOTE: You must allocate space for the buffer holding the returned
	//		     sample values. Also, be generous in allocating storage.
	//			 Allocating insufficient memory to hold sample data will result
	//			 in improper behavior of the driver, such as hanging interrupt
	//			 operations or assertion errors.
	//
	//=========================================================================

	memset(&dscaioint, 0, sizeof(DSCAIOINT));

        dscaioint.num_conversions = HELIOS_FIFO_SIZE;
    	dscaioint.conversion_rate = HELIOS_HZ;
    	dscaioint.cycle = 1;
    	dscaioint.internal_clock  = 1;
    	dscaioint.low_channel = trigger_channel;
    	dscaioint.high_channel = trigger_channel;

	dscaioint.external_gate_enable = 0; // can enable it if need be
	dscaioint.internal_clock_gate = 0;   // can enable it if need be
	dscaioint.fifo_enab = 1;

    	dscaioint.fifo_depth = HELIOS_FIFO_SIZE;
    	dscaioint.dump_threshold = HELIOS_FIFO_SIZE;
        dscaioint.sample_values = scan_buffer;

	//=========================================================================
	// V. SAMPLING AND OUTPUT
	//
	//    Perform the actual sampling and then output the results. To calculate
	//	  the actual input voltages, we must convert the sample code (which
	//	  must be cast to a short to get the correct code) and then plug it
	//	  into one of the formulas located in the manual for your board (under
	//	  "A/D Conversion Formulas").
	//=========================================================================

	dscuserintfunction.func = (DSCUserInterruptFunction) MyUserInterruptFunction;
	dscuserintfunction.int_mode = USER_INT_AFTER;
	dscSetUserInterruptFunction(dscb, &dscuserintfunction);

	if (dscADScanInt(dscb, &dscaioint) != DE_NONE)
	{
		dscGetLastError(&errorParams);
		fprintf(stderr, "dscADScanInt error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
		cancel = 1;
	}

	if (!cancel)
	{
		while (!cancel) dscSleep(100);
	}

	free(scan_buffer);
	dscFree();
	return 0;
}
