#ifndef LEICA_H
#define LEICA_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _Leica *Leica;

Leica Leica_create(char *deviceTTY, char *ipAddress, unsigned short port);
float Leica_lastread(Leica leica);
void  Leica_destroy(Leica leica);

#ifdef __cplusplus
}
#endif

#endif
