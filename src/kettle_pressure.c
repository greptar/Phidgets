#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>

static char linebuf[8]; /* 0.00V\r\n\0 */

static void get_current_voltage(void)
{
        static int fd=-1;
        float v;
        int numread = 0;
        linebuf[7] = 0;
        if (fd == -1)
        {
                fd=open("/dev/ttyUSB0", O_RDWR);
                if (fd == -1)
                {
                        fprintf(stderr, "Cannot open /dev/ttyUSB0\n");
                        exit(1);
                }
        }
        write(fd, "Z", 1);
        while (numread < 7)
        {
                int ret = read(fd, linebuf + numread, 7 - numread);
                if (ret == -1)
                {
                        perror("Reading from /dev/ttyUSB0");
                        exit(1);
                }
                else
                {
                        numread += ret;
                }
        }
        if (sscanf(linebuf, "%f", &v) == 1)
        {
		linebuf[5]=0;
                return;
        }
        else
        {
                fprintf(stderr, "Cannot understand data :%s: from /dev/ttyUSB0\n", linebuf);
                exit(1);
        }
	return;
}

int main(int argc, char **argv)
{
	char buf[128];
	system("stty -F /dev/ttyUSB0 115200 raw -echo");
	int inc=0;
	while (1)
	{
		get_current_voltage();
		sprintf(buf, "%s%d\n", linebuf, inc++);
		write(1, buf, strlen(buf));
		usleep(500000);
	}
	return 0;
}
