#include <stdio.h>
#include <stdlib.h>
#include <math.h> /* exp, signbit */
#define NUM_RINGS 20
#define DITS_PER_RING 6
#define MAX_LINE_LENGTH 128

static int motor_presence[NUM_RINGS][DITS_PER_RING];

// outputs are Ring_Fired and DitFired
static void GetYawCmd(double PsiE[3], double Q[3], int *RingFired, int *DitFired)
{

int solution_found = 0;

// The moment produced by each Dit "Ring":
static double Ms[] = {  -0.001483995,       /* Ring 1 */
                     -0.001327785,
                     -0.001171575,
                     -0.001015365,
                     -0.000859155,
                     -0.000702945,
                     -0.000546735,
                     -0.000390525,
                     -0.000234315,
                     -0.000078105,       /* Ring 10 */
                      0.000078105,       /* Ring 11 */
                      0.000234315,
                      0.000390525,
                      0.000546735,
                      0.000702945,
                      0.000859155,
                      0.001015365,
                      0.001171575,
                      0.001327785,
                      0.001483995  };    // Ring 20
    
    double I = 0.8;          /* Moment of Inertia */
    double T = 0.01;         /* 10 milliseconds */
    double Tau = 0.05;       // Primary Time Constant
    double Lau = 0.5;        // Secondary Time Constant

    double p = -2*exp(-T/Tau) - exp(-T/Lau);
    double q = exp(-2*T/Tau) + 2*exp(-T/Tau)*exp(-T/Lau);
    double r = -exp(-2*T/Tau)*exp(-T/Lau);

    double b = (p - q + r + 3)/4;
    double K = p - b + 2;
    double a = (r - b)/K;
    double Kc = 2*K*I/(T);

    double YTD = (-b*Q[1] + Kc*(PsiE[0] + a*PsiE[1]));     // Yaw Torque-Secs Desired
    
    // Outputs are left at zero if no fireable DITs found.
    *RingFired = 0;
    *DitFired = 0;

   
     if (YTD > 0)                  // Need Positive Torque
     {
        int RingCount = NUM_RINGS;
        while (!solution_found && (RingCount > NUM_RINGS/2))
        {
            if (Ms[RingCount-1] <= YTD)   // Found a potential DIT to fire
            {
                int dit = 0;
                for (dit = 0 ; dit < DITS_PER_RING; dit++)
                {
                    if ((motor_presence[RingCount-1][dit]) && !solution_found)
                    {
                        *RingFired = RingCount;
                        *DitFired = dit + 1;
                        solution_found = 1;
						break;
                    }
                }
            }
            RingCount = RingCount - 1;
        }
     }
    else                        // Need Negative Torque
    {
        int RingCount = 1;
        while (!solution_found && (RingCount <= NUM_RINGS/2))
        {
            if (Ms[RingCount-1] >= YTD)
            {
                int dit = 0;
                for (dit = 0 ; dit < DITS_PER_RING; dit++)
                {
                    if ((motor_presence[RingCount-1][dit]) && !solution_found)
                    {
                        *RingFired = RingCount;
                        *DitFired = dit + 1;
                        solution_found = 1;
						break;
                    }
                }
            }
            RingCount = RingCount + 1;
        }
    }
}

int main(int argc, char **argv)
{
	int i,j;
	char linebuf[MAX_LINE_LENGTH];

	for (i=0;i<NUM_RINGS;i++) for (j=0;j<DITS_PER_RING;j++) motor_presence[i][j]=1;
	while (fgets(linebuf, MAX_LINE_LENGTH, stdin))
	{
		double PsiE[3], Q[3];
		int RingFired, DitFired;
		if (sscanf(linebuf, "[ %lf %lf %lf ][ %lf %lf %lf ]", PsiE, PsiE+1, PsiE+2, Q, Q+1, Q+2)!= 6)
		{
			for (i=0;i<NUM_RINGS;i++) for (j=0;j<DITS_PER_RING;j++) motor_presence[i][j]=1;
			fprintf(stdout, "[ 0 0 ]\n");
		}
		else
		{
			GetYawCmd(PsiE,Q,&RingFired,&DitFired);
			fprintf(stdout, "[ %d %d ]\n", RingFired, DitFired);
			if (RingFired>0 && DitFired>0)
				motor_presence[RingFired-1][DitFired-1]=0;
		}
	}
	return 0;
}
