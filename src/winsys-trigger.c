//*****************************************************************************
//	
//	Copyright 2011 by WinSystems Inc.
//
//	Permission is hereby granted to the purchaser of WinSystems GPIO cards 
//	and CPU products incorporating a GPIO device, to distribute any binary 
//	file or files compiled using this source code directly or in any work 
//	derived by the user from this file. In no case may the source code, 
//	original or derived from this file, be distributed to any third party 
//	except by explicit permission of WinSystems. This file is distributed 
//	on an "As-is" basis and no warranty as to performance or fitness of pur-
//	poses is expressed or implied. In no case shall WinSystems be liable for 
//	any direct or indirect loss or damage, real or consequential resulting 
//	from the usage of this source code. It is the user's sole responsibility 
//	to determine fitness for any considered purpose.
//
//*****************************************************************************
//
//	Name	 : poll.c
//
//	Project	 : PCM-VDX GPIO Sample Application Program
//
//	Author	 : Paul DeMetrotion
//
//*****************************************************************************
//
//	  Date		Revision	                Description
//	--------	--------	---------------------------------------------
//	05/11/11	  1.00		Original Release
//
//*****************************************************************************

// This program demonstrates one manner in which an unprivledged 
//   application running in user space can sychronize to hardware events 
//   handled by a device driver running in Kernel space.

#include <stdio.h>
#include <fcntl.h>      /* open */ 
#include <unistd.h>     /* exit */
#include <sys/ioctl.h>  /* ioctl */
#include <stdlib.h>
#include <pthread.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h>

#include "pcmvdxgpio.h"

#include "sensor-packet.h"

static int trigger_channel = 0;
static char *sendto_address = "127.0.0.1";
static int sendto_port=1027;
static int socketFD;
static struct sockaddr_in socketADDR;
static SensorPacketHeader packet;

// These two functions will be sub-processes using the Posix threads 
// capability of Linux. These two threads will simulate a type of 
// interrupt service routine in that they will start up and then suspend 
// until an interrupt occurs and the driver awakens them.
void *thread_function(void *arg);

// Event count, counts the number of events we've handled
volatile int event_count;
volatile int exit_flag = 0;

char line[80];

static int cancel = 0;
static void sighandle(int signum)
{
        cancel = 1;
}

int main(int argc, char *argv[])
{
	int intr, res, bad_command_line=0;
	unsigned int BitNo;
	pthread_t a_thread;

	signal(SIGINT, &sighandle);

        if (argc != 3)
        {
                bad_command_line++;
        }
        else
        {
                if (sscanf(argv[1], "%d", &trigger_channel) != 1)
                        bad_command_line++;
                else
                        if (trigger_channel < -1 || trigger_channel > 7)
                                bad_command_line++;

                sendto_address = argv[2];
                if (inet_addr(sendto_address) == INADDR_NONE)
                        fprintf(stderr, "Bad IP Address\n"), bad_command_line++;
        }
        if (bad_command_line)
        {
                fprintf(stderr, "Usage: %s <trigger_channel> <IP>\n", argv[0]);
                return 1;
        }

	BitNo = trigger_channel;

        if ((socketFD = socket(AF_INET,SOCK_DGRAM,0)) <  0)
        {
                perror("Creating UDP Socket");
                exit(1);
        }
        memset(&socketADDR,0,sizeof(socketADDR));
        socketADDR.sin_family = AF_INET;
        socketADDR.sin_addr.s_addr = inet_addr(sendto_address);
        socketADDR.sin_port = htons(sendto_port);

        packet.packet_type = SP_TYPE_PULSE;
        packet.packet_cmd = SP_CMD_DATA;
        packet.packet_num = 0;
        packet.num_buffered_packets = 1;
        packet.deviceClass = 0;
        packet.dataLength = 0;

	// test for availability
	if(ioctl_read_bit(0, 1) < 0) 
	{
	    fprintf(stderr, "Can't access device PCM-VDX - Aborting\n");
	    exit(1);
	}

	// configure ports
	ioctl_set_port_dir(0, 0xFF);		// port 0 all output
	ioctl_clr_bit(0, BitNo);			// make sure BitNo is low
	ioctl_set_port_dir(1, 0);			// port 1 all input

	// enable Port 1 - BitNo  for rising edge interrupts
	ioctl_enab_int(1, BitNo, RISING);
	
	// make sure they're ready and armed by explicitly 
	// calling the ioctl_clr_int() function.
	ioctl_clr_int(1, BitNo);

	// clear out any events that are queued up within the 
    // driver and clear any pending interrupts
    while ((intr = ioctl_get_int()))
	{
		ioctl_clr_int((intr-1) / 8 , (intr-1) % 8);
    }

    // Now the sub-threads will be started

    res = pthread_create(&a_thread, NULL, thread_function, NULL);
	
	if(res != 0) 
	{
		perror("Thread creation failed\n");
		exit(EXIT_FAILURE);
    }

    // Both threads are now running in the background. They'll execute up
    // to the point were there are no interrupts and suspend. We as their
    // parent continue on. The nice thing about POSIX threads is that we're 
    // all in the same data space the parent and the children so we can 
    // share data directly. In this program we share the event_count 
    // variable.

	// We'll continue on in this loop until we're terminated
    while(!cancel) 
    {
		    usleep(50000);
    }

    // This flag is a shared variable that the children can look at to
	// know we're finished and they can exit too. */
    exit_flag = 1;

    // Display our event count total
    printf("Event count = %d\r", event_count);
    printf("\nCancelling thread\n");
    
    // If our children are not in a position to see the exit_flag, we
    // will use a more forceful technique to make sure they terminate with
    // us. If we leave them hanging and we try to re-run the program or
    // if another program wants to talk to the device they may be locked
    // out. This way everything cleans up much nicer.

	pthread_cancel(a_thread);
    fflush(NULL);
    return 0;
}

// This is the first of the sub-processes. For the purpose of this
// example, it does nothing but wait for an interrupt and then 
// reports that fact via the console. It also increments the shared 
// data variable event_count.

void *thread_function(void *arg)
{
	int c;

	while(1) 
	{
	    pthread_testcancel();

		if(exit_flag)
			break;

	    // This call will put THIS process to sleep until either an
	    // interrupt occurs or a terminating signal is sent by the 
	    // parent or the system.
	    c = ioctl_wait_int();

	    // We check to see if it was a real interrupt instead of a
	    // termination request.
	    if(c > 0) 
		{
		    struct timeval tv;
                    gettimeofday(&tv, NULL);
		    packet.time_sec = tv.tv_sec;
		    packet.time_usec = tv.tv_usec - 1900;
       		    while (packet.time_usec < 0) packet.time_usec += 1000000 , packet.time_sec -= 1;

                    sendto(socketFD, &packet, sizeof(SensorPacketHeader), 0, (struct sockaddr *)&socketADDR, sizeof(socketADDR));
                    ++event_count;
	    }
		else
			break;
	}
	return NULL;
}
