#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <values.h>

#include "leica.h"
#include "sensor-packet.h"

#define RECVLEN 256 // Should be at least twice the biggest packet size
static char startString[3];

typedef struct _Leica
{
    char            *deviceName;
    int              serialFD;
    int              socketFD;
    struct sockaddr_in socketADDR;
    pthread_t        listenThread;
    int              exitFlag;
    pthread_mutex_t  mutex;
    unsigned char    recv_buf[RECVLEN];
    unsigned short   recv_buf_pos;
    char             comNum;
    float            lastRead;
    struct timeval   tv;
} _Leica;

typedef struct MessageType
{
    unsigned char startByte;
    int (*matchFunction)(Leica leica, unsigned char *buffer, int bytesAvailable);
    // return 0 if no match, num bytes if match, -1 if needs more data
} MessageType;

static int matchDistanceSample(Leica leica, unsigned char *buffer, int bytesAvailable)
{
    static SensorPacket *packet = NULL;
    struct timeval tv;

    gettimeofday(&tv, NULL);
    if (!packet)
    {
        packet = (SensorPacket *)malloc(sizeof(SensorPacket));
        if (!packet) return 0;
    }
    if (bytesAvailable >= 4)
    {
        if (buffer[1] == '1' && buffer[2] == '.' && buffer[3] == '.')
        {
            int neededBytes = 16;
            if (bytesAvailable >= neededBytes)
            {
                buffer[15] = 0; // so atoi function will work
                packet->header.packet_type = SP_TYPE_LEICA;
                packet->header.packet_cmd = SP_CMD_DATA;
                packet->header.packet_num = 0;
                packet->header.num_buffered_packets = 30;
                packet->header.deviceClass = 0;
                packet->header.dataType = 1;
                packet->header.dataSize = 4;
                packet->header.serialNumber = leica->comNum - '0';
                packet->header.time_sec = tv.tv_sec;
                packet->header.time_usec = tv.tv_usec;
                packet->header.channelMask = 1;
                packet->header.dataLength = packet->header.dataSize;
                packet->data.floats[0] = atoi((char *)buffer + 6)/10.0; // Assuming inch mode
                leica->lastRead = packet->data.floats[0];
                leica->tv = tv;

                SensorPacket_ToNetworkOrder(packet);
                sendto(leica->socketFD, packet, sizeof(SensorPacketHeader) + 4, 0,
                        (struct sockaddr *)&leica->socketADDR, sizeof(leica->socketADDR));
                write(leica->serialFD, startString, 3);
                return neededBytes;
            }
            else
            {
                return -1;
            }
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return -1;
    }
}

static int matchErrorCode(Leica leica, unsigned char *buffer, int bytesAvailable)
{
    if (bytesAvailable >= 5)
    {
        write(leica->serialFD, startString, 3);
        return 5;
    }
    else
    {
        return -1;
    }
}

static int matchCRNL(Leica leica, unsigned char *buffer, int bytesAvailable)
{
    if (bytesAvailable >= 2)
    {
        return 2;
    }
    else
    {
        return -1;
    }
}

static int matchDistanceCorrection(Leica leica, unsigned char *buffer, int bytesAvailable)
{
    if (bytesAvailable >= 16)
    {
        return 16;
    }
    else
    {
        return -1;
    }
}

static MessageType messageTypes[] =
{
    {'3', matchDistanceSample},
    {'5', matchDistanceCorrection},
    {'@', matchErrorCode},
    {13, matchCRNL},
    {0x00, NULL},
};

static ssize_t parseBuffer(Leica leica)
{
    ssize_t i;

    for (i=0; i<leica->recv_buf_pos; )
    {
        int j, messageTooLongFound=0;
        for (j=0; messageTypes[j].startByte; j++)
        {
            if (leica->recv_buf[i] == messageTypes[j].startByte)
            {
                int matchResult = messageTypes[j].matchFunction(leica, leica->recv_buf + i, leica->recv_buf_pos - i);
                if (matchResult > 0)
                {
                    i += matchResult;
                    break;
                }
                else if (matchResult < 0)
                {
                    messageTooLongFound = 1;
                }
            }
        }
        if (!messageTypes[j].startByte) // Gone through all message types and i not incremented
            // so either need to increment i or break
        {
            if (messageTooLongFound)
            {
                break;
            }
            else
            {
                fprintf(stderr, "skip %02x\n", leica->recv_buf[i]);
                i++;
            }
        }
    }

    return i;
}

static void *listenThread(void *arg)
{
    Leica leica = (Leica)arg;
    ssize_t i, numBytes, bytesParsed;

    while (!leica->exitFlag)
    {
        usleep(10000);
        pthread_mutex_lock(&leica->mutex);
        if (!leica->serialFD)
        {
            if ((leica->serialFD = open(leica->deviceName, O_RDWR | O_NOCTTY | O_NONBLOCK)) <  0)
            {
                static SensorPacket packet;
                struct timeval tv;
                leica->serialFD = 0;
                sleep(1);
                gettimeofday(&tv, NULL);
                packet.header.packet_type = SP_TYPE_LEICA;
                packet.header.packet_cmd = SP_CMD_DATA;
                packet.header.packet_num = 0;
                packet.header.num_buffered_packets = 30;
                packet.header.deviceClass = 0;
                packet.header.dataType = 1;
                packet.header.dataSize = 4;
                packet.header.serialNumber = leica->comNum - '0';
                packet.header.time_sec = tv.tv_sec;
                packet.header.time_usec = tv.tv_usec;
                packet.header.channelMask = 1;
                packet.header.dataLength = packet.header.dataSize;
                packet.data.floats[0] = MAXFLOAT;

                SensorPacket_ToNetworkOrder(&packet);
                sendto(leica->socketFD, &packet, sizeof(SensorPacketHeader) + 4, 0,
                        (struct sockaddr *)&leica->socketADDR, sizeof(leica->socketADDR));
            }
            else
            {
                write(leica->serialFD, startString, 3);
            }
        }
        if (leica->serialFD)
        {
            if ((numBytes = read(leica->serialFD, leica->recv_buf + leica->recv_buf_pos, RECVLEN - leica->recv_buf_pos)) > 0)
            {
                leica->recv_buf_pos += numBytes;
                bytesParsed = parseBuffer(leica);
                if (bytesParsed)
                    for (i=0; i<(leica->recv_buf_pos - bytesParsed); i++)
                        leica->recv_buf[i] = leica->recv_buf[i+bytesParsed];
                leica->recv_buf_pos -= bytesParsed;
            }
            if (numBytes < 0 && errno != EAGAIN) perror("listen:");
            if (numBytes == 0)
            {
                close(leica->serialFD);
                leica->serialFD = 0;
            }
            if (leica->recv_buf_pos >= RECVLEN) // so long we have to drop it
            {
                fprintf(stderr, "Data Dropped:\n");
                for (i=0; i<RECVLEN; i++)
                    fprintf(stderr, "%02x:", leica->recv_buf[i]);
                fprintf(stderr, "\n");
                leica->recv_buf_pos = 0;
            }
        }
        pthread_mutex_unlock(&leica->mutex);
    }
    return NULL;
}

Leica Leica_create(char *deviceName, char *ipAddress, unsigned short port)
{
    Leica leica = (Leica)malloc(sizeof(_Leica));
    leica->exitFlag = 0;
    leica->deviceName = deviceName;
    pthread_mutex_init(&leica->mutex, NULL);
    leica->recv_buf_pos = 0;
    leica->serialFD = 0;
    leica->lastRead = 0.0;
    leica->comNum = deviceName[strlen(deviceName)-1];
    if ((leica->socketFD = socket(AF_INET,SOCK_DGRAM,0)) <  0)
    {
        perror("Creating UDP Socket");
        free(leica);
        return NULL;
    }
    memset(&leica->socketADDR,0,sizeof(leica->socketADDR));
    leica->socketADDR.sin_family = AF_INET;
    leica->socketADDR.sin_addr.s_addr = inet_addr(ipAddress);
    leica->socketADDR.sin_port = htons(port);

    startString[0] = 'g';
    startString[1] = 13;
    startString[2] = 10;
    if (pthread_create(&leica->listenThread, NULL, listenThread, leica) != 0)
    {
        perror("Leica_create:Creating Thread");
        free(leica);
        return NULL;
    }
    return leica;
}

void Leica_destroy(Leica leica)
{
    leica->exitFlag = 1;
    pthread_join(leica->listenThread, NULL);
    close(leica->serialFD);
    // Need to free memory inside
    free(leica);
}

float Leica_lastread(Leica leica)
{
    struct timeval tv;
    gettimeofday(&tv, NULL);
    if (leica->lastRead == 0.0)
    {
        return 0.0;
    }
    else
    {
        if (leica->tv.tv_sec + 2 < tv.tv_sec)
            return 0.0;
        else
            return leica->lastRead;
    }
}
