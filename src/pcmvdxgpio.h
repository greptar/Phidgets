//*****************************************************************************
//	
//	Copyright 2011 by WinSystems Inc.
//
//	Permission is hereby granted to the purchaser of WinSystems GPIO cards 
//	and CPU products incorporating a GPIO device, to distribute any binary 
//	file or files compiled using this source code directly or in any work 
//	derived by the user from this file. In no case may the source code, 
//	original or derived from this file, be distributed to any third party 
//	except by explicit permission of WinSystems. This file is distributed 
//	on an "As-is" basis and no warranty as to performance or fitness of pur-
//	poses is expressed or implied. In no case shall WinSystems be liable for 
//	any direct or indirect loss or damage, real or consequential resulting 
//	from the usage of this source code. It is the user's sole responsibility 
//	to determine fitness for any considered purpose.
//
//*****************************************************************************
//
//	Name	 : pcmvdxgpio.h
//
//	Project	 : PCM-VDX GPIO Linux Driver
//
//	Author	 : Paul DeMetrotion
//
//*****************************************************************************
//
//	  Date		Revision	                Description
//	--------	--------	---------------------------------------------
//	05/11/11	  1.00		Original Release
//
//*****************************************************************************

#define INPUT 0
#define OUTPUT 1
#define FALLING 0
#define RISING 1

// south bridge config registers
#define GPIO_P0_BAR 0x60
#define GPIO_P1_BAR 0x62
#define GPIO_DIR_BAR 0x6A
#define GPIO_CTRL 0x6C
#define GPIO_P0_INT_MSK 0xDC
#define GPIO_P0_INT_LEV 0xDD
#define GPIO_P0_INT_CTL 0xDE
#define GPIO_P0_INT_MOD 0xDF
#define GPIO_P1_INT_MSK 0xE0
#define GPIO_P1_INT_LEV 0xE1
#define GPIO_P1_INT_CTL 0xE2
#define GPIO_P1_INT_MOD 0xE3

// i/o registers
#define GPIO_P0_DATA 0x78
#define GPIO_P1_DATA 0x79
#define GPIO_P0_DIR 0x98
#define GPIO_P1_DIR 0x99
#define GPIO_P0_INT_STAT 0x9F
#define GPIO_P1_INT_STAT 0x9E

#define IOCTL_NUM 'w'

#ifndef CHARDEV_H
  #define CHARDEV_H

#include <linux/ioctl.h> 

// config port function
#define IOCTL_SET_PORT_DIR _IOW(IOCTL_NUM, 1, int)

// bit read function
#define IOCTL_READ_BIT _IOR(IOCTL_NUM, 2, int)

// set bit function
#define IOCTL_SET_BIT _IOW(IOCTL_NUM, 3, int)

// clear bit function
#define IOCTL_CLR_BIT _IOW(IOCTL_NUM, 4, int)

// read port function
#define IOCTL_READ_PORT _IOR(IOCTL_NUM, 5, int)

// write port function
#define IOCTL_WRITE_PORT _IOW(IOCTL_NUM, 6, int)

// enable interrupt function
#define IOCTL_ENAB_INT _IOW(IOCTL_NUM, 7, long)

// disable interrupt function
#define IOCTL_DISAB_INT _IOW(IOCTL_NUM, 8, int)

// retrieve interrupt function
#define IOCTL_GET_INT _IOR(IOCTL_NUM, 9, int)

// clear interrupt function
#define IOCTL_CLEAR_INT _IOW(IOCTL_NUM, 10, int)

// wait for interrupt function
#define IOCTL_WAIT_INT _IOWR(IOCTL_NUM, 11, int)

// device name in /dev
#define DEVICE_FILE_NAME "/dev/pcmvdxgpio"

#endif

// function calls
int ioctl_set_port_dir(int port, int direction);
int ioctl_read_bit(int port, int bit_number);
int ioctl_set_bit(int port, int bit_number);
int ioctl_clr_bit(int port, int bit_number);
unsigned int ioctl_read_port(int port);
int ioctl_write_port(int port, int value);
int ioctl_enab_int(int port, int bit_number, int polarity);
int ioctl_disab_int(int port);
int ioctl_get_int(void);
int ioctl_clr_int(int port, int bit_number);
int ioctl_wait_int(void);
