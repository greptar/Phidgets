#include <stdio.h>
#include <signal.h>

#include "uStrainBS.h"

static int cancel=0;
static void sighandle(int signum)
{
        cancel = 1;
}

int main(int argc, char **argv)
{
	MicroStrainBS ustrain;

	ustrain = MicroStrainBS_create("/dev/ttyUSB0");
	if (!ustrain)
		exit(1);

	signal(SIGINT, &sighandle);
	while (!cancel)
	{
		sleep(10);
	}
	MicroStrainBS_destroy(ustrain);

	return 0;
}
