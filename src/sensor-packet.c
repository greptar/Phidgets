#include <stdio.h>
#include <endian.h>
#include "sensor-packet.h"

#if __BYTE_ORDER == LITTLE_ENDIAN
#include <linux/byteorder/little_endian.h>
union v
{
  float f;
  unsigned int i;
};

void SensorPacket_ToNetworkOrder(SensorPacket *packet)
{
    int i, numChan;
    union v v;
    unsigned char dataSize;
    unsigned short dataLength;

    dataSize = packet->header.dataSize;
    dataLength = packet->header.dataLength;

    packet->header.time_sec = __cpu_to_be32(packet->header.time_sec);
    packet->header.time_usec = __cpu_to_be32(packet->header.time_usec);
    packet->header.serialNumber = __cpu_to_be32(packet->header.serialNumber);
    packet->header.deviceClass = __cpu_to_be16(packet->header.deviceClass);
    packet->header.channelMask = __cpu_to_be16(packet->header.channelMask);
    packet->header.dataLength = __cpu_to_be16(packet->header.dataLength);

    if (!dataSize)
        return;

    numChan = dataLength/dataSize;
    for (i=0; i<numChan; i++)
        if (dataSize == 2)
            packet->data.uShorts[i] = __cpu_to_be16(packet->data.uShorts[i]);
        else
        {
           v.f = packet->data.floats[i];
           v.i = __cpu_to_be32(v.i);
           packet->data.floats[i] = v.f;
        }
}

void SensorPacket_FromNetworkOrder(SensorPacket *packet, int length)
{
    int i, numChan;
    union v v;
    unsigned char dataSize;
    unsigned short dataLength;

    if (length < sizeof(SensorPacketHeader))
        return;

    packet->header.time_sec = __cpu_to_be32(packet->header.time_sec);
    packet->header.time_usec = __cpu_to_be32(packet->header.time_usec);
    packet->header.serialNumber = __cpu_to_be32(packet->header.serialNumber);
    packet->header.deviceClass = __cpu_to_be16(packet->header.deviceClass);
    packet->header.channelMask = __cpu_to_be16(packet->header.channelMask);
    packet->header.dataLength = __cpu_to_be16(packet->header.dataLength);

    dataSize = packet->header.dataSize;
    dataLength = packet->header.dataLength;

    if (length < sizeof(SensorPacketHeader) + dataLength)
        return;

    if (!dataSize)
        return;

    numChan = dataLength/dataSize;
    for (i=0; i<numChan; i++)
        if (dataSize == 2)
            packet->data.uShorts[i] = __cpu_to_be16(packet->data.uShorts[i]);
        else
        {
           v.f = packet->data.floats[i];
           v.i = __cpu_to_be32(v.i);
           packet->data.floats[i] = v.f;
        }
}

#else
void SensorPacket_ToNetworkOrder(SensorPacket *packet) { return; }
void SensorPacket_FromNetworkOrder(SensorPacket *packet, int length) { return; }
#endif
