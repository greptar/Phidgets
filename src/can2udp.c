#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>
#include <err.h>
#include <string.h>
#include <unistd.h>
#include "hico_api.h"
#include "pcan.h"

static unsigned char usingEmtrion = 1;

#define PCAN_BAUD_1M     0x0014

static void checkPeakError(int canFD)
{
  int do_reset = 0;
  TPSTATUS peakStatus;
  int ret = ioctl(canFD, PCAN_GET_STATUS, &peakStatus);
  if(ret<0){
    err(1, "could not get peak status");
  }
  if (peakStatus.wErrorFlag == 0 || peakStatus.wErrorFlag == CAN_ERR_QRCVEMPTY) return;
  fprintf(stderr, "CAN Errors : ");
  if (peakStatus.wErrorFlag & CAN_ERR_XMTFULL) fprintf(stderr, "CAN_ERR_XMTFULL ");
  if (peakStatus.wErrorFlag & CAN_ERR_OVERRUN) fprintf(stderr, "CAN_ERR_OVERRUN ");
  if (peakStatus.wErrorFlag & CAN_ERR_BUSLIGHT) fprintf(stderr, "CAN_ERR_BUSLIGHT "),do_reset = 1;
  if (peakStatus.wErrorFlag & CAN_ERR_BUSHEAVY) fprintf(stderr, "CAN_ERR_BUSHEAVY "),do_reset = 1;
  if (peakStatus.wErrorFlag & CAN_ERR_BUSOFF) fprintf(stderr, "CAN_ERR_BUSOFF "),do_reset = 1;
  if (peakStatus.wErrorFlag & CAN_ERR_QRCVEMPTY) fprintf(stderr, "CAN_ERR_QRCVEMPTY ");
  if (peakStatus.wErrorFlag & CAN_ERR_QOVERRUN) fprintf(stderr, "CAN_ERR_QOVERRUN ");
  if (peakStatus.wErrorFlag & CAN_ERR_QXMTFULL) fprintf(stderr, "CAN_ERR_QXMTFULL ");
  if (peakStatus.wErrorFlag & CAN_ERR_REGTEST) fprintf(stderr, "CAN_ERR_REGTEST ");
  if (peakStatus.wErrorFlag & CAN_ERR_NOVXD) fprintf(stderr, "CAN_ERR_NOVXD ");
  if (peakStatus.wErrorFlag & CAN_ERR_RESOURCE) fprintf(stderr, " CAN_ERR_RESOURCE ");
  if (peakStatus.wErrorFlag & CAN_ERR_ILLPARAMTYPE) fprintf(stderr, "CAN_ERR_ILLPARAMTYPE ");
  if (peakStatus.wErrorFlag & CAN_ERR_ILLPARAMVAL) fprintf(stderr, "CAN_ERR_ILLPARAMVAL ");
  if (peakStatus.wErrorFlag & CAN_ERRMASK_ILLHANDLE) fprintf(stderr, "CAN_ERRMASK_ILLHANDLE ");
  fprintf(stderr, "\n");
  if (do_reset)
  {
        TPCANInit init;
        init.wBTR0BTR1    = PCAN_BAUD_1M;    // combined BTR0 and BTR1 register of the SJA100
        init.ucCANMsgType = MSGTYPE_EXTENDED;  // 11 or 29 bits
        init.ucListenOnly = 0;            // listen only mode when != 0

        ret=ioctl(canFD, PCAN_INIT, &init);
        if(ret!=0){
            err(1, "could not initialize peak can board");
        }
  }
}

static int initialize_can_port(void)
{
    int ret, val, Node = open("/dev/can0", O_RDWR);
    if(Node<0){
        Node = open("/dev/pcanpci0", O_RDWR);
        if(Node<0)
            err(1, "could not open can node /dev/can0 or /dev/pcanpci0");
        usingEmtrion = 0;
    }
    if (usingEmtrion)
    {
        ret=ioctl(Node,IOC_RESET_BOARD);
        if(ret!=0){
            err(1, "could not reset board");
        }
        val=BITRATE_1000k;
        ret=ioctl(Node,IOC_SET_BITRATE,&val);
        if(ret!=0){
            err(1, "could not set bitrate");
        }
        ret=ioctl(Node,IOC_START);
        if(ret!=0){
            err(1, "IOC_START");
        }
    }
    else
    {
        TPCANInit init;
        init.wBTR0BTR1    = PCAN_BAUD_1M;    // combined BTR0 and BTR1 register of the SJA100
        init.ucCANMsgType = MSGTYPE_EXTENDED;  // 11 or 29 bits
        init.ucListenOnly = 0;            // listen only mode when != 0

        ret=ioctl(Node, PCAN_INIT, &init);
        checkPeakError(Node);
        if(ret!=0){
            err(1, "could not initialize peak can board");
        }
    }
    return Node;
}

static int initialize_udp_port(int port)
{
   int sock;
   struct sockaddr_in server;
   sock=socket(AF_INET, SOCK_DGRAM, 0);
   if (sock < 0) err(1, "Opening socket");
   memset(&server,0,sizeof(server));
   server.sin_family=AF_INET;
   server.sin_addr.s_addr=INADDR_ANY;
   server.sin_port=htons(port);
   if (bind(sock,(struct sockaddr *)&server,sizeof(server))<0)
       err(1, "binding");
   return sock;
}

static void forwardCanToUdp(int canFD, int udpFD, struct sockaddr_in *remote)
{
   if (usingEmtrion)
   {
      char buf[128];
      int ret=read(canFD,buf,sizeof(struct can_msg));
      if (ret == sizeof(struct can_msg))
      {
         if (sendto(udpFD, buf, sizeof(struct can_msg), 0, (struct sockaddr *)remote, sizeof(struct sockaddr_in)) != sizeof(struct can_msg))
            err(1, "could not send to udp");
      }
      else
      {
         fprintf(stderr, "%d read from CAN instead of %d on fd %d\n", ret, sizeof(struct can_msg), canFD);
	 if (ret==-1) err(1, "could not read from can");
      }
   }
   else
   {
      struct timeval tv;
      TPCANRdMsg msg;
      int ret = ioctl(canFD, PCAN_READ_MSG, &msg);
      checkPeakError(canFD);
      if (ret<0)
         err(1, "could not read from peak can board");
      fprintf(stdout, "CAN2UDP,%d,%d\n", (int)msg.Msg.ID, (int)(gettimeofday(&tv, NULL), tv.tv_usec));
      if (sendto(udpFD, &msg, sizeof(TPCANRdMsg), 0, (struct sockaddr *)remote, sizeof(struct sockaddr_in)) != sizeof(TPCANRdMsg))
         err(1, "could not send to udp");
   }
}

static void forwardUdpToCan(int udpFD, int canFD)
{
   struct sockaddr_in from;
   socklen_t fromlen = sizeof(struct sockaddr_in);
   char buf[128];
   int ret = recvfrom(udpFD,buf,128,0,(struct sockaddr *)&from,&fromlen);
   if (usingEmtrion)
   {
      if (ret == sizeof(struct can_msg))
      {
         if (write(canFD,buf,sizeof(struct can_msg)) != sizeof(struct can_msg))
            err(1, "could not write to emtrion can board");
      }
      else
      {
         fprintf(stderr, "%d read from udp instead of %d\n", ret, (int)sizeof(struct can_msg));
      }
   }
   else
   {
      if (ret == sizeof(TPCANRdMsg))
      {
         struct timeval tv;
         TPCANRdMsg *msg = (TPCANRdMsg *)buf;
         fprintf(stdout, "UDP2CAN,%d,%d\n", (int)msg->Msg.ID, (int)(gettimeofday(&tv, NULL), tv.tv_usec));
         int ret = ioctl(canFD, PCAN_WRITE_MSG, msg);
         checkPeakError(canFD);
         if (ret<0)
            err(1, "could not write to peak can board");
      }
      else
      {
         fprintf(stderr, "%d read from udp instead of %d\n", ret, (int)sizeof(TPCANRdMsg));
      }
   }
}

int main(int argc, char **argv)
{
	int port, canFD, udpFD, numFD, ret;
	fd_set fds;
	struct timeval tv;
	struct sockaddr_in remote;
	struct hostent *hp;

	if (argc != 3)
	{
		fprintf(stderr, "Usage: server port\n");
		exit(1);
	}
	hp = gethostbyname(argv[1]);
	if (hp==0) err(1,"Unknown host");
	port = atoi(argv[2]);
	remote.sin_family = AF_INET;
	memcpy(&remote.sin_addr,hp->h_addr,hp->h_length);
	remote.sin_port = htons(port);

	udpFD = initialize_udp_port(port);
	canFD = initialize_can_port();
	numFD = (canFD > udpFD ? canFD : udpFD) + 1;
	FD_ZERO(&fds);
	tv.tv_sec=0;

	while (1)
	{
		tv.tv_usec=1000;
		FD_SET(canFD,&fds);
		FD_SET(udpFD,&fds);
		ret = select(numFD,&fds,0,0,&tv);
		if (ret<0)
			err(1, "select");
		else if (ret>0)
		{
                        if (FD_ISSET(canFD,&fds))
                                forwardCanToUdp(canFD, udpFD, &remote);
                        if (FD_ISSET(udpFD,&fds))
                                forwardUdpToCan(udpFD, canFD);
		}
	}
	return 0;
}
