#include <stdio.h>
#include <string.h> /* memset */
#include <unistd.h> /* usleep */
#include <stdlib.h> /* exit */
#include <sys/io.h> /* inb, outb */
#include <assert.h>
#include <sched.h> /* realtime scheduling */

#define NUM_IO_BOARDS 2 /* Can have up to 4 */
#define IO_BASE 0x300
#define mSECS_TO_FIRE 5
#define mSECS_BETWEEN_FIRE 30

#define NUM_IO_PORTS NUM_IO_BOARDS * 16
#define NUM_IO_BYTES NUM_IO_BOARDS * 2

#define MAXLINEBUF 64

int main(int argc, char **argv)
{
	char linebuf[MAXLINEBUF];
	unsigned char io_out[NUM_IO_BYTES];
	unsigned char io_in[NUM_IO_BYTES];
	int fets_to_burn[NUM_IO_PORTS];
	struct sched_param sched_param;
	unsigned long base = IO_BASE;
	int have_perms = 1;
	int num_fets_to_burn = 0;
	int i, num_iterations;

	sched_param.sched_priority = 5;
	if (ioperm(base, NUM_IO_BOARDS * 8, 1) == -1)
	{
		fprintf(stderr, "Unable to get I/O Permissions\n");
		have_perms = 0;
	}
	if (sched_setscheduler(0, SCHED_RR, &sched_param) == -1)
	{
		fprintf(stderr, "Unable to get Realtime Permissions\n");
	}

	if (argc < 3)
	{
		fprintf(stderr, "Wrong usage\n");
		exit(1);
	}
	if (argc > NUM_IO_PORTS + 2)
	{
		fprintf(stderr, "Too many arguments\n");
		exit(1);
	}
	num_iterations = atoi(argv[1]);
	i=2;
	while (i<argc)
	{
		fets_to_burn[num_fets_to_burn] = atoi(argv[i]);
		num_fets_to_burn++;
		i++;
	}
	fprintf(stdout, "Burning fets ");
	memset(io_out, 0, sizeof(io_out));
	for (i=0;i<num_fets_to_burn;i++)
	{
		fprintf(stdout, "%d ", fets_to_burn[i]);
		io_out[fets_to_burn[i]/8] |= 1 << fets_to_burn[i] % 8;
	}
	fprintf(stdout, "for %d iterations\n", num_iterations);

#if 0
	fprintf(stdout, "Hit enter to continue, ^C to abort->");
	fflush(stdout);
	fgets(linebuf, MAXLINEBUF, stdin);
	fprintf(stdout, "Firing in...");
	for (i=5;i>0;i--)
	{
		fprintf(stdout, "%d...", i);
		fflush(stdout);
		sleep(1);
	}
	fprintf(stdout, "\n");
#endif
	for (i=0;i<num_iterations && have_perms;i++)
	{
		outb(~io_out[0], base);
		outb(~io_out[1], base+4);
		#if NUM_IO_BOARDS > 1
		outb(~io_out[2], base+8);
		outb(~io_out[3], base+12);
		#endif
		#if NUM_IO_BOARDS > 2
		outb(~io_out[4], base+16);
		outb(~io_out[5], base+20);
		#endif
		#if NUM_IO_BOARDS > 3
	        outb(~io_out[6], base+24);
		outb(~io_out[7], base+28);
		#endif
		usleep(mSECS_TO_FIRE * 1000);
		outb(~0, base);
		outb(~0, base+4);
		#if NUM_IO_BOARDS > 1
		outb(~0, base+8);
                outb(~0, base+12);
		#endif
		#if NUM_IO_BOARDS > 2
                outb(~0, base+16);
                outb(~0, base+20);
		#endif
		#if NUM_IO_BOARDS > 3
		outb(~0, base+24);
		outb(~0, base+28);
		#endif
		usleep(mSECS_BETWEEN_FIRE * 1000);
	}
	return 0;
}
