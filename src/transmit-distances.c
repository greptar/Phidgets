#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <signal.h>
#include <unistd.h>
#include <values.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "leica.h"
#include "laser_position.h"

static int cancel=0;
static void sighandle(int signum)
{
        cancel = 1;
}

int main(int argc, char* argv[])
{
	int bad_command_line=0, sendto_port=1027;
	char *sendto_address;
        Leica leica1=NULL, leica2=NULL, leica3=NULL, leica4=NULL;
        Laser_Position laser_position=NULL;

	signal(SIGINT, &sighandle);

        if (argc < 2)
        {
                bad_command_line++;
        }
        else
        {
                sendto_address = argv[1];
                if (inet_addr(sendto_address) == INADDR_NONE)
                        fprintf(stderr, "Bad IP Address\n"), bad_command_line++;
	}
	if (bad_command_line)
        {
                fprintf(stderr, "Usage: %s <ip_address>\n", argv[0]);
                return 1;
	}
	while (!cancel)
	{
                float l1=0.0, l2=0.0, l3=0.0, l4=0.0;
                if (!leica1) leica1 = Leica_create("/dev/rfcomm1", sendto_address, sendto_port);
                if (!leica2) leica2 = Leica_create("/dev/rfcomm2", sendto_address, sendto_port);
                if (!leica3) leica3 = Leica_create("/dev/rfcomm3", sendto_address, sendto_port);
                if (!leica4) leica4 = Leica_create("/dev/rfcomm4", sendto_address, sendto_port);
                if (!laser_position) laser_position = Laser_Position_create(sendto_address, sendto_port);
		sleep(1);
                if (leica1) l1 = Leica_lastread(leica1);
                if (leica2) l2 = Leica_lastread(leica2);
                if (leica3) l3 = Leica_lastread(leica3);
                if (leica4) l4 = Leica_lastread(leica4);
                if (laser_position) Laser_Position_send(laser_position, l1, l2, l3, l4);
	}
        if (leica1) Leica_destroy(leica1);
        if (leica2) Leica_destroy(leica2);
        if (leica3) Leica_destroy(leica3);
        if (leica4) Leica_destroy(leica4);
        if (laser_position) Laser_Position_destroy(laser_position);

	fprintf(stdout, "\nClosed %s cleanly\n", argv[0]);

	return 0;
}

