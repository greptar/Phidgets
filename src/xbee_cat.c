#include <stdio.h>
#include <stdlib.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <netdb.h>
#include <fcntl.h>
#include <err.h>
#include <string.h>
#include <unistd.h>

#define XBEE_MAX_SIZE 1024

static int initialize_xbee_port(char *tty)
{
	int xbfd = open(tty, O_RDWR);
	if (xbfd<0)
	{
		fprintf(stderr, "Could not open xbee node %s\n", tty);
		exit(1);
	}
	return xbfd;
}

static void forwardStdinToXbee(int xbfd, int id)
{
	char buf[64];
	int ret, i;
	ret=read(0,buf,64);
	if (ret > 0)
	{
		char xbeeOut[160]; /* Big enough if every character has to be escaped */
		char idBytes[4];
		char cksum = 0;
		int outLen = ret + 18;
		memcpy(idBytes, &id, 4);
		xbeeOut[0]  = 0x7e;
		xbeeOut[1]  = 0;
		xbeeOut[2]  = ret + 14;
		cksum += xbeeOut[3]  = 0x10;
		cksum += xbeeOut[4]  = 0x00;
		cksum += xbeeOut[5]  = 0;
		cksum += xbeeOut[6]  = 0x13;
		cksum += xbeeOut[7]  = 0xa2;
		cksum += xbeeOut[8]  = 0;
		cksum += xbeeOut[9]  = idBytes[3];
		cksum += xbeeOut[10] = idBytes[2];
		cksum += xbeeOut[11] = idBytes[1];
		cksum += xbeeOut[12] = idBytes[0];
		cksum += xbeeOut[13] = 0xff;
		cksum += xbeeOut[14] = 0xfe;
		cksum += xbeeOut[15] = 0;
		cksum += xbeeOut[16] = 0;
		for (i=0;i<ret;i++)
		{
			cksum += xbeeOut[17+i] = buf[i];
		}
		xbeeOut[17+i] = 0xff - cksum;
		for (i=1;i<outLen;i++)
		{
			if (xbeeOut[i] == 0x7e || xbeeOut[i] == 0x7d || xbeeOut[i] == 0x11 || xbeeOut[i] == 0x13)
			{
				int j;
				for (j=outLen;j>i+1;j--)
				{
					xbeeOut[j] = xbeeOut[j-1];
				}
				xbeeOut[i+1] = xbeeOut[i] ^ 0x20;
				xbeeOut[i] = 0x7d;
				outLen++;
			}
		}
		if (write(xbfd, xbeeOut, outLen) != outLen)
			fprintf(stderr, "Write error\n");
	}
}

static int IndexOf(char *buf, char val, int start, int count)
{
	int i;
	for (i=start;i<start+count;i++)
	{
		if (buf[i] == val)
			return i;
	}
	return -1;
}

static int consumeZigbeePacket(char *b, int len, unsigned int xbid)
{
	unsigned char result[XBEE_MAX_SIZE];
	int i=0, escapeCount=0;
	if (len<1 || b[0]!=0x7e)
		return 0;
	while (i + escapeCount < len)
	{
		if (b[i + escapeCount] == 0x7D && (i + escapeCount + 1) < len)
		{
			escapeCount++;
			result[i] = 0x20 ^ b[i + escapeCount];
		}
		else
		{
			result[i] = b[i + escapeCount];
		}
		i++;
		if (i>2 && i == (result[2] + 256*result[1] + 4))
			break;
	}
	if (i == (result[2] + 256*result[1] + 4))
	{
		unsigned char chksum=0;
		int j;
		for (j=3; j<i; j++)
			chksum += result[j];
		if (chksum == 0xff)
		{
			if (result[3] == 0x90)
			{
				unsigned char *xbid_c = (unsigned char *)&xbid;
				if (	xbid_c[0] == result[11] && xbid_c[1] == result[10] &&
					xbid_c[2] == result[9] && xbid_c[3] == result[8])
				write(1, result+15, i-15-1);
			}
			else if (result[3] == 0x92)
			{
			}
			else
			{
				int j;
				fprintf(stderr, "Valid Packet type %hhx\n", result[3]);
				for (j=0;j<i;j++)
					fprintf(stderr, "%02hhx ", result[j]);
				fprintf(stderr, "\n");
			}
		}
		else
		{
			fprintf(stderr, "Bad Packet\n");
		}
		return i+escapeCount;
	}
	else
	{
		return 0;
	}
}

static void forwardXbeeToStdout(int xbfd, unsigned int xbid)
{
	static char mainBuffer[XBEE_MAX_SIZE];
	static int mainBufferPosition = 0;
	int bytesThisRead, startLoc, endLoc, i;

	bytesThisRead = read(xbfd, mainBuffer + mainBufferPosition, XBEE_MAX_SIZE - mainBufferPosition);
	if (bytesThisRead < 0)
	{
		fprintf(stderr, "Read error from XBee\n");
		return;
	}
	mainBufferPosition += bytesThisRead;

	startLoc = IndexOf(mainBuffer, 0x7e, 0, mainBufferPosition);
	if (startLoc != -1)
	{
		if (startLoc > 0)
		{
			for (i=startLoc; i<mainBufferPosition; i++)
				mainBuffer[i - startLoc] = mainBuffer[i];
			mainBufferPosition -= startLoc;
			startLoc = 0;
		}
		while (mainBufferPosition > 0 && (endLoc = consumeZigbeePacket(mainBuffer, mainBufferPosition, xbid)))
		{
			for (i=endLoc; i<mainBufferPosition; i++)
				mainBuffer[i - endLoc] = mainBuffer[i];
			mainBufferPosition -= endLoc;
		}
	}
	if (mainBufferPosition == XBEE_MAX_SIZE)
	{
		fprintf(stderr, "Buffer overflow\n");
		mainBufferPosition = 0;
	}
}

int main(int argc, char **argv)
{
	int xbfd, numFD, ret;
	unsigned int xbid;
	fd_set fds;
	struct timeval tv;

	if (argc != 3)
	{
		fprintf(stderr, "Usage: %s </dev/ttyUSB?> <XBee remote ID>\n", argv[0]);
		exit(1);
	}
	else
	{
		xbfd = initialize_xbee_port(argv[1]);
		xbid = strtoul(argv[2], NULL, 16);
		numFD = xbfd + 1;
	}
	FD_ZERO(&fds);
	tv.tv_sec=0;

	while (1)
	{
		tv.tv_usec=1000;
		FD_SET(0, &fds);
		FD_SET(xbfd,  &fds);
		ret = select(numFD, &fds, 0, 0, &tv);
		if (ret<0)
			err(1, "select");
		else if (ret>0)
		{
                        if (FD_ISSET(0, &fds))
                                forwardStdinToXbee(xbfd, xbid);
                        if (FD_ISSET(xbfd, &fds))
                                forwardXbeeToStdout(xbfd, xbid);
		}
	}
	return 0;
}
