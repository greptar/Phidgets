//*****************************************************************************
//	
//	Copyright 2011 by WinSystems Inc.
//
//	Permission is hereby granted to the purchaser of WinSystems GPIO cards 
//	and CPU products incorporating a GPIO device, to distribute any binary 
//	file or files compiled using this source code directly or in any work 
//	derived by the user from this file. In no case may the source code, 
//	original or derived from this file, be distributed to any third party 
//	except by explicit permission of WinSystems. This file is distributed 
//	on an "As-is" basis and no warranty as to performance or fitness of pur-
//	poses is expressed or implied. In no case shall WinSystems be liable for 
//	any direct or indirect loss or damage, real or consequential resulting 
//	from the usage of this source code. It is the user's sole responsibility 
//	to determine fitness for any considered purpose.
//
//*****************************************************************************
//
//	Name	 : pcmvdxgpioio.c
//
//	Project	 : PCM-VDX GPIO Linux Driver
//
//	Author	 : Paul DeMetrotion
//
//*****************************************************************************
//
//	  Date		Revision	                Description
//	--------	--------	---------------------------------------------
//	05/11/11	  1.00		Original Release
//
//*****************************************************************************

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>      /* open */ 
#include <unistd.h>     /* exit */
#include <sys/ioctl.h>  /* ioctl */

// Include the WinSystems PCMVDXGPIO definitions
#include "pcmvdxgpio.h"

// device handle
int check_handle(void);
int handle = 0;

// the names of our device nodes
char *device_id = "/dev/pcmvdxgpio";

//------------------------------------------------------------------------
//
// ioctl_set_port_dir - Configure the GPIO ports
//
// Arguments:
//			port		Port number to configure
//			dir			Port I/O config 
//
// Returns:
//			SUCCESS	Completion
//			-1		Device handle is invalid
//
//------------------------------------------------------------------------

int ioctl_set_port_dir(int port, int dir)
{
	if(check_handle())		// Check if device available
		return(-1);			// Return -1 if not

	return(ioctl(handle, IOCTL_SET_PORT_DIR, ((port << 8) | dir)));
}

//------------------------------------------------------------------------
//
// ioctl_read_bit - Reads the bit value of an input point
//
// Arguments:
//			port		Port number to read
//			bit_number	Bit number to read
//
// Returns:
//			0,1		Result of the call (the bit value)
//			-1		Device handle is invalid
//
//------------------------------------------------------------------------

int ioctl_read_bit(int port, int bit_number)
{
	if(check_handle())		// Check if device available
		return(-1);			// Return -1 if not

	return(ioctl(handle, IOCTL_READ_BIT, (port << 8) | bit_number));
}

//------------------------------------------------------------------------
//
// ioctl_set_bit - Sets a single output point
//
// Arguments:
//			port		Port number to write
//			bit_number	Bit number to set
//
// Returns:
//			SUCCESS	Completion
//			-1		Device handle is invalid
//
//------------------------------------------------------------------------

int ioctl_set_bit(int port, int bit_number)
{
	if(check_handle())		// Check if device available
		return(-1);			// Return -1 if not

	return(ioctl(handle, IOCTL_SET_BIT, (port << 8) | bit_number));
}

//------------------------------------------------------------------------
//
// ioctl_clr_bit - Clears a single output point
//
// Arguments:
//			port		Port number to write
//			bit_number	Bit number to clear
//
// Returns:
//			SUCCESS	Completion
//			-1		Device handle is invalid
//
//------------------------------------------------------------------------

int ioctl_clr_bit(int port, int bit_number)
{
	if(check_handle())		// Check if device available
		return(-1);			// Return -1 if not

	return(ioctl(handle, IOCTL_CLR_BIT, (port << 8) | bit_number));
}

//------------------------------------------------------------------------
//
// ioctl_read_port - Reads the value of an port
//
// Arguments:
//			port		Port number to read
//
// Returns:
//			00-FFh	Result of the call (the port value)
//			-1		Device handle is invalid
//
//------------------------------------------------------------------------

unsigned int ioctl_read_port(int port)
{
	if(check_handle())		// Check if device available
		return(-1);			// Return -1 if not

	return(ioctl(handle, IOCTL_READ_PORT, port));
}

//------------------------------------------------------------------------
//
// ioctl_write_port - Writes the value of an port
//
// Arguments:
//			port		Port number to write
//			value		Value to write
//
// Returns:
//			SUCCESS	Completion
//			-1		Device handle is invalid
//
//------------------------------------------------------------------------

int ioctl_write_port(int port, int value)
{
	if(check_handle())		// Check if device available
		return(-1);			// Return -1 if not

	return(ioctl(handle, IOCTL_WRITE_PORT, (port << 8) | value));
}

//------------------------------------------------------------------------
//
// ioctl_enab_int - Enable notification of an input points change to the 
//			  specified state.
//
// Arguments:
//			port		Port selected
//			bit_number	Bit number interrupt to enable
//			polarity	State to look for (0 or 1)
//
// Returns:
//			SUCCESS	Completion
//			1		Illegal IRQ requested
//			-1		Device handle is invalid
//
//------------------------------------------------------------------------

int ioctl_enab_int(int port, int bit_number, int polarity)
{
    if(check_handle())		// Check for chip available
		return -1;			// Return -1 if not

	return(ioctl(handle, IOCTL_ENAB_INT, port << 16 | bit_number << 8 | polarity));
}

//------------------------------------------------------------------------
//
// ioctl_disab_int - Disable notification on an input points state change.
//
// Arguments:
//			port		Port selected
//
// Returns:
//			SUCCESS	Completion
//			-1		Device handle is invalid
//
//------------------------------------------------------------------------

int ioctl_disab_int(int port)
{
    if(check_handle())		// Check for chip available
		return(-1);			// Return -1 if not

	return(ioctl(handle, IOCTL_DISAB_INT, port));
}

//------------------------------------------------------------------------
//
// ioctl_get_int - Poll for notification of an input points state change.
//
// Arguments:
//
// Returns:
//			0		No interrupt
//			1-16	Bit value of received interrupt
//						1-8 = Port0[0..7]
//						9-16 = Port1[0..7]
//			-1		Device handle is invalid
//
//------------------------------------------------------------------------

int ioctl_get_int(void)
{
    if(check_handle())		// Check for chip available
		return(-1);			// Return -1 if not

	return(ioctl(handle, IOCTL_GET_INT, 0));
}

//------------------------------------------------------------------------
//
// ioctl_clr_int - Acknowledge the notification of an input point
//			state change.
//
// Arguments:
//			port		Port selected
//			bit_number	Bit number interrupt to disable
//
// Returns:
//			SUCCESS	Completion
//			-1		Device handle is invalid
//
//------------------------------------------------------------------------

int ioctl_clr_int(int port, int bit_number)
{
    if(check_handle())		// Check for chip available
		return(-1);			// Return -1 if not

	return(ioctl(handle, IOCTL_CLEAR_INT, (port << 8) | bit_number));
}

//------------------------------------------------------------------------
//
// ioctl_wait_int - Wait for the notification of an input point 
//			state change.
//
// Arguments:
//
// Returns:
//			0		No interrupt
//			1-16	Bit value of received interrupt
//						1-8 = Port0[0..7]
//						9-16 = Port1[0..7]
//			-1		Device handle is invalid
//
//------------------------------------------------------------------------

int ioctl_wait_int(void)
{
    if(check_handle())		// Check for chip available
		return(-1);			// Return -1 if not

	return(ioctl(handle, IOCTL_WAIT_INT, 0));
}

//------------------------------------------------------------------------
//
// check_handle - Checks that a handle to the device file exists.
//			If it does not a file open is performed.
//
// Arguments:
//
// Returns:
//			0		Device handle is valid
//			-1		Device handle is invalid
//
//------------------------------------------------------------------------

int check_handle(void)
{
    if(handle > 0)		// If it's already a valid handle
		return(0);		// return that fact

	if(handle == -1)	// If it's already been tried
		return(-1);		// return that fact

	// Perform an open on the device file and store the result
	// open returns -1 upon failure, a file descriptor (a small non-negative integer) on success
	handle = open(device_id, O_RDWR);

    if(handle)		// If it's now a valid handle
		return(0);	// return 0

    handle = -1;	// Flag that it is not valid

	return(-1);		// and return -1 to indicate that fact
}
