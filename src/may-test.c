#include <stdio.h>
#include <string.h> /* memset */
#include <unistd.h> /* usleep */
#include <stdlib.h> /* exit */
#include <sys/io.h> /* inb, outb */
#include <sys/times.h> /* times */
#include <math.h> /* exp, signbit */
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <errno.h>
#include <sched.h> /* realtime scheduling */

#define IO_BASE 0x300
#define NUM_IO_BOARDS 2
#define NUM_IO_BYTES_PER_BOARD 2
#define FIRST_GROUND_IO 0
#define NUM_GROUNDS 6
#define FIRST_POWER_IO 6
#define NUM_POWERS 10

#define MAX_LINE_LENGTH 64
#define MAX_FILE_LENGTH 256

static float vcalibrations[7], acalibrations[7];
static int numcals;
static float angle_targets[MAX_FILE_LENGTH];
static float angle_deltas[MAX_FILE_LENGTH];
static float angle_times[MAX_FILE_LENGTH];
static int num_data_lines_in_file = 0;
static float DataLog[3];

/* Begin Kevin */
#include "GetStateYawCmd.c"
/* End Kevin */

static clock_t times_wrapper(void)
{
       clock_t ret;
       int save_errno = errno;
       errno   = 0;
       ret     = times(NULL);
       if (errno != 0) {
               ret = (clock_t) (-errno);
       }
       errno = save_errno;
       return ret;
}

static float voltage_to_angle(float volts)
{
        int i;
        for (i=0;i<numcals;i++)
                if (vcalibrations[i] > volts) break;
        if (i>0) i--;
        if (i==numcals-1) i--;
	return acalibrations[i] + (volts - vcalibrations[i]) * ((acalibrations[i+1] - acalibrations[i]) / (vcalibrations[i+1] - vcalibrations[i]));
}

static float get_current_voltage(void)
{
	static int fd=-1;
	static int send_req = 1;
	char linebuf[8]; /* 0.00V\r\n\0 */
	float v;
	int numread = 0;
	linebuf[7] = 0;
	if (fd == -1)
	{
		fd=open("/dev/ttyUSB0", O_RDWR);
		if (fd == -1)
		{
			fprintf(stderr, "Cannot open /dev/ttyUSB0\n");
			exit(1);
		}
	}
	if (send_req)
	{
		send_req = 0;
		write(fd, "Z", 1);
		return 0;
	}
	else
	{
		send_req = 1;
		while (numread < 7)
		{
			int ret = read(fd, linebuf + numread, 7 - numread);
			if (ret == -1)
			{
				perror("Reading from /dev/ttyUSB0");
				exit(1);
			}
			else
			{
				numread += ret;
			}
		}
		if (sscanf(linebuf, "%f", &v) == 1)
		{
			return v;
		}
		else
		{
			fprintf(stderr, "Cannot understand data :%s: from /dev/ttyUSB0\n", linebuf);
			exit(1);
		}
	}
}

static float get_current_angle(void)
{
	return voltage_to_angle(get_current_voltage());
}

static float get_target_angle(int t_in_msecs, float *target_rate)
{
	int i;

	for (i=0;i<num_data_lines_in_file;i++)
		if (angle_times[i] > t_in_msecs/1000.0) break;

	if (i>0) i--;

	*target_rate = angle_deltas[i];
	return angle_targets[i] + angle_deltas[i] * ((t_in_msecs/1000.0) - angle_times[i]) ;
}

int main(int argc, char **argv)
{
	FILE *fp;
	int b,i,j,line_num;
	clock_t current_time;
	unsigned char io_out[NUM_IO_BYTES_PER_BOARD];
	unsigned char io_in[NUM_IO_BYTES_PER_BOARD];
	char linebuf[MAX_LINE_LENGTH];
	struct sched_param sched_param;
	int jiffies_to_run;
	float current_angle;

	int fire_command_ready = 0;
	long jiffies_from_start = 0;
	unsigned long base = IO_BASE;
	int have_perms = 0;

        if (system("stty -F /dev/ttyUSB0 raw -echo 115200") != 0)
        {
                fprintf(stderr, "Could not set serial settings on sensor device /dev/ttyUSB0\n");
                exit(1);
        }
        if (argc != 3)
        {
		clock_t start = times_wrapper();
		while (1)
		{
			get_current_voltage();
			fprintf(stdout, "%d,%f\n", (int)(times_wrapper()-start),get_current_voltage());
		}
        }
	if (sscanf(argv[2], "%d", &jiffies_to_run) != 1)
	{
		fprintf(stderr, "Bad milliseconds_to_run\n");
		exit(1);
	}
	jiffies_to_run /= 10;
        fp = fopen(argv[1], "r");
        if (!fp)
        {
                fprintf(stderr, "Bad File\n");
                exit(1);
        }

	sched_param.sched_priority = 5;
	if (ioperm(base, NUM_IO_BOARDS * 8, 1) == -1)
		fprintf(stderr, "Unable to get I/O Permissions\n");
	else
		have_perms = 1;
	if (sched_setscheduler(0, SCHED_RR, &sched_param) == -1)
		fprintf(stderr, "Unable to get Realtime Permissions\n");

	if (fgets(linebuf, MAX_LINE_LENGTH, fp))
	{
		numcals = sscanf(linebuf, "%f,%f,%f,%f,%f,%f,%f",vcalibrations,vcalibrations+1,vcalibrations+2,vcalibrations+3,vcalibrations+4,vcalibrations+5,vcalibrations+6);
		if (numcals < 2)
		{
			fprintf(stderr, "Not enough voltage calibrations on line 1\n");
			exit(1);
		}
	}
	else
	{
		fprintf(stderr, "Bad File\n");
		exit(1);
	}
	if (fgets(linebuf, MAX_LINE_LENGTH, fp))
        {
		if (sscanf(linebuf, "%f,%f,%f,%f,%f,%f,%f",acalibrations,acalibrations+1,acalibrations+2,acalibrations+3,acalibrations+4,acalibrations+5,acalibrations+6) != numcals)
		{
			fprintf(stderr, "Not the same number of voltage calibrations as angle calibrations on line 2\n");
			exit(1);
		}
	}
        else
        {
                fprintf(stderr, "Bad File\n");
                exit(1);
        }

	while (fgets(linebuf, MAX_LINE_LENGTH, fp))
	{
		if (num_data_lines_in_file == MAX_FILE_LENGTH)
		{
			fprintf(stderr, "Ignoring everything after line %d\n", MAX_FILE_LENGTH+1);
			break;
		}
		if (sscanf(linebuf, "%f,%f,%f", angle_times+num_data_lines_in_file,
					        angle_targets+num_data_lines_in_file,
                                                angle_deltas+num_data_lines_in_file ) != 3)
		{
			fprintf(stderr, "Bad format on line %d\n", num_data_lines_in_file+2);
			exit(1);
		}
		if (num_data_lines_in_file > 0 && angle_times[num_data_lines_in_file] < angle_times[num_data_lines_in_file-1])
		{
			fprintf(stderr, "Time out of order on line %d\n", num_data_lines_in_file+2);
			exit(1);
		}
		num_data_lines_in_file++;
	}
	fclose(fp);
	fopen("/tmp/firing_log.txt", "w");
	if (!fp)
	{
		fprintf(stderr, "Cannot open /tmp/firing_log.txt for writing\n");
		exit(1);
	}
	memset(motor_presence, 1, sizeof(motor_presence));
	for (b=0;b<NUM_IO_BOARDS && have_perms;b++)
	{
		int offset = b * 8;
        	for (i=0;i<NUM_POWERS;i++)
		{
                	memset(io_out, 0, sizeof(io_out));
                	io_out[(FIRST_POWER_IO+i)/8] = 1 << (FIRST_POWER_IO+i) % 8;
                	outb(~io_out[0], base+offset);
                	outb(~io_out[1], base+offset+4);
                	usleep(10000);
                	io_in[0] = inb(base+offset+1);
                	io_in[1] = inb(base+offset+5);
                	for (j=0;j<NUM_GROUNDS;j++)
                        	if (((io_in[(FIRST_GROUND_IO+j)/8]) & (1 << (FIRST_GROUND_IO+j) % 8)) == 0)
				{
					motor_presence[(b*NUM_POWERS) + i][j] = 0;
                               		//fprintf(stderr, "Motor missing at %d %d\n", j+1, (b*NUM_POWERS) + i+1);
				}
                	outb(~0, base+offset);
                	outb(~0, base+offset+4);
        	}
	}
	fprintf(stdout, "File:\n");
	for (i=0;i<numcals;i++)
	{
		fprintf(stdout, "%f%s", vcalibrations[i], i < numcals-1 ? "," : "");
	}
	fprintf(stdout, "\n");
        for (i=0;i<numcals;i++)
        {
                fprintf(stdout, "%f%s", acalibrations[i], i < numcals-1 ? "," : "");
        }
        fprintf(stdout, "\n");

	for (line_num=0;line_num<num_data_lines_in_file;line_num++)
	{
		fprintf(stdout, "%f,%f,%f\n", angle_times[line_num], angle_targets[line_num], angle_deltas[line_num]);
	}
	fprintf(stdout, "Motors:\n");
	for (i=0;i<NUM_GROUNDS;i++)
	{
		for (b=0;b<NUM_IO_BOARDS;b++)
		{
			for (j=0;j<NUM_POWERS;j++)
			{
				fprintf(stderr, "%d", motor_presence[(b*NUM_POWERS)+j][i]);
			}
		}
		fprintf(stderr, "\n");
	}

	for (i=0;i<10;i++)
	{
		int ground, power;
		current_angle = get_current_angle(); /* Have to call this in pairs */
		current_angle = get_current_angle(); /* Have to call this in pairs */

		current_time = times_wrapper();
		while (current_time == times_wrapper());

		if (get_motor_to_fire(current_angle, 0, 0, &ground, &power, DataLog) == -1)
		{
			fprintf(stderr, "get_motor_to_fire returned -1\n");
			fclose(fp);
			exit(1);
		}
	}
	fprintf(stdout, "Hit enter to continue, ^C to abort->");
	fflush(stdout);
	fgets(linebuf, MAX_LINE_LENGTH, stdin);
	fprintf(stdout, "Firing in...");
	for (i=5;i>0;i--)
	{
		fprintf(stdout, "%d...", i);
		fflush(stdout);
		sleep(1);
	}
	fprintf(stdout, "\n");

	current_time = times_wrapper();
	while (current_time == times_wrapper()); /* Tight loop until jiffy changes */
	do
	{
		/* Should be close to the beginning of a jiffy at the top of this loop */
		int ground, power;
		float target_angle, target_rate;

		if (jiffies_from_start % 2 == 0)
		{
			int offset;
			if (fire_command_ready)
			{
				int tground = ground;
				int tpower = power;
				offset = (tpower/NUM_POWERS) * 8;
				tpower = tpower%NUM_POWERS;
				memset(io_out, 0, sizeof(io_out));
				io_out[(FIRST_POWER_IO+tpower)/8] = 1 << (FIRST_POWER_IO+tpower) % 8;
				io_out[(FIRST_GROUND_IO+tground)/8] |= 1 << (FIRST_GROUND_IO+tground) % 8;
				if (have_perms)
				{
					outb(~io_out[0], base+offset);
					outb(~io_out[1], base+offset+4);
				}
			}

			target_angle = get_target_angle(jiffies_from_start * 10, &target_rate);
			if (fire_command_ready)
			{
				fprintf(fp, "%f,%f,%f,%d,%d,%f,%f,%f\n", jiffies_from_start/100.0, current_angle, target_angle, ground+1, power+1, DataLog[0], DataLog[1], DataLog[2]);
			}
			else
			{
				fprintf(fp, "%f,%f,%f,0,0,%f,%f,%f\n", jiffies_from_start/100.0, current_angle, target_angle, DataLog[0], DataLog[1], DataLog[2]);
			}
			get_current_angle();
			current_time = times_wrapper();
			while (current_time == times_wrapper());

			if (fire_command_ready)
			{
				if (have_perms)
				{
					outb(~0, base+offset);
					outb(~0, base+offset+4);
				}
			}
		}
		else
		{
			current_angle = get_current_angle(); /* Read the sensor and get the current angle.  Takes some time. */
			target_angle = get_target_angle(jiffies_from_start * 10, &target_rate);
			fire_command_ready = get_motor_to_fire(current_angle, target_angle, target_rate, &power, &ground, DataLog);
			if (fire_command_ready == -1)
			{
				fprintf(stderr, "get_motor_to_fire returned -1\n");
				fclose(fp);
				exit(1);
			}
			fprintf(fp, "%f,%f,%f,0,0,%f,%f,%f\n", jiffies_from_start/100.0, current_angle, target_angle, DataLog[0], DataLog[1], DataLog[2]);
			current_time = times_wrapper();
			while (current_time == times_wrapper());
		}
		jiffies_from_start += 1;
	}
	while (jiffies_from_start <= jiffies_to_run);
	fclose(fp);
	system("cp /tmp/firing_log.txt firing_log.`date +%s`.txt");
	return 0;
}
