/* This program is for the WDC Bridge Node.

Created by KCM at Schafer Corp, August 2012

Command Line: ./WDC <IP Address> <EU Filename>
<IP Address> is the local address
<EU Filename> is the optional comma-delimited Engineering Unit Conversion file  

Spawns two threads:
1) UDPFunction just waits for the CS Server to ask for an IP address
2) TCPFunction responds to all the commands from the CS

the main program will run in an infinite loop.
A Sensor Manager Callback sends data to the CS.

The TCP function, in response to CS commands, sets flags for the main program.

This software will log the data it collects when so comamnded by the CS.

*/

#include "WDC.h"
#include "WDC_EU.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <sys/time.h>
#include <time.h>
#include <pthread.h>
#include <ctype.h>
#include <iostream>
#include <fcntl.h>
#include <signal.h>
#include "sensor-manager.h"

// When this is defined, the WDC software will create a comma-separated log file (.csv)
// in addition to the .dat file used by CVS.  the .csv file is readable with excel.
#define CVSDev

#ifdef CVSDev
static char csvRecordingFileName[BUFSIZE];		// A csv file just for dev purposes
#endif

static int SC_sockfd = 0; // Needed out here because TCP Thread and Data Callback both use it

static char sendbuffer[BUFSIZE];
static char timebuffer[BUFSIZE];
static char errorbuffer[BUFSIZE];
static char AddrStr[INET_ADDRSTRLEN];
static char datRecordingFileName[BUFSIZE];		// Expected to be in the format CS wants
static int scanbuffer[BUFSIZE];

// On Startup
static char* LOCALIP = NULL;
static char* EUFILENAME = NULL;
static FILE* EUFile = NULL;

// These struct used for time stamp
static struct timeval  tv;
static struct timezone tz;
static struct tm* mtm;

// Notice: WDC is currently not capable of being the "Master"
static int Master = 0;		// Set to 1 if Master, 0 if Slave
static int Record = 0;		// Set to 1 to record, 0 otherwise
static int ScanStep = DEFDATASTEP;		// We are pulse-Driven, so this is inconsequential
static int RecordStep = 1;
static int PickOffStep = 1;
static int OverSampleStep = 1;

static float SamplesToAverage [MAXFLOATS][MAXOVERSAMPLES];

static int datRecordingFile = -1;

#ifdef CVSDev
static FILE* csvRecordingFile = NULL;
#endif

// These are all inter-thread flags
static int Scan = 0;
static int ScanCount = 0;
static int RecordCount = 0;

static SensorManager sm;
static char **SensorNames;
static char **SensorUnits;
static int SensorCount;

static int cancel = 0;
static void sighandle(int signum)
{
	cancel = 1;
}

// Callback for the Sensor Manager...
static void SMcallBack(struct timeval timestamp, float *values, int count)
{
	int it,sit,nReturn,nSample;
	float AveragedValue = 0;
	float* FloatPointer;
	if (1 == Scan)
	{
		if (count != SensorCount)
		{
			sprintf(errorbuffer,"Sensor Manager CB count = %d, should be %d",count,SensorCount);
			perror(errorbuffer);
		}
		ScanCount++;

		nSample = (ScanCount % OverSampleStep);
		for (it = 0; it < SensorCount; it++)
		{
			SamplesToAverage[it][nSample] = values[it];
		}

		if (0 == nSample)
		{
			RecordCount++;

			scanbuffer[0] = SCAN_DATA;
			scanbuffer[1] = BYTESPERINT + sizeof(float)*SensorCount;
			scanbuffer[2] = ScanCount;			// Scan Count
			FloatPointer = (float *) &(scanbuffer[3]);
			for (it = 0; it < SensorCount; it++)
			{
				AveragedValue = 0.0;
				for (sit = 0; sit < OverSampleStep; sit++)
				{
					AveragedValue = AveragedValue + SamplesToAverage[it][sit]/OverSampleStep;
				}
				*FloatPointer = AveragedValue;
				FloatPointer++;
			}
			if (0 == (nSample % PickOffStep))
			{
				nReturn = send(SC_sockfd,scanbuffer,(int)(scanbuffer[1]+2*BYTESPERINT),0);
				if (nReturn < 0) perror("ERROR Sending Sensor Data to CS");
			}
		}

		if (Record)
		{
			// The following is to write the csv file header, for dev purposes.
			#ifdef CVSDev
			if (1 == ScanCount)
			{
				fprintf(csvRecordingFile,"scan,time,");
				for (it = 0; it < SensorCount; it++) {
					fprintf(csvRecordingFile,"%s",SensorNames[it]);
					if (it != (SensorCount-1)) fprintf(csvRecordingFile,",");
				}
				fprintf(csvRecordingFile,"\n");
				fprintf(csvRecordingFile,"(count),(sec),");
				for (it = 0; it < SensorCount; it++) {
					fprintf(csvRecordingFile,"(%s)",SensorUnits[it]);
					if (it != (SensorCount-1)) fprintf(csvRecordingFile,",");
				}
				fprintf(csvRecordingFile,"\n");
			}
			#endif
			
			if ( (nSample % RecordStep) == 0)
			{
				// this is the file I expect that the CS wants
				// Note that this is averaged (oversampled) data.
				write(datRecordingFile,&(scanbuffer[2]),(int)(scanbuffer[1]));

				// This is for the csv file, for dev only
				#ifdef CVSDev
				fprintf(csvRecordingFile,"%d,%d.%06d,",ScanCount,(int)timestamp.tv_sec, (int)timestamp.tv_usec);
				for (it = 0; it < SensorCount; it++) {
					fprintf(csvRecordingFile,"%g",values[it]);
					if (it != (SensorCount-1)) fprintf(csvRecordingFile,",");
				}
				fprintf(csvRecordingFile,"\n");
				#endif
			}
		}
	}
	fprintf(stdout, "SM CallBack: %d.%06d  ScanCount = %d\n", (int)timestamp.tv_sec, (int)timestamp.tv_usec, ScanCount);
	for (it=0; it<count; it++)
		fprintf(stdout, "%g %s\n", values[it], SensorUnits[it]);
	fprintf(stdout, "\n");

}

// This function is just used for console display
static char * GetTimeStr()
{
	gettimeofday(&tv, &tz);
	mtm = localtime(&tv.tv_sec);
	sprintf(timebuffer,"%02d/%02d/%04d %02d:%02d:%02d.%03d: ",
		mtm->tm_mon, mtm->tm_mday, mtm->tm_year + 1900,
		mtm->tm_hour, mtm->tm_min, mtm->tm_sec, (int)tv.tv_usec/1000); 
	return timebuffer;
}

// For when CS commands local recording.
static void CreateRecordingFileName()
{
	gettimeofday(&tv, &tz);
	mtm = localtime(&tv.tv_sec);
	#ifdef CVSDev
	sprintf(csvRecordingFileName,"/data/WDC%3d_Recording_File.csv",WDCID);
	#endif
	sprintf(datRecordingFileName,"/data/WDC%3d_Recording_File.dat",WDCID); 
}

static void OpenRecordingFile()
{
	if ((-1 == datRecordingFile) || (NULL == csvRecordingFile))
	{
		CreateRecordingFileName();
		if (-1 == datRecordingFile) datRecordingFile = open(datRecordingFileName,O_CREAT | O_WRONLY,S_IRUSR);
		printf("%sNow Recording to filename: %s\n",GetTimeStr(),datRecordingFileName);
		#ifdef CVSDev
		if (NULL == csvRecordingFile) csvRecordingFile = fopen(csvRecordingFileName,"w");
		printf("%sNow Recording to filename: %s\n",GetTimeStr(),csvRecordingFileName);
		#endif
	}	
}

static void CloseRecordingFile()
{
	#ifdef CVSDev
	if (NULL != csvRecordingFile)
	{
		fclose(csvRecordingFile);
		csvRecordingFile = NULL;
	}
	#endif
	if (-1 != datRecordingFile)
	{
		close(datRecordingFile);
		datRecordingFile = -1;
	}
}

// UDP function will run forever as a thread.
static void *UDPFunction(void *UDParam)
{
	char UDPbuffer[BUFSIZE];
	int uReturn = 0;
	int UDP_sockfd = 0;
	struct sockaddr_in UDP_addr;
	socklen_t UDP_len = 0;
	char reuse=1;

	// Open the UDP Socket
	bzero(UDPbuffer,BUFSIZE);
	gethostname(UDPbuffer,BUFSIZE-1);
	printf("\n%sStarting Server on %s...\n",GetTimeStr(),UDPbuffer);
	while ((UDP_sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 && !cancel)
	{
		perror("ERROR opening UDP socket");
		sleep(1);
	}
	fcntl(UDP_sockfd, F_SETFL, O_NONBLOCK);
	setsockopt(UDP_sockfd, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse, sizeof(char));
	if (cancel) return UDParam;
	UDP_len = sizeof(UDP_addr);
	bzero((char *) &UDP_addr, UDP_len);
	UDP_addr.sin_family = AF_INET;
	UDP_addr.sin_addr.s_addr = INADDR_ANY;
	UDP_addr.sin_port = htons(UDPINPORT);
	while (bind(UDP_sockfd, (struct sockaddr *) &UDP_addr, UDP_len) < 0 && !cancel)
	{
		perror("ERROR on binding UDP Socket");		  
		sleep(1);
	}
	if (cancel) return UDParam;
	printf("%sSuccessfully bound UDP Socket on port #%d\n",GetTimeStr(),UDPINPORT);

	// Will respond to repeated UDP messages from the CS.
	while (!cancel)
	{
		uReturn = recvfrom(UDP_sockfd, UDPbuffer, BUFSIZE-1,MSG_WAITALL, (struct sockaddr *) &UDP_addr, &UDP_len);
		if (uReturn > 0)
		{
			// Report the IP address of the CS
			inet_ntop(AF_INET, &(UDP_addr.sin_addr), AddrStr, INET_ADDRSTRLEN);
			printf("%sReceived UDP message from CS, IP Address = %s on Port %d\n",GetTimeStr(),AddrStr,UDPINPORT);	   
			// Report what the UDP Message said
			printf("%sThis is the UDP message:  %s\n",PADSTR,UDPbuffer);
			strcpy(UDPbuffer,LOCALIP);
			UDP_addr.sin_port = htons(UDPOUTPORT);
			uReturn = sendto(UDP_sockfd, UDPbuffer, strlen(UDPbuffer), 0, (struct sockaddr *) &UDP_addr,  UDP_len);
			if (uReturn < 0) perror("ERROR Sending on UDP Socket");  	 
			printf("%sSent this UDP Reply To CS: %s\n",GetTimeStr(),UDPbuffer);
		}
	}
	close(UDP_sockfd);
	return UDParam;
}

// This eternal thread is activated after the UDPFunction.
// It will wait for the CS to connect,
// then it will continuously respond to TCP Commands from the CS.
static void *TCPFunction(void *TCParam)
{
	int TCP_sockfd = 0;
	socklen_t TCP_len = 0;
	int TCPbuffer[BUFSIZE];
	int TCPReply[BUFSIZE];
	char *TCPPointer = NULL;
	int ReadByteCount = 0;
	int tReturn = 0;
	int rit,sit,ni = 0;
	struct sockaddr_in TCP_addr = {0};
	char reuse = 1;

	while ((TCP_sockfd = socket(AF_INET, SOCK_STREAM, 0)) < 0 && !cancel)
	{
		perror("ERROR opening TCP socket");
		sleep(1);
	}
	if (cancel) return TCParam;
	fcntl(TCP_sockfd, F_SETFL, O_NONBLOCK);
	setsockopt(TCP_sockfd, SOL_SOCKET, SO_REUSEADDR, (char *)&reuse, sizeof(char));
	TCP_len = sizeof(TCP_addr);
	bzero((char *) &TCP_addr, TCP_len);
	TCP_addr.sin_family = AF_INET;
	TCP_addr.sin_addr.s_addr = INADDR_ANY;
	TCP_addr.sin_port = htons(TCPPORT);
	while ((tReturn = bind(TCP_sockfd, (struct sockaddr *) &TCP_addr, TCP_len)) < 0 && !cancel)
	{
		perror("ERROR on binding TCP Socket");		  
		sleep(1);
	}
	if (cancel) return TCParam;
	printf("%sSuccessfully opened TCP Socket on port #%d\n",GetTimeStr(),TCPPORT);

	printf("%sListening for TCP connect.\n",GetTimeStr());
	listen(TCP_sockfd,5);		// "5" is the maximum # connections allowed by most systems

	bzero(TCPbuffer,BUFSIZE);
	// Forever respond to CS Messages.
	while (!cancel)
	{
		int newSocketFD = accept(TCP_sockfd, (struct sockaddr *)&TCP_addr, &TCP_len);
		if (newSocketFD > 0)
		{
			if (SC_sockfd > 0)
			{
				close(SC_sockfd);
			}
			SC_sockfd = newSocketFD;
			fcntl(SC_sockfd, F_SETFL, O_NONBLOCK);
		}
		if (!SC_sockfd)
		{
			sleep(1);
			continue;
		}
		
		ReadByteCount = read(SC_sockfd,TCPbuffer,BUFSIZE);
		if (ReadByteCount <= 0) continue;
		rit = 0;
		while (ReadByteCount > 0) {
			bzero(TCPReply,BUFSIZE);
			switch(TCPbuffer[rit]) {
			case GET_WDC_NAME:
				TCPReply[0] = GET_WDC_NAME;
				TCPReply[1] = BYTESPERINT; 				// Number of Bytes to come...
				TCPReply[2] = WDCID;
				tReturn = send(SC_sockfd,TCPReply,3*BYTESPERINT,0);	// This message is always 3 ints.
				if (tReturn < 0) perror("ERROR Sending WDC NAME");
				printf("%sReceived GET_WDC_NAME from CS, then sent: %d %d %d\n",
					GetTimeStr(),TCPReply[0],TCPReply[1],TCPReply[2]);
				break;
			case START_SCANNING:
				if (1 != Scan) {
					Scan = 1;	// flag looked at by callBack
					ScanCount = 0;
					RecordCount = 0;
				}
				if (Record) OpenRecordingFile();
				printf("%sReceived START_SCANNING from CS\n",GetTimeStr());
				break;
			case STOP_SCANNING:
				Scan = 0;	// flag looked at by main
				printf("%sReceived STOP_SCANNING from CS\n",GetTimeStr());
				CloseRecordingFile();
				break;
			case SCAN_DATA:
				printf("%sReceived SCAN_DATA from CS\n",GetTimeStr());
				break;
			case LOAD_SCANLIST:
				printf("%sReceived LOAD_SCANLIST from CS\n",GetTimeStr());
				// Get the Sensor Names
				SensorManager_getNamesAndUnits(sm, &SensorNames, &SensorUnits, &SensorCount);
				
				//  Now, go thru the EU list, and send EU Conversions to the SM
				for (sit=0; sit<SensorCount; sit++)
				{
					ni = FindSensorEUConversion(SensorNames[sit]);
					if (ni > -1)
					{
						SetEUIndex(ni,sit);
						SensorManager_setEUfunction(sm, sit, GetEUUnits(ni),  GetEUFunction(ni), GetEUParams(ni));
						printf("Set EU Funtion - Sensor %s, Units = %s\n",SensorNames[sit],GetEUUnits(ni));
					}
				}					
				// Send the sensor list to the CS
				TCPReply[0] = LOAD_SCANLIST;
				TCPReply[1] = 0;
				TCPPointer = (char *) &(TCPReply[2]);  // So we can put in several strings.
				for (sit=0; sit<SensorCount; sit++) {
					sprintf(sendbuffer,"%s",SensorNames[sit]);		// This string identifies a sensor
					printf("%s%s\n",PADSTR,sendbuffer);					// This is just for display
					strcpy(TCPPointer,sendbuffer);						// Put the string into the reply buffer
					TCPPointer += strlen(sendbuffer);					// Bump the pointer
					*TCPPointer++ = ',';								// Comma delimiter
					TCPReply[1] += strlen(sendbuffer) + 1; 				// Bump the byte counter.
				}
				*TCPPointer = 0;	// String delimiter
				TCPReply[1] -= 1;	// Decrement the byte count
				tReturn = send(SC_sockfd,TCPReply,2*BYTESPERINT + TCPReply[1],0);
				if (tReturn < 0) perror("ERROR Sending LOAD_SCANLIST Message!");				
				break;
			case ERROR_Renamed:
				printf("%sReceived ERROR_Renamed from CS\n",GetTimeStr());
				break;
			case WARNING:
				printf("%sReceived WARNING from CS\n",GetTimeStr());
				break;
			case SET_SCAN_RATE:
				printf("%sReceived SET_SCAN_RATE %d from CS\n",GetTimeStr(),TCPbuffer[rit+2]);
				ScanStep = (int) (TCPbuffer[rit+2]/USPERMS);	// Convert from int microseconds to int milliseconds
			sprintf(errorbuffer,"SET_SCAN_RATE will be ignored by WDC!");
				printf("%s%s\n",GetTimeStr(),errorbuffer);
				TCPReply[0] = WARNING;
				TCPReply[1] = strlen(errorbuffer); 				// Number of Bytes to come...
				strcpy(((char*)TCPReply) + 2*BYTESPERINT,errorbuffer);
				tReturn = send(SC_sockfd,TCPReply,2*BYTESPERINT + strlen(errorbuffer),0);
				if (tReturn < 0) perror("ERROR Sending Warning Message to CS!");
				break;
			case ARM_SYNCHRONOUS_START:
				printf("%sReceived ARM_SYNCHRONOUS_START from CS\n",GetTimeStr());
				Master = (TCPbuffer[rit+2] != 0);	// zero for false, non-zero otherwise
				if (0 != Master) {
					sprintf(errorbuffer,"Set to be Master, but can't be!");
					printf("%s%s\n",GetTimeStr(),errorbuffer);
					TCPReply[0] = ERROR_Renamed;
					TCPReply[1] = strlen(errorbuffer); 				// Number of Bytes to come...
					strcpy(((char*)TCPReply) + 2*BYTESPERINT,errorbuffer);
					tReturn = send(SC_sockfd,TCPReply,2*BYTESPERINT + strlen(errorbuffer),0);
					if (tReturn < 0) perror("ERROR Sending ARM_SYNCHRONOUS_START Error Message!");
				}				
				break;
			case GET_UNITS:
				printf("%sReceived GET_UNITS from CS\n",GetTimeStr());
				// Reply with a Units list.
				// We do not call the Sensor Manager again.  Use the data collected during "Get Sensors" call.
				TCPReply[0] = GET_UNITS;
				TCPReply[1] = 0;
				TCPPointer = (char *) &(TCPReply[2]);  // So we can put in several strings.
				for (sit=0; sit<SensorCount; sit++) {
					sprintf(sendbuffer,"%s",SensorUnits[sit]);	// This string identifies a unit
					printf("%s%s\n",PADSTR,sendbuffer);					// This is just for display
					strcpy(TCPPointer,sendbuffer);						// Put the string into the reply buffer
					TCPPointer += strlen(sendbuffer);					// Bump the pointer
					*TCPPointer++ = ',';								// Comma delimiter
					TCPReply[1] += strlen(sendbuffer) + 1; 				// Bump the byte counter.
				}
				*TCPPointer = 0;	// String delimiter
				TCPReply[1] -= 1;	// Decrement the byte count
				tReturn = send(SC_sockfd,TCPReply,2*BYTESPERINT + TCPReply[1],0);
				if (tReturn < 0) perror("ERROR Sending GET_UNITS Message!");				
				break;
			case SET_WDC_RECORDING:
				CloseRecordingFile();
				Record = (TCPbuffer[rit+2] != 0);	// zero for false, non-zero otherwise
				if (Record) OpenRecordingFile();
				else printf("%sRecording set to %s\n",GetTimeStr(),BOOL_STR(Record));
				break;
			case SET_WDC_RECORDING_RATE:
				RecordStep = TCPbuffer[rit+2];
				printf("%sSET_WDC_RECORDING_RATE %d\n",GetTimeStr(),RecordStep);
				break;
			case SET_WDC_PICKOFF_RATE:
				PickOffStep = TCPbuffer[rit+2];
				printf("%sSET_WDC_PICKOFF_RATE %d\n",GetTimeStr(),PickOffStep);
				break;
			case SET_WDC_OVER_SAMPLING_RATE:
				OverSampleStep = TCPbuffer[rit+2];
				printf("%sSET_WDC_OVER_SAMPLING_RATE %d\n",GetTimeStr(),OverSampleStep);
				break;
			case NULL_SENSORS:
				TCPPointer = (char *) &(TCPbuffer[rit+2]);  // So we can put in several strings.
				printf("%sReceived NULL_SENSORS from CS:\n%s",GetTimeStr(),TCPPointer);
				SensorManager_nullSensors(sm,TCPPointer);
				break;
			default:
				sprintf(errorbuffer,"%sReceived UNRECOGNIZED MESSAGE from CS: %d",GetTimeStr(),TCPbuffer[rit]);
				printf("%s\n",errorbuffer);
				TCPReply[0] = ERROR_Renamed;
				TCPReply[1] = strlen(errorbuffer); 				// Number of Bytes to come...
				strcpy(((char*)TCPReply) + 2*BYTESPERINT,errorbuffer);
				tReturn = send(SC_sockfd,TCPReply,2*BYTESPERINT + strlen(errorbuffer),0);
				if (tReturn < 0) perror("ERROR Sending Error Message to CS!");
				break;
			}
			ReadByteCount = ReadByteCount - 8 - TCPbuffer[rit + 1];
			rit = rit + 2 + TCPbuffer[rit + 1]/4;
		}	
	}
	if (SC_sockfd > 0) close(SC_sockfd);
	close (TCP_sockfd);
	return TCParam;
}

// main will spawn the other two threads
// then it will forever collect and send data,
// as long as CS wants it.
int main(int argc, char *argv[])
{
	pthread_t UDPthread, TCPthread;
	int nReturn = 0;	// Used for checking return values for errors
	int TCParam = 0, UDParam = 0;

	if (argc < 2)
	{
		fprintf(stderr, "Usage: %s <My IP> <EU Config Filename (optional)>\n", argv[0]);
		return 1;
	}
	else
	{
		LOCALIP = argv[1];
	}
	
	// Look for an (Optional) Engineering Unit Conversion File
	if (3 == argc)
	{ 
		EUFILENAME = argv[2];
		EUFile = fopen(EUFILENAME,"r+");	// Open for read/write
		if (NULL == EUFile)
		  {
			fprintf(stderr, "Failure to open EU Config File %s\n", EUFILENAME);
			return 1;
		  }
	}

	signal(SIGINT, &sighandle);
	
	// Initialize the Sensor Manager
	sm = SensorManager_create(SMPORT);
	SensorManager_setCallback(sm, SMcallBack);

	// Initialize the Communication Threads
	nReturn = pthread_create( &UDPthread, NULL, UDPFunction, &UDParam);
	nReturn = pthread_create( &TCPthread, NULL, TCPFunction, &TCParam);

	gettimeofday(&tv, &tz);
	mtm = localtime(&tv.tv_sec);
	printf("DATE %02d/%02d/%04d TIME %02d:%02d:%02d.%03d - TCP Server Running.\n",
		mtm->tm_mon, mtm->tm_mday, mtm->tm_year + 1900,
		mtm->tm_hour, mtm->tm_min, mtm->tm_sec, (int)tv.tv_usec/1000);
		
	// Initialize the EUConversion Structures
	InitEUStructure();
	if (NULL != EUFile)
	{
		CreateEUStructure(EUFile);
		fclose(EUFile);
		EUFile = NULL;
	}

	// Now, run forever or until ^c from console
	while (!cancel)
	{
		usleep(100*MS);
	}

	fprintf(stdout, "Sensor Manager To Be Released.\n");
	SensorManager_destroy(sm);
	fprintf(stdout, "Sensor Manager Released.\n");

	pthread_join(UDPthread, NULL);
	fprintf(stdout, "UDPthread Released.\n");
	pthread_join(TCPthread, NULL);
	fprintf(stdout, "TCPthread Released.\n");

	CleanUpEUStructs();
	CloseRecordingFile();
	return 0; 
}
