//=============================================================================
// (c) Copyright 2009 Diamond Systems Corporation. Use of this source code
// is subject to the terms of Diamond Systems' Software License Agreement.
// Diamond Systems provides no warranty of proper performance if this
// source code is modified.
//
// File: DSCADSampleInt.c   v1.00
//
// Depedency :	Universal Driver version 6.02
//				DSCUD.H - Include file.
//				DSCUDBCL.LIB - Library file for MS-DOS
//						( USE BC 5.02 compiler )
//				LIBDSCUD_6.02_1.a	- Library file for Linux 2.6.23
//						( USE GCC 3.xx compiler )
//				DSCUD.LIB	- Static library file for Windows XP
//						( USE VC++ 6.0 or Visual Studio 2005 )
//				DSCUD_API.LIB	- Static library for Windows CE 6.0
//						( Use Visual Studio 2005 with Platform Builder 6.0 )
// Description: This sample code demonstrates the usage of UD function
//				dscADSampleInt(). This sample code describes the usage
//				for Helios SBC. The driver will use the incumbent OS
//              in conjunction with the interrupt selected to transfer many
//              values from the A/D converter to user space, and once stopped,
//              will display a single set of those channel values,
//              interpretting them based on the mode given by the user.
// ************************* History ********************
// Version		Engineer		Date		Description
// 2.0                  Nathan Wharton          12/12/11        Schafer version
// 1.0			James Moore		07/31/09	Created.
//=============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctype.h>

#include "dscud.h"
#include "phidget21.h"

#define HELIOS_DEFAULT_BASE_ADDRESS 0x280
#define HELIOS_DEFAULT_IRQ 5
#define HELIOS_VOLTAGE_RANGE RANGE_10

#define HELIOS_HZ 10000
#define HELIOS_FIFO_SIZE 2000 // 200 ms time length of fifo

#define MS 1000                                 // milliseconds per second
#define USPERMS 1000                    // microseconds per millisecond
#define BUFSIZE 256                     // for character buffers
#define MINDATASTEP 100                 // Fastest ms interval for sending data
#define DEFDATASTEP 2000                // Default ms interval for sending data
#define MAXDATASTEP 100000              // Slowest ms interval for sending data
#define UDPINPORT 6551                  // Port to listen for "GetDC" from the CS
#define UDPOUTPORT 6552                 // Port to reply to "GetDC"
#define TCPPORT 6553                    // Port for all TCP Communication
static int WDCID=300;                              // Should be in a config file - THREE DIGITS for recording file name
static char *LOCALIP="192.168.52.104";       // ditto
#define PADSTR "                         "      // 25 blanks
#define BYTESPERINT 4                   // Bytes in an integer, for messaging.

// ============== From Kevin =======================
#define DUMMYSENSOR "W_Sensor_"
#define NUMSENSORS 3
#define DUMMYUNIT "mho"
// These are the messages from the CS:
#define GET_WDC_NAME 0
#define START_SCANNING 1
#define STOP_SCANNING 2
#define SCAN_DATA 3
#define LOAD_SCANLIST 4
#define ERROR_Renamed 5
#define WARNING 6
#define SET_SCAN_RATE 7
#define ARM_SYNCHRONOUS_START 8
#define GET_UNITS 9
#define SET_WDC_RECORDING 10
#define SET_WDC_RECORDING_RATE 11
#define SET_WDC_PICKOFF_RATE 12
#define SET_WDC_OVER_SAMPLING_RATE 13
#define NULL_SENSORS 14

#define BOOL_STR(b) ((b)?"true":"false")

int UDP_sockfd = 0;
int TCP_sockfd = 0;
int SC_sockfd = 0;
socklen_t UDP_len = 0;
socklen_t TCP_len = 0;
socklen_t SC_len = 0;
struct sockaddr_in UDP_addr = {0};
struct sockaddr_in TCP_addr = {0};
struct sockaddr_in SC_addr = {0};
struct sockaddr_in SELF_addr = {0};
char readbuffer[BUFSIZE];
char sendbuffer[BUFSIZE];
char timebuffer[BUFSIZE];
char errorbuffer[BUFSIZE];
char AddrStr[INET_ADDRSTRLEN];
char RecordingFile[BUFSIZE];
int scanbuffer[BUFSIZE];

// These struct used for time stamp
struct timeval  tv;
struct timezone tz;
struct tm *tm;
unsigned long DataTime = 0;             // Will be loop starting time (ms)
unsigned long BaseSecs = 0;             // Starting reference (sec)
unsigned long NowTime = 0;              // Milliseconds since BaseTime

int ScanStep = DEFDATASTEP;             // Will accept CS-commanded change
int RecordStep = 1;                             // Not Implemented yet.
int PickOffStep = 1;                    // Not Implemented yet.
int OverSampleStep = 1;                 // Not Implemented yet.

// For the threads.
pthread_t UDPthread, TCPthread, DCThread;       // DCThread not used yet...
pthread_attr_t tattr;           // Thread attributes

// These are all inter-thread flags
int UDPWait = 1;
int TCPWait = 1;
int Scan = 0;
// Notice: WDC is currently not capable of being the "Master"
int Master = 0;         // Set to 1 if Master, 0 if Slave
// Notice: Recording is not yet implemented"
int Record = 0;         // Set to 1 to record, 0 otherwise

// An Error handling routine
static void error(const char *msg)
{
        perror(msg);
        exit(1);
}

static char * GetTimeStr()
{
        gettimeofday(&tv, &tz);
        tm = localtime(&tv.tv_sec);
        sprintf(timebuffer,"%02d/%02d/%04d %02d:%02d:%02d.%03d: ",
                tm->tm_mon, tm->tm_mday, tm->tm_year + 1900,
                tm->tm_hour, tm->tm_min, tm->tm_sec, (int)tv.tv_usec/1000);
        return timebuffer;
}

static char * CreateRecordingFileName()
{
        gettimeofday(&tv, &tz);
        tm = localtime(&tv.tv_sec);
        sprintf(RecordingFile,"WDC%3d_Recording_File_%02d%02d%04d_%02d%02d%02d.txt",WDCID,
                tm->tm_mon, tm->tm_mday, tm->tm_year + 1900,
                tm->tm_hour, tm->tm_min, tm->tm_sec);
        return timebuffer;
}
// UDP function will run forever as a thread.
static void *UDPFunction(void *UDParam)
{
        char UDPbuffer[BUFSIZE];
        int uReturn = 0;
        // Open the UDP Socket
        bzero(UDPbuffer,BUFSIZE);
        gethostname(UDPbuffer,BUFSIZE-1);
        printf("\n%sStarting Server on %s...\n",GetTimeStr(),UDPbuffer);
        UDP_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
        if (UDP_sockfd < 0) error("ERROR opening UDP socket");
        UDP_len = sizeof(UDP_addr);
        bzero((char *) &UDP_addr, UDP_len);
        UDP_addr.sin_family = AF_INET;
        UDP_addr.sin_addr.s_addr = INADDR_ANY;
        UDP_addr.sin_port = htons(UDPINPORT);
        if (bind(UDP_sockfd, (struct sockaddr *) &UDP_addr, UDP_len) < 0)
                error("ERROR on binding UDP Socket");
        printf("%sSuccessfully opened UDP Socket on port #%d\n",GetTimeStr(),UDPINPORT);

        UDPWait = 0;            // This flag tells the main program to continue

        // Will respond to repeated UDP messages from the CS.
        while (1) {
                bzero(UDPbuffer,BUFSIZE);
                usleep(10*MS);
                printf("%sWaiting for a UDP message on port %d\n",GetTimeStr(),UDPINPORT);
                // Wait here until we receive any message at all...
                uReturn = recvfrom(UDP_sockfd, UDPbuffer, BUFSIZE-1,MSG_WAITALL, (struct sockaddr *) &UDP_addr, &UDP_len);
                if (uReturn < 0) error("ERROR on Receiving on UDP Socket");
                // Report the IP address of the CS
                inet_ntop(AF_INET, &(UDP_addr.sin_addr), AddrStr, INET_ADDRSTRLEN);
                printf("%sReceived UDP message from CS, IP Address = %s on Port %d\n",GetTimeStr(),AddrStr,UDPINPORT);
                // Report what the UDP Message said
                printf("%sThis is the UDP message:  %s\n",PADSTR,UDPbuffer);
                strcpy(UDPbuffer,LOCALIP);
                UDP_addr.sin_port = htons(UDPOUTPORT);
                uReturn = sendto(UDP_sockfd, UDPbuffer, strlen(UDPbuffer), 0, (struct sockaddr *) &UDP_addr,  UDP_len);
                if (uReturn < 0) error("ERROR Sending on UDP Socket");
                printf("%sSent UDP Reply To CS: %s\n",GetTimeStr(),UDPbuffer);
        }
        return UDParam;
}
// This eternal thread is activated after the UDPFunction.
// It will wait for the CS to connect,
// then it will continuously respond to TCP Commands from the CS.
static void *TCPFunction(void *TCParam)
{
        //naw char *CHTCP = NULL;
        int TCPbuffer[BUFSIZE];
        //      int Holdbuffer[BUFSIZE];
        int TCPReply[BUFSIZE];
        char *TCPPointer = NULL;
        //naw int HoldFlag = 0;               // If a TCP Message read delivers only a portion of a message.
        int ReadByteCount = 0;
        int tReturn = 0;
        int rit,sit = 0;

        TCP_sockfd = socket(AF_INET, SOCK_STREAM, 0);
        if (TCP_sockfd < 0) error("ERROR opening TCP socket");
        TCP_len = sizeof(TCP_addr);
        bzero((char *) &TCP_addr, TCP_len);
        TCP_addr.sin_family = AF_INET;
        TCP_addr.sin_addr.s_addr = INADDR_ANY;
        TCP_addr.sin_port = htons(TCPPORT);
        tReturn = bind(TCP_sockfd, (struct sockaddr *) &TCP_addr, TCP_len);
        if (tReturn < 0) error("ERROR on binding TCP Socket");
        printf("%sSuccessfully opened TCP Socket on port #%d\n",GetTimeStr(),TCPPORT);

        TCPWait = 0;    // This flag tells the main program to continue

        printf("%sListening for TCP connect on %s.\n",GetTimeStr(),AddrStr);
        listen(TCP_sockfd,5);           // "5" is the maximum # connections allowed by most systems
        // Now that a message has been received, get an IP address, and establish a connection
        SC_sockfd = accept(TCP_sockfd, (struct sockaddr *)&TCP_addr, &TCP_len);
        if (SC_sockfd < 0) error("ERROR accepting TCP Socket");
        printf("%sTCP Socket Connected on %s.\n",GetTimeStr(),AddrStr);

        bzero(TCPbuffer,BUFSIZE);
        // Forever respond to CS Messages.
        while (1) {
                printf("%sWaiting for a TCP message on port %d\n",GetTimeStr(),TCPPORT);
                ReadByteCount = read(SC_sockfd,TCPbuffer,BUFSIZE);
                if (ReadByteCount < 0) error("ERROR reading from TCP socket");
                if (ReadByteCount == 0) return TCParam;
                //               hexdump(TCPbuffer,16);
                rit = 0;
                while (ReadByteCount > 0) {
                        bzero(TCPReply,BUFSIZE);
                        switch(TCPbuffer[rit]) {
                        case GET_WDC_NAME:
                                TCPReply[0] = GET_WDC_NAME;
                                TCPReply[1] = BYTESPERINT;                              // Number of Bytes to come...
                                TCPReply[2] = WDCID;
                                tReturn = send(SC_sockfd,TCPReply,3*BYTESPERINT,0);  // This message is always 3 ints.
                                if (tReturn < 0) error("ERROR Sending WDC NAME");
                                printf("%sReceived GET_WDC_NAME from CS, then sent: %d %d %d\n",
                                        GetTimeStr(),TCPReply[0],TCPReply[1],TCPReply[2]);
                                break;
                        case START_SCANNING:
                                Scan = 1;       // flag looked at by main
                                printf("%sReceived START_SCANNING from CS\n",GetTimeStr());
                                break;
                        case STOP_SCANNING:
                                Scan = 0;       // flag looked at by main
                                printf("%sReceived STOP_SCANNING from CS\n",GetTimeStr());
                                break;
                        case SCAN_DATA:
                                printf("%sReceived SCAN_DATA from CS\n",GetTimeStr());
                                break;
                        case LOAD_SCANLIST:
                                printf("%sReceived LOAD_SCANLIST from CS\n",GetTimeStr());
                                // Reply with a sensor list.
                                TCPReply[0] = LOAD_SCANLIST;
                                TCPReply[1] = 0;
                                TCPPointer = (char *) &(TCPReply[2]);  // So we can put in several strings.
                                for (sit=0;sit<NUMSENSORS;sit++) {
                                        sprintf(sendbuffer,"%s%d",DUMMYSENSOR,sit+1);           // This string identifies a sensor
                                        printf("%s%s\n",PADSTR,sendbuffer);                                     // This is just for display
                                        strcpy(TCPPointer,sendbuffer);                                          // Put the string into the reply buffer
                                        TCPPointer += strlen(sendbuffer);                                       // Bump the pointer
                                        *TCPPointer++ = ',';                                                            // Comma delimiter
                                        TCPReply[1] += strlen(sendbuffer) + 1;                          // Bump the byte counter.
                                }
                                *TCPPointer = 0;        // String delimiter
                                TCPReply[1] -= 1;       // Decrement the byte count
                                tReturn = send(SC_sockfd,TCPReply,2*BYTESPERINT + TCPReply[1],0);
                                //                              hexdump(TCPReply,TCPReply[1] + 2*BYTESPERINT);
                                if (tReturn < 0) error("ERROR Sending LOAD_SCANLIST Message!");
                                break;
                        case ERROR_Renamed:
                                printf("%sReceived ERROR_Renamed from CS\n",GetTimeStr());
                                break;
                        case WARNING:
                                printf("%sReceived WARNING from CS\n",GetTimeStr());
                                break;
                        case SET_SCAN_RATE:
                                printf("%sReceived SET_SCAN_RATE %d from CS\n",GetTimeStr(),TCPbuffer[rit+2]);
                                ScanStep = (int) (TCPbuffer[rit+2]/USPERMS);    // Convert from int microseconds to int milliseconds
                                if (ScanStep < MINDATASTEP) {
                                        ScanStep = MINDATASTEP;
                                        sprintf(errorbuffer,"Setting Scan Step to minimum allowable %d ms",ScanStep);
                                        printf("%s%s\n",PADSTR,errorbuffer);
                                        TCPReply[0] = WARNING;
                                        TCPReply[1] = strlen(errorbuffer);                              // Number of Bytes to come...
                                        strcpy(((char*)TCPReply) + 2*BYTESPERINT,errorbuffer);
                                        tReturn = send(SC_sockfd,TCPReply,2*BYTESPERINT + strlen(errorbuffer),0);
                                        if (tReturn < 0) error("ERROR Sending SET_SCAN_RATE Error Message!");
                                }
                                if (ScanStep > MAXDATASTEP) {
                                        ScanStep = MAXDATASTEP;
                                        sprintf(errorbuffer,"Setting Scan Step to maximum allowable %d ms",ScanStep);
                                        printf("%s%s\n",PADSTR,errorbuffer);
                                        TCPReply[0] = ERROR_Renamed;
                                        TCPReply[1] = strlen(errorbuffer);                              // Number of Bytes to come...
                                        strcpy(((char*)TCPReply) + 2*BYTESPERINT,errorbuffer);
                                        tReturn = send(SC_sockfd,TCPReply,2*BYTESPERINT + strlen(errorbuffer),0);
                                        if (tReturn < 0) error("ERROR Sending SET_SCAN_RATE Error Message!");
                                }
                                // Echo Reply to the CS (I thought this was protocol, but now I think not -KCM)
//                              TCPReply[0] = SET_SCAN_RATE;
//                              TCPReply[1] = BYTESPERINT;
//                              TCPReply[2] = ScanStep*USPERMS;
//                              tReturn = send(SC_sockfd,TCPReply,3*BYTESPERINT,NULL);
//                              if (tReturn < 0) error("ERROR Sending SET_SCAN_RATE Message!");
//                              printf("%sSent SET_SCAN_RATE %d To CS\n",GetTimeStr(),TCPReply[2]);
                                break;
                        case ARM_SYNCHRONOUS_START:
                                printf("%sReceived ARM_SYNCHRONOUS_START from CS\n",GetTimeStr());
                                Master = (TCPbuffer[rit+2] != 0);       // zero for false, non-zero otherwise
                                if (0 != Master) {
                                        sprintf(errorbuffer,"Set to be Master, but can't be!");
                                        printf("%s%s\n",GetTimeStr(),errorbuffer);
                                        TCPReply[0] = ERROR_Renamed;
                                        TCPReply[1] = strlen(errorbuffer);                              // Number of Bytes to come...
                                        strcpy(((char*)TCPReply) + 2*BYTESPERINT,errorbuffer);
                                        tReturn = send(SC_sockfd,TCPReply,2*BYTESPERINT + strlen(errorbuffer),0);
                                        if (tReturn < 0) error("ERROR Sending ARM_SYNCHRONOUS_START Error Message!");
                                }
                                break;
                        case GET_UNITS:
                                printf("%sReceived GET_UNITS from CS\n",GetTimeStr());
                                // Reply with a Units list.
                                TCPReply[0] = GET_UNITS;
                                TCPReply[1] = 0;
                                TCPPointer = (char *) &(TCPReply[2]);  // So we can put in several strings.
                                for (sit=0;sit<NUMSENSORS;sit++) {
                                        sprintf(sendbuffer,"%s%d",DUMMYUNIT,sit+1);                     // This string identifies a unit
                                        printf("%s%s\n",PADSTR,sendbuffer);                                     // This is just for display
                                        strcpy(TCPPointer,sendbuffer);                                          // Put the string into the reply buffer
                                        TCPPointer += strlen(sendbuffer);                                       // Bump the pointer
                                        *TCPPointer++ = ',';                                                            // Comma delimiter
                                        TCPReply[1] += strlen(sendbuffer) + 1;                          // Bump the byte counter.
                                }
                                *TCPPointer = 0;        // String delimiter
                                TCPReply[1] -= 1;       // Decrement the byte count
                                tReturn = send(SC_sockfd,TCPReply,2*BYTESPERINT + TCPReply[1],0);
                                if (tReturn < 0) error("ERROR Sending GET_UNITS Message!");
                                break;
                        case SET_WDC_RECORDING:
                                Record = (TCPbuffer[rit+2] != 0);       // zero for false, non-zero otherwise
                                if (Record) {
                                        CreateRecordingFileName();
                                        printf("%sRecording set to %s, %s\n",GetTimeStr(),BOOL_STR(Record),RecordingFile);
                                        }
                                else {
                                        printf("%sRecording set to %s\n",GetTimeStr(),BOOL_STR(Record));
                                        }
                                break;
                        case SET_WDC_RECORDING_RATE:
                                RecordStep = TCPbuffer[rit+2];
                                printf("%sSET_WDC_RECORDING_RATE %d\n",GetTimeStr(),RecordStep);
                                break;
                        case SET_WDC_PICKOFF_RATE:
                                PickOffStep = TCPbuffer[rit+2];
                                printf("%sSET_WDC_PICKOFF_RATE %d\n",GetTimeStr(),PickOffStep);
                                break;
                        case SET_WDC_OVER_SAMPLING_RATE:
                                OverSampleStep = TCPbuffer[rit+2];
                                printf("%sSET_WDC_OVER_SAMPLING_RATE %d\n",GetTimeStr(),OverSampleStep);
                                break;
                        case NULL_SENSORS:
                                printf("%sReceived NULL_SENSORS from CS\n",GetTimeStr());
                                break;
                        default:
                                printf("%sReceived UNRECOGNIZED MESSAGE from CS: %d\n",GetTimeStr(),TCPbuffer[rit]);
                                break;
                        }
//                      printf("Before rit = %d, ByteCount = %d\n",rit,ReadByteCount);
                        ReadByteCount = ReadByteCount - 8 - TCPbuffer[rit + 1];
                        rit = rit + 2 + TCPbuffer[rit + 1]/4;
//                      printf("After rit = %d, ByteCount = %d\n",rit,ReadByteCount);
//                      usleep(10*MS);  // sleep for 10 milliseconds
                }

        }
        return TCParam;
}

// ==================== End From Kevin ====================

// command line variables.  Set to example values
static int trigger_channel = 0;
static float trigger_value = 2.5;

static int cancel = 0;
static void sighandle(int signum)
{
	cancel = 1;
}

static DSCB dscb;                   // handle used to refer to the board
static DSCADSETTINGS dscadsettings; // structure containing A/D conversion settings
static DSCAIOINT dscaioint;         // structure containing interrupt based acquire settings

static double lastAmbient = 0;
static double lastTemperature[4] = {0,0,0,0};

static void MyUserInterruptFunction(void* param)
{
	DFLOAT voltage;
	ERRPARAMS errorParams;       // structure for returning error code and error string
	struct timeval irq_tv, trigger_tv;
	int i;
	static int has_triggered = 0;

	gettimeofday(&irq_tv, NULL);
	irq_tv.tv_usec -= 1000000 / HELIOS_HZ * HELIOS_FIFO_SIZE + 4353;
	while (irq_tv.tv_usec < 0) irq_tv.tv_usec += 1000000 , irq_tv.tv_sec -= 1;
        for (i = 0 ; i < HELIOS_FIFO_SIZE; i++)
        {
                if (dscADCodeToVoltage(dscb, dscadsettings, dscaioint.sample_values[i], &voltage) != DE_NONE)
                {
                        dscGetLastError(&errorParams);
                        fprintf(stderr, "dscADCodeToVoltage error: %s %s\n",
				dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
			cancel = 1;
                }
		else
		{
                	if (has_triggered)
                	{
                        	if (voltage < trigger_value)
                        	{
                                	has_triggered = 0;
                        	}
                	}
                	else
                	{
                        	if (voltage >= trigger_value)
                        	{
                                	has_triggered = 1;
					trigger_tv = irq_tv;
                                	trigger_tv.tv_usec += 1000000 / HELIOS_HZ * i;
                                	while  (trigger_tv.tv_usec >= 1000000)
						trigger_tv.tv_usec -= 1000000 , trigger_tv.tv_sec += 1;
                                	//fprintf(stderr, "Trigger scan %d at time %d.%06d temp %f\n", i, 
						//(int)trigger_tv.tv_sec, (int)trigger_tv.tv_usec, lastTemperature[0]);
	                        	if (Scan)
                        		{
						static int it=1;
						float *FloatPointer;
						int nit, nReturn;

                                		scanbuffer[0] = SCAN_DATA;
                                		scanbuffer[1] = BYTESPERINT + sizeof(float)*NUMSENSORS;
                                		scanbuffer[2] = it;                     // Scan Count
                                		FloatPointer = (float *) &(scanbuffer[3]);
                                		for (nit=0; nit < NUMSENSORS; nit++) {
                                        		*FloatPointer = (float)lastTemperature[0], FloatPointer++;
                                        		}
                                		nReturn = send(SC_sockfd,scanbuffer,scanbuffer[1]+2*BYTESPERINT,0);
                                		if (nReturn < 0) error("ERROR Sending Data");
                                		it++;
                        		}
                        	}
                	}
		}
        }
}

static int TemperatureChangeHandler(CPhidgetTemperatureSensorHandle TEMP, void *usrptr, int Index, double Value)
{
        CPhidgetTemperatureSensor_getAmbientTemperature(TEMP, &lastAmbient);
	lastTemperature[Index] = Value;
        return 0;
}

static CPhidgetTemperatureSensorHandle temperatureSensorHandle = 0;
static int Start_Temperature_Sensor(void)
{
        int result;
        const char *err;

        CPhidgetTemperatureSensor_create(&temperatureSensorHandle);
        CPhidgetTemperatureSensor_set_OnTemperatureChange_Handler(temperatureSensorHandle, TemperatureChangeHandler, NULL);

        //open the temperature sensor for device connections
        CPhidget_open((CPhidgetHandle)temperatureSensorHandle, -1);

        //get the program to wait for an temperature sensor device to be attached
        if((result = CPhidget_waitForAttachment((CPhidgetHandle)temperatureSensorHandle, 1000)))
        {
                CPhidget_getErrorDescription(result, &err);
                printf("Problem waiting for temperature sensor: %s\n", err);
                return -1;
        }

        //modify the sensor sensitivity, index 1 is the thermocouple sensor, index 0 is the onboard or ambient sensor
        CPhidgetTemperatureSensor_setTemperatureChangeTrigger (temperatureSensorHandle, 1, 2.00);

        return 0;
}

static int Stop_Temperature_Sensor(void)
{
	CPhidget_close((CPhidgetHandle)temperatureSensorHandle);
	CPhidget_delete((CPhidgetHandle)temperatureSensorHandle);
	return 0;
}

int main(int argc, char **argv)
{
	DSCCB dsccb;                 // structure containing board settings (IRQ and Address)
	ERRPARAMS errorParams;       // structure for returning error code and error string
	DSCUSERINTFUNCTION dscuserintfunction;
	DSCSAMPLE *scan_buffer;
	int bad_command_line = 0;

	signal(SIGINT, &sighandle);

        //=========================================================================
        // 0. Read Command Line
        //=========================================================================

	if (argc != 4)
	{
		bad_command_line++;
	}
	else
	{
		if (sscanf(argv[1], "%d", &trigger_channel) != 1)
			bad_command_line++;
		else
			if (trigger_channel < -1 || trigger_channel > 15)
				bad_command_line++;
	
		if (sscanf(argv[2], "%f", &trigger_value) != 1)
			bad_command_line++;
		else
			if (trigger_value < -10 || trigger_value > 10)
				bad_command_line++;

		LOCALIP=argv[3];
	}
	if (bad_command_line)
	{
		fprintf(stderr, "Usage: %s <trigger_channel> <trigger_value> <My IP>\n", argv[0]);
		return 1;
	}

        //=========================================================================
	// 0.1 Calculations from command line
        //=========================================================================
	scan_buffer = calloc(HELIOS_FIFO_SIZE, sizeof(DSCSAMPLE));
        if (!scan_buffer)
        {
                fprintf(stderr, "Could not allocate memory.\n");
                return 1;
        }
        
	//=========================================================================
	// I. DRIVER INITIALIZATION
	//
	//    Initializes the DSCUD library.
	//
	//=========================================================================

	if (dscInit(DSC_VERSION) != DE_NONE)
	{
		dscGetLastError(&errorParams);
		fprintf(stderr, "dscInit error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
		return 1;
	}

	//=========================================================================
	// II. BOARD INITIALIZATION
	//
	//	   Initialize the HELIOS board. This function passes the various
	//	   hardware parameters to the driver and resets the hardware.
	//
	//=========================================================================

	dsccb.base_address = HELIOS_DEFAULT_BASE_ADDRESS;
        dsccb.int_level = HELIOS_DEFAULT_IRQ;

	if (dscInitBoard(DSC_HELIOS, &dsccb, &dscb) != DE_NONE)
	{
		dscGetLastError(&errorParams);
		fprintf(stderr, "dscInitBoard error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
		return 1;
	}

	//=========================================================================
	// III. AD SETTINGS INITIALIZATION
	//
	//	    Initialize the structure containing the AD conversion settings and
	//		then pass it to the driver.
	//
	//=========================================================================

	memset(&dscadsettings, 0, sizeof(DSCADSETTINGS));

        // printf ( "Enter the polarity (0 for BIPOLAR, 1 for UNIPOLAR; default: 0): " );
    	dscadsettings.polarity = 0;
	// The Helios SBC only has a 10V physical range.
	dscadsettings.range = HELIOS_VOLTAGE_RANGE;
	// printf ( "Enter the gain\n(0 for GAIN 1, 1 for GAIN 2, 2 for GAIN 4, 3 for GAIN 8; default: 0): " );
    	dscadsettings.gain = 0;

	dscadsettings.load_cal = 1;
	dscadsettings.current_channel = 0;

	dscadsettings.scan_interval = SCAN_INTERVAL_20;
	//dscadsettings.addiff = SINGLE_ENDED;
	dscadsettings.addiff = DIFFERENTIAL;

	if (dscADSetSettings(dscb, &dscadsettings) != DE_NONE)
	{
		dscGetLastError(&errorParams);
		fprintf(stderr, "dscADSetSettings error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
		return 1;
        }

	//=========================================================================
	// IV. I/O INTERRUPT SETTINGS INITIALIZATION
	//
	//	   Initialize the structure containing the analog I/O interrupt
	//	   settings.
	//
	//	   NOTE: You must allocate space for the buffer holding the returned
	//		     sample values. Also, be generous in allocating storage.
	//			 Allocating insufficient memory to hold sample data will result
	//			 in improper behavior of the driver, such as hanging interrupt
	//			 operations or assertion errors.
	//
	//=========================================================================

	memset(&dscaioint, 0, sizeof(DSCAIOINT));

        dscaioint.num_conversions = HELIOS_FIFO_SIZE;
    	dscaioint.conversion_rate = HELIOS_HZ;
    	dscaioint.cycle = 1;
    	dscaioint.internal_clock  = 1;
    	dscaioint.low_channel = trigger_channel;
    	dscaioint.high_channel = trigger_channel;

	dscaioint.external_gate_enable = 0; // can enable it if need be
	dscaioint.internal_clock_gate = 0;   // can enable it if need be
	dscaioint.fifo_enab = 1;

    	dscaioint.fifo_depth = HELIOS_FIFO_SIZE;
    	dscaioint.dump_threshold = HELIOS_FIFO_SIZE;
        dscaioint.sample_values = scan_buffer;

	//=========================================================================
	// V. SAMPLING AND OUTPUT
	//
	//    Perform the actual sampling and then output the results. To calculate
	//	  the actual input voltages, we must convert the sample code (which
	//	  must be cast to a short to get the correct code) and then plug it
	//	  into one of the formulas located in the manual for your board (under
	//	  "A/D Conversion Formulas").
	//=========================================================================

	dscuserintfunction.func = (DSCUserInterruptFunction) MyUserInterruptFunction;
	dscuserintfunction.int_mode = USER_INT_AFTER;
	dscSetUserInterruptFunction(dscb, &dscuserintfunction);

	Start_Temperature_Sensor();

	if (dscADScanInt(dscb, &dscaioint) != DE_NONE)
	{
		dscGetLastError(&errorParams);
		fprintf(stderr, "dscADScanInt error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
		cancel = 1;
	}

	if (!cancel)
	{
		int UDParam = 0;
		int TCParam = 0;
		pthread_attr_init(&tattr);
		pthread_create( &UDPthread, &tattr, UDPFunction, &UDParam);
		while (UDPWait) usleep(10*MS);
		pthread_create( &TCPthread, &tattr, TCPFunction, &TCParam);
		while (TCPWait) usleep(10*MS);
		while (!cancel) dscSleep(100);
	}

	free(scan_buffer);
	dscFree();
	Stop_Temperature_Sensor();
	return 0;
}
