#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>
#include <fcntl.h>
#include <values.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "sensor-packet.h"
#include "sensor-manager.h"
#include "uStrainBS.h"
#include "phidget21.h"

#define PACKET_BUFFER_LEN 30
#define UDP_BUFFER_LEN 256

typedef struct SensorPacketBuffer
{
    SensorPacket lastPacket;
    struct timeval lastPacketTime;
    SensorPacket packets[PACKET_BUFFER_LEN];
    struct sockaddr_in lastFromADDR;
    unsigned short callbackChannelMask;
} SensorPacketBuffer;

typedef struct _SensorManager
{
    int                  socketFD;
    pthread_t            listenThread;
    int                  exitFlag;
    void               (*callBack)(struct timeval timestamp, float *values, int count);
    pthread_mutex_t      mutex;
    SensorPacketBuffer  *buffers;
    int                  numAllBuffers;
    int                  numCallbackBuffers;
    int                  callbackCount;
    char               **callbackNames;
    char               **callbackUnits;
    float               *callbackFloats;
    SensorPacketBuffer **callbackBuffers;
    int                 *callbackChannels;
    float             (**callbackEU)(float in, void *parameters);
    void               **callbackEUParameters;
    float               *callbackOffsets;
    int                  dataFlood;
} _SensorManager;

static void *listenThread(void *arg)
{
    SensorManager sm = (SensorManager)arg;
    char recv_buf[UDP_BUFFER_LEN];
    struct sockaddr_in fromADDR;
    socklen_t sizeOfFromADDR = sizeof(fromADDR);
    ssize_t recv_len;

    while (!sm->exitFlag)
    {
        pthread_mutex_lock(&sm->mutex);
        while ((recv_len = recvfrom(sm->socketFD, recv_buf, UDP_BUFFER_LEN, 0, (struct sockaddr *)&fromADDR, &sizeOfFromADDR)) > 0)
        {
            SensorPacket *packet = (SensorPacket *)recv_buf;
            SensorPacket_FromNetworkOrder(packet, recv_len);

            if (packet->header.dataLength + sizeof(SensorPacketHeader) != recv_len || recv_len > sizeof(SensorPacket))
            {
                char AddrStr[32];
                inet_ntop(AF_INET, &(fromADDR.sin_addr), AddrStr, 32);
                fprintf(stderr, "Throwing out bad length packet from %s\n", AddrStr);
		fprintf(stderr, "packet data length was %d\n", packet->header.dataLength);
                continue;
            }
    
            if (packet->header.packet_type == SP_TYPE_PHIDGET || packet->header.packet_type == SP_TYPE_USTRAIN ||
                packet->header.packet_type == SP_TYPE_XBEE    || packet->header.packet_type == SP_TYPE_SYNAPSE ||
                packet->header.packet_type == SP_TYPE_LEICA)
            {
                int i;
                if (packet->header.num_buffered_packets != PACKET_BUFFER_LEN)
                {
                    char AddrStr[32];
                    inet_ntop(AF_INET, &(fromADDR.sin_addr), AddrStr, 32);
                    fprintf(stderr, "Throwing out packet with different buffer length from %s\n", AddrStr);
                    continue;
                }
                for (i=0; i<sm->numAllBuffers; i++)
                    if (packet->header.serialNumber == sm->buffers[i].lastPacket.header.serialNumber)
                        break;
                if (i==sm->numAllBuffers)
                {
                    sm->numAllBuffers++;
                    sm->buffers = (SensorPacketBuffer *)realloc(sm->buffers, sizeof(SensorPacketBuffer) * sm->numAllBuffers);
                }
                memcpy(&sm->buffers[i].lastPacket, packet, recv_len);
                if (packet->header.packet_num < PACKET_BUFFER_LEN)
                    memcpy(sm->buffers[i].packets + packet->header.packet_num, packet, recv_len);
                sm->buffers[i].lastFromADDR = fromADDR;
                gettimeofday(&(sm->buffers[i].lastPacketTime), NULL);
            }
            else if (packet->header.packet_type!=SP_TYPE_PULSE)
            {
                char AddrStr[32];
                inet_ntop(AF_INET, &(fromADDR.sin_addr), AddrStr, 32);
                fprintf(stderr, "Throwing out unknown packet type from %s\n", AddrStr);
                continue;
            }
            if (packet->header.packet_type==SP_TYPE_PULSE || sm->dataFlood)
            {
                int i, j, k=0;
                struct timeval tv;
                gettimeofday(&tv, NULL);

                for (i=0; i<sm->numCallbackBuffers; i++)
                {
                    int m=0;
                    for (j=0; j<16; j++)
                    {
                        if (sm->buffers[i].callbackChannelMask & (1 << j))
                        {
                            // j is channel number that needs to be transmitted.
                            if (sm->buffers[i].lastPacket.header.channelMask & (1 << j) && sm->buffers[i].lastPacketTime.tv_sec + 4 > tv.tv_sec)
                            {
                                float v;
                                if (sm->buffers[i].lastPacket.header.dataType == 1)
                                    v = sm->buffers[i].lastPacket.data.floats[m] + sm->callbackOffsets[k];
                                else
                                    v = sm->buffers[i].lastPacket.data.uShorts[m] + sm->callbackOffsets[k];
                                if (sm->callbackEU[k])
                                    sm->callbackFloats[k] = sm->callbackEU[k](v, sm->callbackEUParameters[k]);
                                else
                                    sm->callbackFloats[k] = v;
                                m++;
                            }
                            else
                            {
                                sm->callbackFloats[k] = MAXFLOAT;
                            }
                            k++;
                        }
                        else
                        {
                            // j is not a channel to be transmitted
                            if (sm->buffers[i].lastPacket.header.channelMask & (1 << j))
                            {
                                m++;
                            }
                        }
                    }
                }
                if (sm->callbackNames && sm->callBack)
                    sm->callBack(tv, sm->callbackFloats, sm->callbackCount);
            }
        }
        pthread_mutex_unlock(&sm->mutex);
        usleep(10000);
    }
    return NULL;
}

SensorManager SensorManager_create(unsigned short port)
{
    struct sockaddr_in bindADDR;
    SensorManager sm = (SensorManager)malloc(sizeof(_SensorManager));
    if (!sm)
    {
        perror("SensorManager_create in malloc:");
        return NULL;
    }
    sm->exitFlag = 0;
    sm->callBack = NULL;
    pthread_mutex_init(&sm->mutex, NULL);
    sm->buffers = NULL;
    sm->numAllBuffers = 0;
    sm->numCallbackBuffers = 0;
    sm->callbackCount = 0;
    sm->callbackNames = NULL;
    sm->callbackUnits = NULL;
    sm->callbackFloats = NULL;
    sm->callbackOffsets = NULL;
    sm->callbackBuffers = NULL;
    sm->callbackChannels = NULL;
    sm->callbackEU = NULL;
    sm->callbackEUParameters = NULL;
    sm->dataFlood = 0;
        if ((sm->socketFD = socket(AF_INET,SOCK_DGRAM,IPPROTO_IP)) <  0)
        {
                perror("SensorManager_create in socket");
        free(sm);
        return NULL;
        }
    if (fcntl(sm->socketFD, F_SETFL, O_NONBLOCK))
    {
                perror("SensorManager_create in fcntl");
                free(sm);
                return NULL;
    }
    memset(&bindADDR,0,sizeof(bindADDR));
    bindADDR.sin_family = AF_INET;
    bindADDR.sin_addr.s_addr = htonl(INADDR_ANY);
    bindADDR.sin_port = htons(port);
    if (bind(sm->socketFD, (struct sockaddr *)&bindADDR, sizeof(bindADDR)) < 0)
    {
        perror("SensorManager_create in bind");
        free(sm);
        return NULL;
    }
    if (pthread_create(&sm->listenThread, NULL, listenThread, sm) != 0)
    {
        perror("SensorManager_create in pthread_create");
        free(sm);
        return NULL;
    }
    return sm;
}

void SensorManager_destroy(SensorManager sensorManager)
{
    int i;
    sensorManager->exitFlag = 1;
    sensorManager->callBack = NULL;
    pthread_join(sensorManager->listenThread, NULL);
    close(sensorManager->socketFD);
    free(sensorManager->buffers);
    for (i=0; i<sensorManager->callbackCount; i++)
        free(sensorManager->callbackNames[i]);
    free(sensorManager->callbackNames);
    free(sensorManager->callbackUnits);
    free(sensorManager->callbackFloats);
    free(sensorManager->callbackOffsets);
    free(sensorManager->callbackBuffers);
    free(sensorManager->callbackChannels);
    free(sensorManager->callbackEU);
    free(sensorManager->callbackEUParameters);
    free(sensorManager);
}

void SensorManager_getNamesAndUnits(SensorManager sensorManager, char ***names, char ***units, int *count)
{
    int i, k=0;

    pthread_mutex_lock(&sensorManager->mutex);

    for (i=0; i<sensorManager->callbackCount; i++)
        free(sensorManager->callbackNames[i]);
    free(sensorManager->callbackNames);
    free(sensorManager->callbackUnits);
    free(sensorManager->callbackFloats);
    free(sensorManager->callbackOffsets);
    free(sensorManager->callbackBuffers);
    free(sensorManager->callbackChannels);
    free(sensorManager->callbackEU);
    free(sensorManager->callbackEUParameters);

    sensorManager->callbackCount = 0;
    sensorManager->numCallbackBuffers = sensorManager->numAllBuffers;

    for (i=0; i<sensorManager->numCallbackBuffers; i++)
    {
        int j;
        sensorManager->buffers[i].callbackChannelMask = sensorManager->buffers[i].lastPacket.header.channelMask;
        for (j=0; j<16; j++)
            if (sensorManager->buffers[i].callbackChannelMask & (1 << j))
                sensorManager->callbackCount++;
    }

    sensorManager->callbackNames    = (char **)malloc(sensorManager->callbackCount * sizeof(char *));
    sensorManager->callbackUnits    = (char **)malloc(sensorManager->callbackCount * sizeof(char *));
    sensorManager->callbackFloats   = (float *)malloc(sensorManager->callbackCount * sizeof(float));
    sensorManager->callbackOffsets  = (float *)malloc(sensorManager->callbackCount * sizeof(float));
    sensorManager->callbackBuffers  = (SensorPacketBuffer **)malloc(sensorManager->callbackCount * sizeof(SensorPacketBuffer *));
    sensorManager->callbackChannels = (int *)malloc(sensorManager->callbackCount * sizeof(int));
    sensorManager->callbackEU       = malloc(sensorManager->callbackCount * sizeof(void *));
    sensorManager->callbackEUParameters = (void **)malloc(sensorManager->callbackCount * sizeof(void *));

    for (i=0; i<sensorManager->numCallbackBuffers; i++)
    {
        char buf[64];
        int j;

        for (j=0; j<16; j++)
        {
            if (sensorManager->buffers[i].callbackChannelMask & (1 << j))
            {
                if (sensorManager->buffers[i].lastPacket.header.packet_type == SP_TYPE_XBEE)
                    sprintf(buf, "%X-%d", sensorManager->buffers[i].lastPacket.header.serialNumber, j);
                else if (sensorManager->buffers[i].lastPacket.header.packet_type == SP_TYPE_SYNAPSE)
                    sprintf(buf, "%X-%d", sensorManager->buffers[i].lastPacket.header.serialNumber, j);
                else
                    sprintf(buf, "%d-%d", sensorManager->buffers[i].lastPacket.header.serialNumber, j);
                sensorManager->callbackNames[k] = strdup(buf);
                if (sensorManager->buffers[i].lastPacket.header.packet_type == SP_TYPE_LEICA)
                    sensorManager->callbackUnits[k] = "inches";
                else if (sensorManager->buffers[i].lastPacket.header.deviceClass == PHIDCLASS_TEMPERATURESENSOR)
                    sensorManager->callbackUnits[k] = "degC";
                else if (sensorManager->buffers[i].lastPacket.header.deviceClass == PHIDCLASS_BRIDGE)
                    sensorManager->callbackUnits[k] = "mV/V";
                else if (sensorManager->buffers[i].lastPacket.header.deviceClass == PHIDCLASS_INTERFACEKIT)
                    sensorManager->callbackUnits[k] = "bits";
                else if (sensorManager->buffers[i].lastPacket.header.packet_type == SP_TYPE_XBEE)
                    sensorManager->callbackUnits[k] = "bits";
                else if (sensorManager->buffers[i].lastPacket.header.packet_type == SP_TYPE_SYNAPSE)
                    sensorManager->callbackUnits[k] = "bits";
		else if (sensorManager->buffers[i].lastPacket.header.packet_type == SP_TYPE_USTRAIN &&
			 sensorManager->buffers[i].lastPacket.header.deviceClass == 2427)
		    sensorManager->callbackUnits[k] = "degC";
		else if (sensorManager->buffers[i].lastPacket.header.packet_type == SP_TYPE_USTRAIN &&
			 sensorManager->buffers[i].lastPacket.header.deviceClass == 2428)
		    sensorManager->callbackUnits[k] = "bits";
		else if (sensorManager->buffers[i].lastPacket.header.packet_type == SP_TYPE_USTRAIN &&
			 sensorManager->buffers[i].lastPacket.header.deviceClass == 2417)
		    sensorManager->callbackUnits[k] = "bits";
                else
                    sensorManager->callbackUnits[k] = "Unknown";
                sensorManager->callbackBuffers[k] = sensorManager->buffers + i;
                sensorManager->callbackChannels[k] = j;
                sensorManager->callbackEU[k] = NULL;
                sensorManager->callbackEUParameters[k] = NULL;
                sensorManager->callbackOffsets[k] = 0;
                k++;
            }
        }
    }
    
    *names = sensorManager->callbackNames;
    *units = sensorManager->callbackUnits;
    *count = sensorManager->callbackCount;

    pthread_mutex_unlock(&sensorManager->mutex);
}

void SensorManager_setCallback(SensorManager sensorManager, void(*fptr)(struct timeval timestamp, float *values, int count))
{
    pthread_mutex_lock(&sensorManager->mutex);
    sensorManager->callBack = fptr;
    pthread_mutex_unlock(&sensorManager->mutex);
}

void SensorManager_clearCallback(SensorManager sensorManager)
{
    pthread_mutex_lock(&sensorManager->mutex);
    sensorManager->callBack = NULL;
    pthread_mutex_unlock(&sensorManager->mutex);
}

void SensorManager_nullSensors(SensorManager sensorManager, char *commaSeparatedNames)
{
    int i;
    char *saveptr, *token, *localCopy, *firstArg;
    
    localCopy = strdup(commaSeparatedNames);
    for (firstArg = localCopy; ; firstArg = NULL)
    {
        token = strtok_r(firstArg, ",", &saveptr);
        if (!token) break;
        for (i=0; i<sensorManager->callbackCount; i++)
            if (!strcmp(token, sensorManager->callbackNames[i])) break;
        if (i!=sensorManager->callbackCount)
            SensorManager_nullSensor(sensorManager, i);
    }
    free(localCopy);
}

void SensorManager_nullSensor(SensorManager sensorManager, int index)
{
    if (index < 0 || index >= sensorManager->callbackCount)
    {
        fprintf(stderr, "Bad sensor index %d\n", index);
    }
    else
    {
        float v;
        if (sensorManager->callbackBuffers[index]->lastPacket.header.dataType == 1)
            v = sensorManager->callbackBuffers[index]->lastPacket.data.floats[sensorManager->callbackChannels[index]];
        else
            v = sensorManager->callbackBuffers[index]->lastPacket.data.uShorts[sensorManager->callbackChannels[index]];

        sensorManager->callbackOffsets[index] -= v;
    }
}

void SensorManager_floodCallback(SensorManager sensorManager)
{
    pthread_mutex_lock(&sensorManager->mutex);
    sensorManager->dataFlood = 1;
    pthread_mutex_unlock(&sensorManager->mutex);
}

void SensorManager_setRate(SensorManager sensorManager, int index, unsigned short Hz)
{
    if (index < 0 || index >= sensorManager->callbackCount)
    {
        fprintf(stderr, "Bad sensor index %d\n", index);
    }
    else
    {
        struct sockaddr_in lastFromADDR = sensorManager->callbackBuffers[index]->lastFromADDR;
        SensorPacketHeader header = sensorManager->callbackBuffers[index]->lastPacket.header;
        header.packet_cmd = SP_CMD_SET_RATE;
        header.channelMask = Hz;
        header.dataLength = 0;
        sendto(sensorManager->socketFD, &header, sizeof(SensorPacketHeader), 0,
            (struct sockaddr *)&lastFromADDR, sizeof(lastFromADDR));
    }
}

void SensorManager_setThermocoupleType(SensorManager sensorManager, int index, unsigned char ThermocoupleType)
{
    if (index < 0 || index >= sensorManager->callbackCount)
    {
        fprintf(stderr, "Bad sensor index %d\n", index);
    }
    else
    {
        struct sockaddr_in lastFromADDR = sensorManager->callbackBuffers[index]->lastFromADDR;
        SensorPacketHeader header = sensorManager->callbackBuffers[index]->lastPacket.header;
        header.packet_cmd = SP_CMD_SET_TYPE;
        header.channelMask = 1<<sensorManager->callbackChannels[index];
        header.dataType = ThermocoupleType;
        header.dataLength = 0;
        sendto(sensorManager->socketFD, &header, sizeof(SensorPacketHeader), 0,
            (struct sockaddr *)&lastFromADDR, sizeof(lastFromADDR));
    }
}

void SensorManager_setEUfunction(SensorManager sensorManager, int index, char *newUnits, float(*fptr)(float in, void *parameters), void *parameters)
{
    if (index < 0 || index >= sensorManager->callbackCount)
    {
        fprintf(stderr, "Bad sensor index %d\n", index);
    }
    else
    {
        pthread_mutex_lock(&sensorManager->mutex);
        sensorManager->callbackEU[index] = fptr;
        sensorManager->callbackEUParameters[index] = parameters;
        sensorManager->callbackUnits[index] = newUnits;
        pthread_mutex_unlock(&sensorManager->mutex);
    }
}
