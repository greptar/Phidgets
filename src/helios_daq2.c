//=============================================================================
// (c) Copyright 2009 Diamond Systems Corporation. Use of this source code
// is subject to the terms of Diamond Systems' Software License Agreement.
// Diamond Systems provides no warranty of proper performance if this
// source code is modified.
//
// File: DSCADSampleInt.c   v1.00
//
// Depedency :	Universal Driver version 6.02
//				DSCUD.H - Include file.
//				DSCUDBCL.LIB - Library file for MS-DOS
//						( USE BC 5.02 compiler )
//				LIBDSCUD_6.02_1.a	- Library file for Linux 2.6.23
//						( USE GCC 3.xx compiler )
//				DSCUD.LIB	- Static library file for Windows XP
//						( USE VC++ 6.0 or Visual Studio 2005 )
//				DSCUD_API.LIB	- Static library for Windows CE 6.0
//						( Use Visual Studio 2005 with Platform Builder 6.0 )
// Description: This sample code demonstrates the usage of UD function
//				dscADSampleInt(). This sample code describes the usage
//				for Helios SBC. The driver will use the incumbent OS
//              in conjunction with the interrupt selected to transfer many
//              values from the A/D converter to user space, and once stopped,
//              will display a single set of those channel values,
//              interpretting them based on the mode given by the user.
// ************************* History ********************
// Version		Engineer		Date		Description
// 2.0                  Nathan Wharton          12/12/11        Schafer version
// 1.0			James Moore		07/31/09	Created.
//=============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>

#include "dscud.h"

#define HELIOS_DEFAULT_BASE_ADDRESS 0x280
#define HELIOS_DEFAULT_IRQ 5
#define HELIOS_VOLTAGE_RANGE RANGE_10

#define NUM_PHIDGETS_TEMPS 4
#define MAX_PHIDGETS_RATE 50

#define FIFO_MINIMUM 1
#define FIFO_NOFACTOR_MAX 48
#define FIFO_FACTOR_MAX 1024
#define FIFO_FACTOR 8

// command line variables.  Set to example values
static char *sendto_address = "224.0.0.100";
static int   sendto_port = 9942;
static int   polarity = 0;
static int   differ = 1;
static int   gain = 0;
static int   scan_int = 20;
static int   oversample = 1;
static int   num_chan = 8;
static int   hz = 1000;
static int   fifo_size = 2000;
static int   buffer_seconds = 30;

// Calculated from command line
static int scans_per_fifo;
static int microseconds_per_fifo;

typedef struct PacketHeader
{
//	short packet_type;
//	short packet_subtype;
	short packet_num;
	short num_channels;
	short num_scans;
	short num_buffered_packets;
	short hz;
	short oversample;
	struct timeval time;
} PacketHeader;
static void *DAQ_Packet_Buffer;

static unsigned int num_buffered_packets;
static unsigned int buffer_pos;

static int cancel = 0;
static void sighandle(int signum)
{
	cancel = 1;
}

static DSCB dscb;                   // handle used to refer to the board
static DSCADSETTINGS dscadsettings; // structure containing A/D conversion settings
static DSCAIOINT dscaioint;         // structure containing interrupt based acquire settings
static int sd = 0;
static struct sockaddr_in saddr;
static ssize_t daq_bytes_to_send, temperature_bytes_to_send, sendto_result;
void MyUserInterruptFunction(void* param)
{
	DFLOAT voltage;
	struct timeval irq_tv;
        int i;
        static int fifo_count = 0;
	static float daq_sums[FIFO_FACTOR_MAX];
	PacketHeader *daq_packet = (PacketHeader *)(DAQ_Packet_Buffer + (buffer_pos * daq_bytes_to_send));
	float *daq_samples = (float *)((void *)daq_packet + sizeof(PacketHeader));

	gettimeofday(&irq_tv, NULL);
	irq_tv.tv_usec -= microseconds_per_fifo + 4353;
	while (irq_tv.tv_usec < 0) irq_tv.tv_usec += 1000000 , irq_tv.tv_sec -= 1;

        if (oversample > 1)
        {
            int start_scan = fifo_count * scans_per_fifo;
            for (i=0; i<fifo_size; i++)
            {
                int current_scan = start_scan + (i / num_chan);
                int current_chan = i % num_chan;

		dscADCodeToVoltage(dscb, dscadsettings, dscaioint.sample_values[i], &voltage);

                if (current_scan % oversample == 0)
                {
                    daq_sums[((current_scan/oversample) * num_chan) + current_chan] = voltage;
                }
                else
                {
                    daq_sums[((current_scan/oversample) * num_chan) + current_chan] += voltage;
                }
            }
            fifo_count++;
            if (fifo_count == oversample)
            {
                daq_packet->time = irq_tv;
                fifo_count = 0;
                for (i=0; i<fifo_size; i++)
                    daq_samples[i] = daq_sums[i] / oversample;
                sendto_result = sendto(sd, daq_packet, daq_bytes_to_send, 0, (struct sockaddr *)&saddr, sizeof saddr);
                if (sendto_result != daq_bytes_to_send)
                {
                    perror("sendto");
                    cancel = 1;
                }
                buffer_pos++;
            }
        }
        else
        {
            daq_packet->time = irq_tv;
	    for (i=0; i<fifo_size; i++)
	    {
		dscADCodeToVoltage(dscb, dscadsettings, dscaioint.sample_values[i], &voltage);
		daq_samples[i] = voltage;
	    }
            sendto_result = sendto(sd, daq_packet, daq_bytes_to_send, 0, (struct sockaddr *)&saddr, sizeof saddr);
            if (sendto_result != daq_bytes_to_send)
            {
                perror("sendto");
                cancel++;
            }
            buffer_pos++;
        }
        if (buffer_pos == num_buffered_packets) buffer_pos = 0;
}

int main(int argc, char **argv)
{
	DSCCB dsccb;                 // structure containing board settings (IRQ and Address)
	ERRPARAMS errorParams;       // structure for returning error code and error string
	DSCUSERINTFUNCTION dscuserintfunction;
	DSCSAMPLE *scan_buffer;
	int i, bad_command_line = 0;
	float fifos_per_second, packets_per_second;

	signal(SIGINT, &sighandle);

        //=========================================================================
        // 0. Read Command Line
        //=========================================================================

	if (argc != 8)
	{
		bad_command_line++;
	}
	else
	{
		sendto_address = argv[1];
		if (inet_addr(sendto_address) == INADDR_NONE)
			bad_command_line++, fprintf(stderr, "Bad address\n");

        	if (sscanf(argv[2], "%d", &sendto_port) != 1)
                	bad_command_line++, fprintf(stderr, "Bad port\n");
        	else
                	if (sendto_port < 1025 || sendto_port > 65534)
                        	bad_command_line++, fprintf(stderr, "Port out of range\n");

                if (sscanf(argv[3], "%d", &gain) != 1)
                        bad_command_line++, fprintf(stderr, "Bad gain\n");
                else
                        if (gain < 0 || gain > 3)
                                bad_command_line++, fprintf(stderr, "Gain should be 0 to 3\n");

                if (sscanf(argv[4], "%d", &scan_int) != 1)
                        bad_command_line++, fprintf(stderr, "Bad Dwell\n");
                else
                        if (scan_int != 4 && scan_int != 5 && scan_int != 9 && scan_int != 10 && scan_int != 15 && scan_int != 20)
                                bad_command_line++, fprintf(stderr, "Dwell is either 4,5,9,10,15,or 20.  Units are microseconds.\n");

                if (sscanf(argv[5], "%d", &oversample) != 1)
                        bad_command_line++, fprintf(stderr, "Bad oversample\n");
                else
                        if (oversample < 1)
                                bad_command_line++, fprintf(stderr, "Oversample should be 1 or more\n");

                if (sscanf(argv[6], "%d", &num_chan) != 1)
                        bad_command_line++, fprintf(stderr, "Bad Number of Channels\n");
                else
                        if (num_chan < 1 || num_chan > 8)
                                bad_command_line++, fprintf(stderr, "Channels should be 1 to 8\n");

                if (sscanf(argv[7], "%d", &hz) != 1)
                        bad_command_line++, fprintf(stderr, "Bad hz\n");
                else
                        if (hz < 1 || hz > 100000)
                                bad_command_line++, fprintf(stderr, "Hz should be 1 to 100000\n");

	}
	if (bad_command_line)
	{
		fprintf(stderr, "Usage: %s <ip_address> <port> <gain> <dwell> <oversample> <num_chan> <hz>\n", argv[0]);
		return 1;
	}

        //=========================================================================
	// 0.1 Calculations from command line
        //=========================================================================

	fifo_size = num_chan * hz; // Start out at trying to get 1 second per fifo, and go down from there
        if (fifo_size > FIFO_FACTOR_MAX)
	{
		fifo_size = FIFO_FACTOR_MAX;
	}
        else if (fifo_size > FIFO_NOFACTOR_MAX)
        {
                fifo_size -= fifo_size % FIFO_FACTOR; // Make it a multiple of FIFO_FACTOR
        }

	while (fifo_size % num_chan)
	{
		if (fifo_size > FIFO_NOFACTOR_MAX)
			fifo_size -= FIFO_FACTOR;
		else
			fifo_size -= 1;
	}
	fprintf(stderr, "FIFO size is %d\n", fifo_size);

	scans_per_fifo = fifo_size / num_chan;

	scan_buffer = calloc(fifo_size, sizeof(DSCSAMPLE));
        if (!scan_buffer)
        {
                fprintf(stderr, "Could not allocate memory.\n");
                return 1;
        }

        if ((sd = socket(AF_INET,SOCK_DGRAM,0)) <  0)
        {
                perror("send socket");
                exit(1);
        }
        memset(&saddr,0,sizeof(saddr));
        saddr.sin_family = AF_INET;
        saddr.sin_addr.s_addr = inet_addr(sendto_address);
        saddr.sin_port = htons(sendto_port);

       	daq_bytes_to_send = sizeof(PacketHeader) + sizeof(float)*fifo_size;
       	temperature_bytes_to_send = sizeof(PacketHeader) + sizeof(float)*(NUM_PHIDGETS_TEMPS+1);
        
	fifos_per_second = (float)hz / (float)scans_per_fifo;
	microseconds_per_fifo = 1000000 * (1/fifos_per_second);
	packets_per_second = fifos_per_second / oversample;
	num_buffered_packets = (int)(packets_per_second * buffer_seconds);
	if (num_buffered_packets<2) num_buffered_packets = 2;
	fprintf(stderr, "%f packets per second at %d bytes per packet\n", packets_per_second, daq_bytes_to_send);
	fprintf(stderr, "%d buffered packets\n", num_buffered_packets);
	DAQ_Packet_Buffer= calloc(num_buffered_packets, daq_bytes_to_send);
	if (!DAQ_Packet_Buffer)
	{
		fprintf(stderr, "Could not allocate memory.\n");
		return 1;
	}
	buffer_pos = 0;
        for (i=0; i<num_buffered_packets; i++)
        {
	    PacketHeader *daq_packet = (PacketHeader *)(DAQ_Packet_Buffer + i * daq_bytes_to_send);
            daq_packet->packet_num = i;
            daq_packet->num_channels = num_chan;
            daq_packet->num_scans = scans_per_fifo;
            daq_packet->num_buffered_packets = num_buffered_packets;
            daq_packet->hz = hz;
            daq_packet->oversample = oversample;
        }

	//=========================================================================
	// I. DRIVER INITIALIZATION
	//
	//    Initializes the DSCUD library.
	//
	//=========================================================================

	if (dscInit(DSC_VERSION) != DE_NONE)
	{
		dscGetLastError(&errorParams);
		fprintf(stderr, "dscInit error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
		return 1;
	}

	//=========================================================================
	// II. BOARD INITIALIZATION
	//
	//	   Initialize the HELIOS board. This function passes the various
	//	   hardware parameters to the driver and resets the hardware.
	//
	//=========================================================================

	dsccb.base_address = HELIOS_DEFAULT_BASE_ADDRESS;
        dsccb.int_level = HELIOS_DEFAULT_IRQ;

	if (dscInitBoard(DSC_HELIOS, &dsccb, &dscb) != DE_NONE)
	{
		dscGetLastError(&errorParams);
		fprintf(stderr, "dscInitBoard error: %s %s\n",
			dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
		return 1;
	}

	//=========================================================================
	// III. AD SETTINGS INITIALIZATION
	//
	//	    Initialize the structure containing the AD conversion settings and
	//		then pass it to the driver.
	//
	//=========================================================================

	memset(&dscadsettings, 0, sizeof(DSCADSETTINGS));

        // printf ( "Enter the polarity (0 for BIPOLAR, 1 for UNIPOLAR; default: 0): " );
    	dscadsettings.polarity = polarity;
	// The Helios SBC only has a 10V physical range.
	dscadsettings.range = HELIOS_VOLTAGE_RANGE;
	// printf ( "Enter the gain\n(0 for GAIN 1, 1 for GAIN 2, 2 for GAIN 4, 3 for GAIN 8; default: 0): " );
    	dscadsettings.gain = gain;

	dscadsettings.load_cal = 1;
	dscadsettings.current_channel = 0;

	if (scan_int == 4) dscadsettings.scan_interval = SCAN_INTERVAL_4;
	if (scan_int == 5) dscadsettings.scan_interval = SCAN_INTERVAL_5;
	if (scan_int == 9) dscadsettings.scan_interval = SCAN_INTERVAL_9;
	if (scan_int == 10) dscadsettings.scan_interval = SCAN_INTERVAL_10;
	if (scan_int == 15) dscadsettings.scan_interval = SCAN_INTERVAL_15;
	if (scan_int == 20) dscadsettings.scan_interval = SCAN_INTERVAL_20;
	dscadsettings.addiff = differ;

	if (dscADSetSettings(dscb, &dscadsettings) != DE_NONE)
	{
		dscGetLastError(&errorParams);
		fprintf(stderr, "dscADSetSettings error: %s %s\n",
			dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
		return 1;
        }

	//=========================================================================
	// IV. I/O INTERRUPT SETTINGS INITIALIZATION
	//
	//	   Initialize the structure containing the analog I/O interrupt
	//	   settings.
	//
	//	   NOTE: You must allocate space for the buffer holding the returned
	//		     sample values. Also, be generous in allocating storage.
	//			 Allocating insufficient memory to hold sample data will result
	//			 in improper behavior of the driver, such as hanging interrupt
	//			 operations or assertion errors.
	//
	//=========================================================================

	memset(&dscaioint, 0, sizeof(DSCAIOINT));

        dscaioint.num_conversions = fifo_size;
    	dscaioint.conversion_rate = hz;
    	dscaioint.cycle = 1;
    	dscaioint.internal_clock  = 1;
    	dscaioint.low_channel = 0;
    	dscaioint.high_channel = num_chan - 1;

	dscaioint.external_gate_enable = 0; // can enable it if need be
	dscaioint.internal_clock_gate = 0;   // can enable it if need be
	dscaioint.fifo_enab = 1;

    	dscaioint.fifo_depth = fifo_size;
    	dscaioint.dump_threshold = fifo_size;
        dscaioint.sample_values = scan_buffer;

	//=========================================================================
	// V. SAMPLING AND OUTPUT
	//
	//    Perform the actual sampling and then output the results. To calculate
	//	  the actual input voltages, we must convert the sample code (which
	//	  must be cast to a short to get the correct code) and then plug it
	//	  into one of the formulas located in the manual for your board (under
	//	  "A/D Conversion Formulas").
	//=========================================================================

	dscuserintfunction.func = (DSCUserInterruptFunction) MyUserInterruptFunction;
	dscuserintfunction.int_mode = USER_INT_AFTER;
	dscSetUserInterruptFunction(dscb, &dscuserintfunction);
	// Start_Temperature_Sensor();

	if (dscADScanInt( dscb, &dscaioint) != DE_NONE)
	{
		dscGetLastError(&errorParams);
		fprintf(stderr, "dscADScanInt error: %s %s\n",
			dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
		cancel = 1;
	}

	while (!cancel)
	{
		dscSleep(750);
	}

	free(scan_buffer);
	dscFree();
	// Stop_Temperature_Sensor();
	return 0;
}
