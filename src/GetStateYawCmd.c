#include <stdio.h>
#include <stdlib.h>
#include <math.h> /* exp, signbit */
//#include "matrix.h"
#define NUM_RINGS 20
#define DITS_PER_RING 6
#define TValue .02				//  20 millisecond time step
#define TauValue 0.125			// Time Constant of the Control Loop
#define ObsTauValue 0.06		// Time Constant of the Observer
#define MincValue 0.0			// Moment Increment Value for thresholding
#define IValue 0.6				// Moment of Inertia of the Test Apparatus
#define UpperAngleLimit 0.5		// Error Limit
#define LowerAngleLimit -0.5	// Error Limit
#define UpperRateLimit 0.2		// Error Limit
#define LowerRateLimit -0.2		// Error Limit
#define InitStepCount 10		// Initialization Step Count
#define TF 1.0					// Tuning Factor

// I expect this to be done elsewhere in the embedded code.
unsigned char motor_presence[NUM_RINGS][DITS_PER_RING] = { { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
                                                           { 1, 1, 1, 1, 1, 1 } ,
														   { 1, 1, 1, 1, 1, 1 } } ;

// These variables must persist from time step to time step.
// Should be initialized with these values.
float Ud = 0;
float C = 0.5;		// This is a scar
int init_count = 0;
float angle = 0;
float rate = 0;

static int ObserveInertialState(float previous_angle, float previous_rate, float sensed_angle, float UdIn, float CIn, float *OutAngle, float *OutRate) {
	// This is a two-state, Static Kalman Filter

// These matrices are particular to the Yaw Test Assembly:
	static float A[2][2] = { { 1 , TValue } , { 0 , 1 } };
    static float B[2][2] = { { TValue , -TValue } , { 1 , 0 } };
//	static float Cmat[2] = { 1 , 0 };

// Here, poles are chosen directly, instead of time-varying Kalman gain.
	static float T = TValue;
	static float Tau = ObsTauValue;       // Not the same as the controler time constant
	float alpha = -2*exp(-T/Tau);
	float beta = exp(-2*T/Tau);
	float K1 = -(2 + alpha);
	float K2 = -(1 + alpha + beta)/T;

// Compute Xhat of k+1 given k
//	float yHat = Cmat[0]*current_angle + Cmat[1]*current_rate;

// The observer, or filter:
	float AngleCorrection = K1*(sensed_angle - previous_angle);
	float RateCorrection = K2*(sensed_angle - previous_angle);
	float AnglePrediction = A[0][0]*previous_angle + A[0][1]*previous_rate + B[0][0]*UdIn + B[0][1]*CIn*UdIn;
	float RatePrediction = A[1][0]*previous_angle + A[1][1]*previous_rate + B[1][0]*UdIn + B[1][1]*CIn*UdIn;
	*OutAngle = AnglePrediction - AngleCorrection;
	*OutRate = RatePrediction - RateCorrection;
	return 0;

}

// "ground" is Ring, return is "solution_found"
static int get_motor_to_fire(float current_angle, float target_angle, float target_rate, int *ground, int *power, float *DataLog) {


	// This array is to be scaled according to the scale factor
	static float Ms[] =     {   -0.017880428,
								-0.015998278,
								-0.014116128,
								-0.012233977,
								-0.010351827,
								-0.008469677,
								-0.006587526,
								-0.004705376,
								-0.002823226,
								-0.000941075,
								 0.000941075,
								 0.002823226,
								 0.004705376,
								 0.006587526,
								 0.008469677,
								 0.010351827,
								 0.012233977,
								 0.014116128,
								 0.015998278,
								 0.017880428 };

//	static float Iz = IValue;          /* Moment of Inertia */
	static float T = TValue;         /* 20 milliseconds */
	static float Tau = TauValue;       // Time Constant
	static float Minc = MincValue;		// Moment Increment

	// Controller:
	float alpha = -2*exp(-T/Tau);
	float beta = exp(-2*T/Tau);

	// These two are the "conventional" State Feedback Gains
	float Kc1 = -(alpha + beta + 1)/T;
	float Kc2 = beta/2 -1.5 -alpha/2;
	float YTD = 0;

	int solution_found = 0;

	float previous_angle = 0;
	float previous_rate = 0;

	previous_angle = angle;
	previous_rate = rate;

	// Return unreasonable numbers by default
	*ground = 21;
	*power = 21;


	if (0 == init_count) {
			angle = current_angle;
	}
	else {
		solution_found = ObserveInertialState(previous_angle, previous_rate, current_angle , Ud, C, &angle, &rate);
	}

	if ((angle > UpperAngleLimit) || (angle < LowerAngleLimit) || (rate > UpperRateLimit) || (rate < LowerRateLimit)) {
		solution_found = -1;	// error condition
	}

	if (init_count < InitStepCount) {
		init_count++;
	}

	// Generate the Command!
	else if (0 == solution_found) {
		float E1 = angle - target_angle;
		float E2 = rate - target_rate;

		float SmallestDelta = 1000.00;   // Arbitrarily Large
		float Delta = 0.0;
		float Udit = 0.0;
		int rit = 0;
		int cit = 0;

//		YTD = Iz*(Kc1*E1 + Kc2*E2);		// Torque Desired (Ideal)
		YTD = TF*(Kc1*E1 + Kc2*E2);		// Torque Desired (Ideal)

		//for (rit = 0; rit < NUM_RINGS; rit++ ) {
		//		for (cit = 0; cit < DITS_PER_RING; cit++) {
		//			mexPrintf(" %d ",motor_presence[rit][cit]);
		//		}
		//		mexPrintf("\n");
		//	}

		Ud = 0;							// Will be the actual torque
		for (rit = 0; rit < NUM_RINGS; rit++ ) {
			Udit = Ms[rit];
			if (Udit*YTD > 0) {    // Testing the sign
				if (fabs(YTD) > (fabs(Udit) - Minc)) {
					Delta = fabs(Udit - YTD);
					for (cit = 0; cit < DITS_PER_RING; cit++) {
						if (1 == motor_presence[rit][cit]) {
							if (Delta < SmallestDelta) {
								SmallestDelta = Delta;
								*ground = rit;
								*power = cit;
								solution_found = 1;
							}
						}
					}
				}
			}
		}
		if (1 == solution_found) {
			Ud = Ms[*ground];
			motor_presence[*ground][*power] = 0;
		}
	}
	*DataLog = YTD;
	*(DataLog+1) = angle;
	*(DataLog+2) = rate;
	return solution_found;
}


/* the intermediate function */
void MatLabToC(double *current_angle,double *target_angle,double *target_rate,double *RingFired,double *DitFired, double *solution_found, double *DataLog)
{
	int CRingFired = 0;
	int CDitFired = 0;
	int CSolutionFound = 0;
	float FDataLog[3] = {0, 0 , 0};

	CSolutionFound = get_motor_to_fire((float)*current_angle,(float)*target_angle,(float)*target_rate, &CRingFired, &CDitFired, FDataLog);

	*solution_found = (double)CSolutionFound;
	*RingFired = (double)CRingFired;
	*DitFired = (double)CDitFired;
	*DataLog = (double)FDataLog[0];
	*(DataLog + 1) = (double)FDataLog[1];
	*(DataLog + 2) = (double)FDataLog[2];
}

/* the gateway function */
// Interprets this Matlab call:
// [RingFired DitFired solution_found DataLog] = GetStateYawCmd(current_angle, target_angle, target_rate);
