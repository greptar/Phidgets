// WDC_EU.c includes a set of Engineering Unit Conversion Routines.
// It create the struct that maps all of the Sensor IDs to the appropriate
// Engineering Unit Conversion Routines.
// The EU Conversions are defined in a comma-delimted ascii file

// There are three types of EU Conversions:
// Strain Gage (SG)
// Slope/Offset (SO)
// Table Lookup (TL)

#include "WDC_EU.h"

// For Engineering Unit Conversion
static EUConvRecord EUConv[MAXEUFLOATS];

static char Line[EUBUFSIZE];

// This is the conversion routine for SG (Strain Gage)
// Conversion is from mv/V to microstrain
// Parameter is a Gage Factor
static float BridgeEU(float in, void *parameter)
{
    float val = (in/1000.0);
	float sF = *(float*)parameter;
	#ifdef EUDiagnose
	printf("BridgeEU Called with in = %f, sf = %f\n",in,sF);
	#endif
    return (4*val/(sF*(1+2*val))) * 1000000;
}

// This is the conversion routine for SO (Slope Offset)
// Conversion is Out = Slope*In + Offset
// Parameters are slope, offset
static float LinearEU(float in, void *parameter)
{
	float* p_SO = (float*)parameter;
	float slope = p_SO[0];
	float offset = p_SO[1];
	#ifdef EUDiagnose
	printf("LinearEU Called with in = %f, slope = %f, offset = %f\n",in,slope,offset);
	#endif
    return slope*in + offset;
}

// This is the conversion routine for TL (Table Lookup)
// Conversion is linear interpolation and extrapolation
// Parameters are (int) Number of table pairs, followed by the (float) pairs
static float TableEU(float in, void *parameter)
{
	// ASSUMPTION: Ordered pairs are sequential from low to high.
	// NOT CHECKED.
	EUTableLookup* Table = (EUTableLookup*)parameter;
	int nEntries = Table->nEntries;
	if (nEntries < 2) return in;		// Table does not make sense in this case.
	float m = 1, b = 0,y = in;					// y = mx + b
	int nCount = 0;
	// Just testing to see if I am getting them...
	#ifdef EUDiagnose
	printf("Table has %d entries\n",nEntries);
	#endif
	while(nCount < nEntries)
	{
		#ifdef EUDiagnose
		printf("Table entry %d =   %f  ,  %f\n",nCount,Table->Abs[nCount],Table->Ord[nCount]);
		#endif
		nCount++;
	}
	if (in <= Table->Abs[0])	// Lower than the lowest pair, extrapolation required.
	{
		#ifdef EUDiagnose
		printf("Doing Low Extrapolation on input %f\n",in);
		#endif
		m = (Table->Ord[1] - Table->Ord[0])/(Table->Abs[1] - Table->Abs[0]);
		b = Table->Ord[0] - m*Table->Abs[0];
	}
	if (in >= Table->Abs[nEntries-1])	// Higher than the highest pair, extrapolation required.
	{
		#ifdef EUDiagnose
		printf("Doing High Extrapolation on input %f\n",in);
		#endif
		m = (Table->Ord[nEntries-1] - Table->Ord[nEntries-2])/(Table->Abs[nEntries-1] - Table->Abs[nEntries-2]);
		b = Table->Ord[nEntries-2] - m*Table->Abs[nEntries-2];
	}
	if ((in > Table->Abs[0]) && (in < Table->Abs[nEntries-1]))
	{
		#ifdef EUDiagnose
		printf("Doing Interpolation on input %f\n",in);
		#endif
		nCount = 0;
		while (in > Table->Abs[nCount])
		{
			nCount++;
		}
		m = (Table->Ord[nCount] - Table->Ord[nCount-1])/(Table->Abs[nCount] - Table->Abs[nCount-1]);
		b = Table->Ord[nCount-1] - m*Table->Abs[nCount-1];
	}
	y = m*in + b;
	#ifdef EUDiagnose
	printf("%f = %f*%f + %f\n",y,m,in,b);
	#endif
    return y;
}

// Called on Startup
void InitEUStructure()
{
	int ni = 0;
	for (ni = 0; ni < MAXEUFLOATS; ni++)
	{
		EUConv[ni].ID = NULL;
		EUConv[ni].Index = -1;
		EUConv[ni].Function = NULL;
		EUConv[ni].Params = NULL;
	}
}

// Will create an entry in the EU Conversion structure
// from a single Line in the ascii file
void CreateEUStructureEntry(char *CommaSeparatedLine,int entry)
{
	#ifdef EUDiagnose
	printf("Parsing CommaSeparatedLine %s, entry = %d\n",CommaSeparatedLine,entry);
	#endif
    int ni = 0;
    char *saveptr, *token, *localCopy;
    
    localCopy = strdup(CommaSeparatedLine);
	// Get the ID String
    token = strtok_r(localCopy, ",", &saveptr);
	EUConv[entry].ID = new char[strlen(token)];
	strcpy(EUConv[entry].ID,token);
	#ifdef EUDiagnose
	printf("ID = %s\n",EUConv[entry].ID);
	#endif
	// The index should remain -1	
	// Get the Units String
    token = strtok_r(NULL, ",", &saveptr);
	EUConv[entry].Units = new char[strlen(token)];
	strcpy(EUConv[entry].Units,token);
	#ifdef EUDiagnose
	printf("Units = %s\n",EUConv[entry].Units);
	#endif

	// Get the Conversion Type
    token = strtok_r(NULL, ",", &saveptr);
	#ifdef EUDiagnose
	printf("Conversion Type = %s\n",token);
	#endif
	if(0 == strcmp(token,SGDes))
	{
		EUConv[entry].Function = BridgeEU;		// Strain Gage
		EUConv[entry].Params = new float;
		token = strtok_r(NULL, ",", &saveptr);	// Single Parameter
		sscanf(token,"%f",(float*)EUConv[entry].Params);
		#ifdef EUDiagnose
		printf("Set Sensor ID %s to Strain Gage, Units = %s, Factor = %f\n",EUConv[entry].ID,EUConv[entry].Units,((float*)EUConv[entry].Params)[0]);
		#endif
	}
	if(0 == strcmp(token,SODes))
	{
		EUConv[entry].Function = LinearEU;		// Slope/Offset
		float* TempParam = new float[2];
		EUConv[entry].Params = TempParam;
		token = strtok_r(NULL, ",", &saveptr);	// First Parameter
		#ifdef EUDiagnose
		printf("Parsing Slope-Offset Line, Slope String = %s\n",token);
		#endif
		sscanf(token,"%f",TempParam++);
		token = strtok_r(NULL, ",", &saveptr);	// Second Parameter
		#ifdef EUDiagnose
		printf("Parsing Slope-Offset Line, Offest String = %s\n",token);
		#endif
		sscanf(token,"%f",TempParam);
		#ifdef EUDiagnose
		printf("Set Sensor ID %s to Slope-Offset, Units = %s, Slope = %f, Offset = %f\n",EUConv[entry].ID,EUConv[entry].Units,((float*)EUConv[entry].Params)[0],(float)((float*)EUConv[entry].Params)[1]);
		#endif
	}
	if(0 == strcmp(token,TLDes))
	{
		int nEntries = 0;
		EUTableLookup* NewTable = new EUTableLookup;
		EUConv[entry].Function = TableEU;		// Table Lookup
		token = strtok_r(NULL, ",", &saveptr);
		sscanf(token,"%d",&nEntries);
		EUConv[entry].Params = NewTable;
		sscanf(token,"%d",&(NewTable->nEntries));
		#ifdef EUDiagnose
		printf("Set Sensor ID %s to Table Lookup, Units = %s, Entries = %d\n",EUConv[entry].ID,EUConv[entry].Units,*(int*)EUConv[entry].Params);
		#endif
		NewTable->Abs = new float[nEntries];
		NewTable->Ord = new float[nEntries];
		for (ni = 0; ni < nEntries; ni++)
		{
			token = strtok_r(NULL, ",", &saveptr);
			sscanf(token,"%f",&(NewTable->Abs[ni]));
			token = strtok_r(NULL, ",", &saveptr);
			sscanf(token,"%f",&(NewTable->Ord[ni]));
		}
	}
   free(localCopy);
}

// Free up all the memory allocated in CreateEUStructureEntry
void CleanUpEUStructs()
{
	int ni = 0;
	for (ni = 0; ni < MAXEUFLOATS; ni++)
	{
		if (NULL != EUConv[ni].ID)
		{
			EUConv[ni].Function = NULL;
			EUConv[ni].Index = -1;
			if(0 == strcmp(EUConv[ni].ID,SGDes))	// Strain Gage
			{
				delete (float*)EUConv[ni].Params;			// Single Parameter to delete
			}
			if(0 == strcmp(EUConv[ni].ID,SODes))	// Slope/Offset
			{
				delete[] (float*)EUConv[ni].Params;			// Two Params to delete
			}
			if(0 == strcmp(EUConv[ni].ID,TLDes))	// Table Lookup
			{
				EUTableLookup* Table = (EUTableLookup*)EUConv[ni].Params;
				delete[] Table->Abs;
				delete[] Table->Ord;
				delete Table;
			}
			delete[] EUConv[ni].ID;					// Delete a char array
			EUConv[ni].ID = NULL;
		}
	}
}

// Set the Sensor index
int SetEUIndex(int ni, int sit)
{
	if ((ni < MAXEUFLOATS) && (ni > 0))
	{
		EUConv[ni].Index = sit;
		return 1;
	}
		
	else return -1;
}

// Get the Sensor Units
char* GetEUUnits(int ni)
{
	return EUConv[ni].Units;
}

// Get the Sensor Function
float (*GetEUFunction(int ni)) (float, void*) 
{
	return EUConv[ni].Function;
}

// Get the Sensor Params
void* GetEUParams(int ni)
{
	return EUConv[ni].Params;
}

// Returns the Sensor index, given a Sensor ID string
int FindSensorEUConversion(char* SensorName)
{
	int ni,idx = -1;
	for (ni = 0; ni < MAXEUFLOATS; ni++)
	{
		if (NULL != EUConv[ni].ID)
		{
			if(0 == strcmp(EUConv[ni].ID,SensorName))
			{
				idx = ni;
			}
		}
	}
	return idx;
}

void CreateEUStructure(FILE* EUFile)
{
	int idx = 0;
	while (NULL != fgets(Line,EUBUFSIZE,EUFile))
	{
		#ifdef EUDiagnose
		printf("Read Line: %s",Line);
		#endif
		CreateEUStructureEntry(Line,idx);
		idx++;
	}
}
