#ifndef USTRAINBS_H
#define USTRAINBS_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _MicroStrainBS *MicroStrainBS;

MicroStrainBS MicroStrainBS_create(char *deviceTTY, char *ipAddress, unsigned short port);
void          MicroStrainBS_destroy(MicroStrainBS uStrainBS);
void          MicroStrainBS_allowNewSensors(MicroStrainBS uStrainBS);
void          MicroStrainBS_ignoreNewSensors(MicroStrainBS uStrainBS);

#ifdef __cplusplus
}
#endif

#endif
