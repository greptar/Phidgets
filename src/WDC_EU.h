#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define EUBUFSIZE 256
#define MAXEUFLOATS 128

// These are the defined EU Conversion types
#define SGDes "SG"
#define SODes "SO"
#define TLDes "TL"

struct EUConvRecord { char* ID; int Index; char* Units; float(*Function)(float in, void *parameters); void* Params;};
struct EUTableLookup { int nEntries; float* Abs; float* Ord; };

// Uncomment for Diagnostic messages dumped to the console
// #define EUDiagnose

// Called on Startup
void InitEUStructure();

// Set the Sensor index
int SetEUIndex(int ni, int sit);

// Get the Sensor entries
char* GetEUUnits(int ni);
float (*GetEUFunction(int ni)) (float, void*) ;
void* GetEUParams(int ni);

// Will create an entry in the EU Conversion structure
// from a single Line in the ascii file
void CreateEUStructureEntry(char *CommaSeparatedLine,int entry);

// Free up all the memory allocated in CreateEUStructureEntry
void CleanUpEUStructs();

// Returns the Sensor index, given a Sensor ID string
int FindSensorEUConversion(char* SensorName);

// Called from WDC to set up the structure
void CreateEUStructure(FILE* EUFile);
