/*
This software is a driver for ACCES I/O Products, Inc. PCI cards.
Copyright (C) 2007  ACCES I/O Products, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation. This software is released under
version 2 of the GPL.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

In addition ACCES provides other licenses with its software at customer request.
For more information please contact the ACCES software department at 
(800)-326-1649 or visit www.accesio.com
*/



#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <linux/types.h>
#include <errno.h>
#include <sys/times.h>
#include <strings.h>
#include <asm/byteorder.h>
#include <sys/io.h>

#define NUM_RINGS 4
#define MOTORS_PER_RING 12
#define FIRST_RING_IO 0
#define FIRST_MOTOR_IO 4

int main (int argc, char **argv)
{
	int i, j;
	unsigned char io_in[6];
	unsigned char io_out[6];
	int ring_to_ioport[NUM_RINGS + 1];
	int motor_to_ioport[MOTORS_PER_RING + 1];
	unsigned long base = 0x300;

	if (ioperm(base, 24, 1) == -1)
	{
		fprintf(stderr, "Unable to get I/O Permissions\n");
		exit(1);
	}

	for (i=1;i<=NUM_RINGS;i++)
		ring_to_ioport[i] = FIRST_RING_IO + i - 1;
	for (i=1;i<=MOTORS_PER_RING;i++)
		motor_to_ioport[i] = FIRST_MOTOR_IO + i - 1;

	for (i=1;i<=MOTORS_PER_RING;i++)
	{
		bzero(io_out, 6);
		io_out[motor_to_ioport[i]/8] = 1 << motor_to_ioport[i] % 8;
		outb(~io_out[0], base);
		outb(~io_out[1], base+4);
		outb(~io_out[2], base+8);
		outb(~io_out[3], base+12);
		outb(~io_out[4], base+16);
		outb(~io_out[5], base+20);
		usleep(1000);
		io_in[0] = inb(base+1);
		io_in[1] = inb(base+5);
		io_in[2] = inb(base+9);
		io_in[3] = inb(base+13);
		io_in[4] = inb(base+17);
		io_in[5] = inb(base+21);
		for (j=1;j<=NUM_RINGS;j++)
		{
			if (((io_in[ring_to_ioport[j]/8]) & (1 << ring_to_ioport[j] % 8)) == 0)
				fprintf(stderr, "Motor missing at %d %d\n", j, i);
		}
		outb(~0, base);
		outb(~0, base+4);
		outb(~0, base+8);
                outb(~0, base+12);
                outb(~0, base+16);
                outb(~0, base+20);
	}

	return 0;
}
