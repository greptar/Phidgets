// Find the center of a phere given four points on the surface
// implementing an alforithm from
// 2000clicks.com/mathhelp/GeometryConicSectionSphereEquationGivenFourPoints.aspx

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <values.h>

#include "laser_position.h"
#include "sensor-packet.h"

static float radius;
static float X[4];
static float Y[4];
static float Z[4];
static float D[4];

typedef struct _Laser_Position
{
        int              socketFD;
        struct sockaddr_in socketADDR;
} _Laser_Position;

typedef struct vec
{
  float x,y,z;
} vec;

static float normSquared(vec v)
{
  return v.x*v.x + v.y*v.y + v.z*v.z;
}

static vec newv(float x, float y, float z)
{
  vec v;
  v.x = x;
  v.y = y;
  v.z = z;
  return v;
}

static vec cross(vec a, vec b)
{
  return newv(a.y*b.z - a.z*b.y, a.z*b.x - a.x*b.z, a.x*b.y - a.y*b.x);
}

static float dot(vec a, vec b)
{
  return a.x*b.x + a.y*b.y + a.z*b.z;
}

static vec vminus(vec a, vec b)
{
  return newv(a.x-b.x, a.y-b.y, a.z-b.z);
}

static vec vplus(vec a, vec b)
{
  return newv(a.x+b.x, a.y+b.y, a.z+b.z);
}

static vec vtimes(vec a, float s)
{
  return newv(a.x*s, a.y*s, a.z*s);
}

static int readConfig(void)
{
  int it;
  FILE *f = fopen("/root/laser.config", "r");

  if (f && fscanf(f, "%f %f %f %f %f %f %f %f %f %f %f %f %f",
                  &radius,
                  X  , Y  , Z  ,
                  X+1, Y+1, Z+1,
                  X+2, Y+2, Z+2,
                  X+3, Y+3, Z+3) == 13)
  {
    fprintf(stderr, "Read /root/laser.config.\n");
    fclose(f);
    for (it = 0; it < 4; it++)
      D[it] = sqrt(X[it]*X[it] + Y[it]*Y[it] + Z[it]*Z[it]);
    return 1;
  }
  else
  {
    if (f) fclose(f);
    f = fopen("/tmp/laser.config", "w");
    fprintf(stderr, "Could not read /root/laser.config.  Examples in /tmp/laser.config.\n");
    radius = 1.25;
    X[0] = 16*(12);
    Y[0] = -4*(12);
    Z[0] = -4*(12);
    X[1] = 16*(12);
    Y[1] = 4*(12);
    Z[1] = 4*(12);
    X[2] = 16*(12);
    Y[2] = -4*(12);
    Z[2] = 4*(12);
    X[3] = 16*(12);
    Y[3] = 4*(12);
    Z[3] = -4*(12);
    fprintf(f, "%f\n%f\n%f\n%f\n%f\n%f\n%f\n%f\n%f\n%f\n%f\n%f\n%f\n",
            radius, 
            X[0], Y[0], Z[0],
            X[1], Y[1], Z[1],
            X[2], Y[2], Z[2],
            X[3], Y[3], Z[3]);
    fclose(f);
    return 0;
  }
}

static int distancesToXYZ(float m1, float m2, float m3, float m4, float *xyz)
{
  vec C, n, p0, p1, p2, p3, p4, CenterPointA, CenterPointB, result;
  vec p12, p21, p13, p31, p14, p41, p23, p32, p24, p42, p34, p43;
  float Denominator, alpha, beta, gamma, a, b, c;
  float M[4];
  float xhat[4];
  float yhat[4];
  float zhat[4];
  int it;
  float x1, x2, x3, x4, y1, y2, y3, y4, z1, z2, z3, z4, t, tee;

  M[0] = m1;
  M[1] = m2;
  M[2] = m3;
  M[3] = m4;
  xyz[0] = MAXFLOAT;
  xyz[1] = MAXFLOAT;
  xyz[2] = MAXFLOAT;
  if (m1==0.0 || m2==0.0 || m3==0.0 || m4==0.0)
    return 0;

  // Now, estimate the values of x,y,z from the distance measurements
  // Call these estimates "hat"
  for (it = 0; it < 4; it ++)
  {
    tee = 1 - M[it]/D[it];
    xhat[it] = tee*X[it];
    yhat[it] = tee*Y[it];
    zhat[it] = tee*Z[it];
  }

  x1 = xhat[0];
  y1 = yhat[0];
  z1 = zhat[0];
  x2 = xhat[1];
  y2 = yhat[1];
  z2 = zhat[1];
  x3 = xhat[2];
  y3 = yhat[2];
  z3 = zhat[2];
  x4 = xhat[3];
  y4 = yhat[3];
  z4 = zhat[3];
  p1 = newv(x1,y1,z1);   // point 1
  p2 = newv(x2,y2,z2);   // point 2
  p3 = newv(x3,y3,z3);   // point 3
  p4 = newv(x4,y4,z4);   // point 4
  p12 = vminus(p1,p2);
  p21 = vminus(p2,p1);
  p13 = vminus(p1,p3);
  p31 = vminus(p3,p1);
  p14 = vminus(p1,p4);
  p41 = vminus(p4,p1);
  p23 = vminus(p2,p3);
  p32 = vminus(p3,p2);
  p24 = vminus(p2,p4);
  p42 = vminus(p4,p2);
  p34 = vminus(p3,p4);
  p43 = vminus(p4,p3);
        
  // Use points 1,2,3 to find the center of the circumscribed circle
  n = cross(p21,p31); // n is normal to the plane.
  Denominator = 2*(normSquared(n));
  alpha = ((normSquared(p23)))*(dot(p12,p13))/Denominator;
  beta = ((normSquared(p13)))*(dot(p21,p23))/Denominator;
  gamma = ((normSquared(p12)))*(dot(p31,p32))/Denominator;
  p0 = vplus(vplus(vtimes(p1,alpha) , vtimes(p2,beta)) , vtimes(p3,gamma));
  // The sphere center is a point that is on the line and distance r from
  // any of the other points.
  // Choose point 1
  C = vminus(p0,p1);
  a = dot(n,n);
  b = -2*dot(n,C);
  c = dot(C,C) - radius*radius;
  if(4*a*c > b*b)
      return 0;
  t = (-b - sqrt(b*b - 4*a*c))/(2*a);
  CenterPointA = vplus(p0, vtimes(n,t));
    
  // Use points 2,3,4 to find the center of the circumscribed circle
  n = cross(p24,p34); // n is normal to the plane.
  Denominator = 2*(normSquared(n));
  alpha = ((normSquared(p23)))*(dot(p42,p43))/Denominator;
  beta = ((normSquared(p43)))*(dot(p24,p23))/Denominator;
  gamma = ((normSquared(p42)))*(dot(p34,p32))/Denominator;
  p0 = vplus(vplus(vtimes(p4,alpha) , vtimes(p2,beta)) , vtimes(p3,gamma));
  // The sphere center is a point that is on the line and distance r from
  // any of the other points.
  // Choose point 4
  C = vminus(p0,p4);
  a = dot(n,n);
  b = -2*dot(n,C);
  c = dot(C,C) - radius*radius;
  if(4*a*c > b*b)
      return 0;
  t = (-b - sqrt(b*b - 4*a*c))/(2*a);
  CenterPointB = vplus(p0, vtimes(n,t));

  // The answer will be the average of the two computations
  result  = vtimes(vplus(CenterPointA,CenterPointB), 0.5);
  xyz[0] = result.x;
  xyz[1] = result.y;
  xyz[2] = result.z;
  return 1;
}

void Laser_Position_send(Laser_Position lp, float m1, float m2, float m3, float m4)
{
  static SensorPacket *packet = NULL;
  struct timeval tv;

  gettimeofday(&tv, NULL);
  if (!packet)
  {
    packet = (SensorPacket *)malloc(sizeof(SensorPacket));
    if (!packet) return;
  }
  packet->header.packet_type = SP_TYPE_LEICA;
  packet->header.packet_cmd = SP_CMD_DATA;
  packet->header.packet_num = 0;
  packet->header.num_buffered_packets = 30;
  packet->header.deviceClass = 0;
  packet->header.dataType = 1;
  packet->header.dataSize = 4;
  packet->header.serialNumber = 10;
  packet->header.time_sec = tv.tv_sec;
  packet->header.time_usec = tv.tv_usec;
  packet->header.channelMask = 7;
  packet->header.dataLength = packet->header.dataSize * 3;

  distancesToXYZ(m1, m2, m3, m4, packet->data.floats);
  SensorPacket_ToNetworkOrder(packet);
  sendto(lp->socketFD, packet, sizeof(SensorPacketHeader) + 12, 0,
         (struct sockaddr *)&lp->socketADDR, sizeof(lp->socketADDR));
}

Laser_Position Laser_Position_create(char *ipAddress, unsigned short port)
{
        Laser_Position lp;

        if (!readConfig()) return NULL;
        lp = (Laser_Position)malloc(sizeof(_Laser_Position));
	if ((lp->socketFD = socket(AF_INET,SOCK_DGRAM,0)) <  0)
	{
		perror("Creating UDP Socket");
		free(lp);
		return NULL;
	}
	memset(&lp->socketADDR,0,sizeof(lp->socketADDR));
	lp->socketADDR.sin_family = AF_INET;
	lp->socketADDR.sin_addr.s_addr = inet_addr(ipAddress);
	lp->socketADDR.sin_port = htons(port);

        return lp;
}

void Laser_Position_destroy(Laser_Position lp)
{
        // Need to free memory inside
        free(lp);
}
