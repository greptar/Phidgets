// Common for the WDC program.

#define MS 1000					// milliseconds per second
#define USPERMS 1000			// microseconds per millisecond
#define BUFSIZE 256 			// for character buffers
#define MINDATASTEP 100 		// Fastest ms interval for sending data
#define DEFDATASTEP 5000		// Default ms interval for sending data
#define MAXDATASTEP 100000		// Slowest ms interval for sending data
#define UDPINPORT 6551			// Port to listen for "GetDC" from the CS
#define UDPOUTPORT 6552			// Port to reply to "GetDC"
#define TCPPORT 6553			// Port for all TCP Communication
#define DATAINPORT 1027			// Port for WC SENSOR DATA Communication
#define WDCID 300				// Should be in a config file - THREE DIGITS for recording file name
#define PADSTR "                         "	// 25 blanks
#define BYTESPERINT 4			// Bytes in an integer.
#define BYTESPERFLOAT 4			// Bytes in a float.

#define MAXFLOATS 128			// Max number of floats to be transmitted to CS = Max Number of Sensors
#define MAXOVERSAMPLES 10		// This plus MAXFLOATS is for memory allocation.

// These are the messages from the CS:
#define GET_WDC_NAME 0
#define START_SCANNING 1
#define STOP_SCANNING 2
#define SCAN_DATA 3
#define LOAD_SCANLIST 4
#define ERROR_Renamed 5
#define WARNING 6
#define SET_SCAN_RATE 7
#define ARM_SYNCHRONOUS_START 8
#define GET_UNITS 9
#define SET_WDC_RECORDING 10
#define SET_WDC_RECORDING_RATE 11
#define SET_WDC_PICKOFF_RATE 12
#define SET_WDC_OVER_SAMPLING_RATE 13
#define NULL_SENSORS 14

// For the Sensor Manager
#define SMPORT 1027

#define BOOL_STR(b) ((b)?"true":"false")
