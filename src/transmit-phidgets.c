#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <signal.h>
#include <unistd.h>
#include <values.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "sensor-packet.h"
#include "phidget21.h"
#include "uStrainBS.h"

#define PACKET_BUFFER_LEN 30
#define UDP_BUFFER_LEN 256

typedef struct SensorPacketBuffer
{
    SensorPacket accum;
    SensorPacket packets[PACKET_BUFFER_LEN];
    int          position;
} SensorPacketBuffer;

static int numTempHandles = 0;
static CPhidgetTemperatureSensorHandle *tempHandles = NULL;
static SensorPacketBuffer *tempPacketBuffers = NULL;

static int numSpatialHandles = 0;
static CPhidgetSpatialHandle *spatialHandles = NULL;
static SensorPacketBuffer *spatialPacketBuffers = NULL;

static int numBridgeHandles = 0;
static CPhidgetBridgeHandle *bridgeHandles = NULL;
static SensorPacketBuffer *bridgePacketBuffers = NULL;

static int numInterfaceKitHandles = 0;
static CPhidgetInterfaceKitHandle *interfaceKitHandles = NULL;
static SensorPacketBuffer *interfaceKitPacketBuffers = NULL;

static pthread_mutex_t changeHandlerMutex;

static int cancel=0;

static int socketFD;
static struct sockaddr_in socketADDR;

static void sighandle(int signum)
{
    cancel = 1;
}

static void sendto_wrapper(SensorPacketBuffer *buffer)
{
#if __BYTE_ORDER == __BIG_ENDIAN
    sendto(socketFD,  buffer->packets + buffer->position,
            sizeof(SensorPacketHeader) + buffer->packets[buffer->position].header.dataLength,
            0, (struct sockaddr *)&socketADDR, sizeof(socketADDR));
#else
    SensorPacket packet = buffer->packets[buffer->position];
    SensorPacket_ToNetworkOrder(&packet);
    sendto(socketFD, &packet,
            sizeof(SensorPacketHeader) + buffer->packets[buffer->position].header.dataLength,
            0, (struct sockaddr *)&socketADDR, sizeof(socketADDR));
#endif
}

static int DetachHandler(CPhidgetHandle phid, void *userPtr)
{
    SensorPacketBuffer *buffer;
    if (pthread_mutex_lock(&changeHandlerMutex) != 0) perror("pthread_mutex_lock"), cancel=1;
    buffer = (SensorPacketBuffer *)userPtr;
    buffer->accum.header.channelMask = 0;
    buffer->accum.header.dataLength = 0;
    if (pthread_mutex_unlock(&changeHandlerMutex) != 0) perror("pthread_mutex_unlock"), cancel=1;
    return 0;
}

static int TemperatureChangeHandler(CPhidgetTemperatureSensorHandle TEMP, void *usrptr, int Index, double Value)
{
    SensorPacketBuffer *buffer;
    struct timeval tv;
    double ambient;
    //double pot, potMin, potMax;

    //CPhidgetTemperatureSensor_getPotential(TEMP, Index, &pot);
    //CPhidgetTemperatureSensor_getPotentialMin(TEMP, Index, &potMin);
    //CPhidgetTemperatureSensor_getPotentialMax(TEMP, Index, &potMax);
    //if (pot < potMin || pot > potMax) return 0;

    if (pthread_mutex_lock(&changeHandlerMutex) != 0) perror("pthread_mutex_lock"), cancel=1;

    buffer = (SensorPacketBuffer *)usrptr;
    gettimeofday(&tv, NULL);
    if (Index != -1)
    {
        int i, j=0;
        if (!(buffer->accum.header.channelMask & (1 << Index)))
        {
            buffer->accum.header.channelMask += (1 << Index);
            buffer->accum.header.dataLength += buffer->accum.header.dataSize;
        }
        for (i=0; i<16; i++)
        {
            if (i == Index) break;
            if (buffer->accum.header.channelMask & (1 << i)) j++;
        }
        buffer->accum.data.floats[j] = Value;
        // Now going to put junction temp in position 15
        if (!(buffer->accum.header.channelMask & (1 << 15)))
        {
            buffer->accum.header.channelMask += (1 << 15);
            buffer->accum.header.dataLength += buffer->accum.header.dataSize;
        }
        j = 0;
        for (i=0; i<16; i++)
        {
            if (i == 15) break;
            if (buffer->accum.header.channelMask & (1 << i)) j++;
        }
        CPhidgetTemperatureSensor_getAmbientTemperature(TEMP, &ambient);
        buffer->accum.data.floats[j] = (float)ambient;
    }
    buffer->packets[buffer->position].header.time_sec = tv.tv_sec;
    buffer->packets[buffer->position].header.time_usec = tv.tv_usec;
    buffer->packets[buffer->position].header.channelMask = buffer->accum.header.channelMask;
    buffer->packets[buffer->position].header.dataLength = buffer->accum.header.dataLength;
    buffer->packets[buffer->position].data = buffer->accum.data;

    if (buffer->accum.header.channelMask || buffer->accum.header.time_sec)
        if (Index != -1 || (tv.tv_sec + tv.tv_usec / 1000000.0) > (buffer->accum.header.time_sec + buffer->accum.header.time_usec / 1000000.0 + 1))
        {
            sendto_wrapper(buffer);
            buffer->position += 1;
            if (buffer->position == PACKET_BUFFER_LEN) buffer->position = 0;
            buffer->accum.header.time_sec = tv.tv_sec;
            buffer->accum.header.time_usec = tv.tv_usec;
        }

    if (pthread_mutex_unlock(&changeHandlerMutex) != 0) perror("pthread_mutex_unlock"), cancel=1;
    return 0;
}

static int TemperatureAttachHandler(CPhidgetHandle phid, void *userPtr)
{
    CPhidgetTemperatureSensorHandle temp = (CPhidgetTemperatureSensorHandle)phid;
    int i, count=0, sn;
    SensorPacketBuffer *buffer = (SensorPacketBuffer *)userPtr;

    CPhidgetTemperatureSensor_getTemperatureInputCount(temp, &count);
    for (i=0; i<count; i++)
    {
        CPhidgetTemperatureSensor_setThermocoupleType(temp, i, PHIDGET_TEMPERATURE_SENSOR_E_TYPE);
    }
    CPhidget_getSerialNumber(phid, &sn);

    buffer->accum.header.time_sec = 0;
    buffer->accum.header.time_usec = 0;
    buffer->accum.header.packet_type = SP_TYPE_PHIDGET;
    buffer->accum.header.packet_cmd = SP_CMD_DATA;
    buffer->accum.header.packet_num = 0;
    buffer->accum.header.num_buffered_packets = PACKET_BUFFER_LEN;
    buffer->accum.header.serialNumber = sn;
    buffer->accum.header.deviceClass = PHIDCLASS_TEMPERATURESENSOR;
    buffer->accum.header.channelMask = 0;
    buffer->accum.header.dataType = 1;
    buffer->accum.header.dataSize = 4;
    buffer->accum.header.dataLength = 0;

    for (i=0; i<PACKET_BUFFER_LEN; i++)
    {
        buffer->packets[i].header = buffer->accum.header;
        buffer->packets[i].header.packet_num = i;
    }

    CPhidgetTemperatureSensor_set_OnTemperatureChange_Handler(temp, TemperatureChangeHandler, buffer);
    CPhidget_set_OnDetach_Handler((CPhidgetHandle)temp, DetachHandler, buffer);

    return 0;
}

static int SpatialChangeHandler(CPhidgetSpatialHandle phid, void *userPtr,
        CPhidgetSpatial_SpatialEventDataHandle *data, int dataCount)
{
    int i;
    for (i=0; i<dataCount; i++)
        fprintf(stdout, "%d.%06d Spatial sensor: %f %f %f\n",
                data[i]->timestamp.seconds,
                data[i]->timestamp.microseconds,
                data[i]->acceleration[0], data[i]->acceleration[1], data[i]->acceleration[2]);
    return 0;
}

static int SpatialAttachHandler(CPhidgetHandle phid, void *userPtr)
{
    CPhidgetSpatialHandle spatial = (CPhidgetSpatialHandle)phid;
    SensorPacketBuffer *buffer = (SensorPacketBuffer *)userPtr;

    CPhidgetSpatial_set_OnSpatialData_Handler(spatial, SpatialChangeHandler, buffer);
    CPhidget_set_OnDetach_Handler((CPhidgetHandle)spatial, DetachHandler, buffer);
    return 0;
}

static int BridgeChangeHandler(CPhidgetBridgeHandle phid, void *usrptr,
        int Index, double Value)
{
    SensorPacketBuffer *buffer;
    struct timeval tv;

    if (pthread_mutex_lock(&changeHandlerMutex) != 0) perror("pthread_mutex_lock"), cancel=1;

    buffer = (SensorPacketBuffer *)usrptr;
    gettimeofday(&tv, NULL);
    if (Index != -1)
    {
        int i, j=0;
        if (!(buffer->accum.header.channelMask & (1 << Index)))
        {
            buffer->accum.header.channelMask += (1 << Index);
            buffer->accum.header.dataLength += buffer->accum.header.dataSize;
        }
        for (i=0; i<16; i++)
        {
            if (i == Index) break;
            if (buffer->accum.header.channelMask & (1 << i)) j++;
        }
        buffer->accum.data.floats[j] = Value;
    }
    buffer->packets[buffer->position].header.time_sec = tv.tv_sec;
    buffer->packets[buffer->position].header.time_usec = tv.tv_usec;
    buffer->packets[buffer->position].header.channelMask = buffer->accum.header.channelMask;
    buffer->packets[buffer->position].header.dataLength = buffer->accum.header.dataLength;
    buffer->packets[buffer->position].data = buffer->accum.data;

    if (buffer->accum.header.channelMask || buffer->accum.header.time_sec)
        if (Index != -1 || (tv.tv_sec + tv.tv_usec / 1000000.0) > (buffer->accum.header.time_sec + buffer->accum.header.time_usec / 1000000.0 + 1))
        {
            sendto_wrapper(buffer);
            buffer->position += 1;
            if (buffer->position == PACKET_BUFFER_LEN) buffer->position = 0;
            buffer->accum.header.time_sec = tv.tv_sec;
            buffer->accum.header.time_usec = tv.tv_usec;
        }

    if (pthread_mutex_unlock(&changeHandlerMutex) != 0) perror("pthread_mutex_unlock"), cancel=1;
    return 0;
}

static int BridgeAttachHandler(CPhidgetHandle phid, void *userPtr)
{
    CPhidgetBridgeHandle bridge = (CPhidgetBridgeHandle)phid;
    int i, count=0, sn;
    SensorPacketBuffer *buffer = (SensorPacketBuffer *)userPtr;

    CPhidgetBridge_getInputCount(bridge, &count);
    for (i=0; i<count; i++)
    {
        CPhidgetBridge_setEnabled(bridge, i, PTRUE);
        CPhidgetBridge_setGain(bridge, i, PHIDGET_BRIDGE_GAIN_128);
    }
    CPhidgetBridge_setDataRate(bridge, 1000);
    CPhidget_getSerialNumber(phid, &sn);

    buffer->accum.header.time_sec = 0;
    buffer->accum.header.time_usec = 0;
    buffer->accum.header.packet_type = SP_TYPE_PHIDGET;
    buffer->accum.header.packet_cmd = SP_CMD_DATA;
    buffer->accum.header.packet_num = 0;
    buffer->accum.header.num_buffered_packets = PACKET_BUFFER_LEN;
    buffer->accum.header.serialNumber = sn;
    buffer->accum.header.deviceClass = PHIDCLASS_BRIDGE;
    buffer->accum.header.channelMask = 0;
    buffer->accum.header.dataType = 1;
    buffer->accum.header.dataSize = 4;
    buffer->accum.header.dataLength = 0;

    for (i=0; i<PACKET_BUFFER_LEN; i++)
    {
        buffer->packets[i].header = buffer->accum.header;
        buffer->packets[i].header.packet_num = i;
    }

    CPhidgetBridge_set_OnBridgeData_Handler(bridge, BridgeChangeHandler, buffer);
    CPhidget_set_OnDetach_Handler((CPhidgetHandle)bridge, DetachHandler, buffer);

    return 0;
}

static int InterfaceKitChangeHandler(CPhidgetInterfaceKitHandle phid, void *usrptr,
        int Index, int Value)
{
    SensorPacketBuffer *buffer;
    struct timeval tv;

    if (pthread_mutex_lock(&changeHandlerMutex) != 0) perror("pthread_mutex_lock"), cancel=1;

    buffer = (SensorPacketBuffer *)usrptr;
    gettimeofday(&tv, NULL);
    if (Index != -1)
    {
        int i, j=0;
        if (!(buffer->accum.header.channelMask & (1 << Index)))
        {
            buffer->accum.header.channelMask += (1 << Index);
            buffer->accum.header.dataLength += buffer->accum.header.dataSize;
        }
        for (i=0; i<16; i++)
        {
            if (i == Index) break;
            if (buffer->accum.header.channelMask & (1 << i)) j++;
        }
        buffer->accum.data.uShorts[j] = Value;
    }
    buffer->packets[buffer->position].header.time_sec = tv.tv_sec;
    buffer->packets[buffer->position].header.time_usec = tv.tv_usec;
    buffer->packets[buffer->position].header.channelMask = buffer->accum.header.channelMask;
    buffer->packets[buffer->position].header.dataLength = buffer->accum.header.dataLength;
    buffer->packets[buffer->position].data = buffer->accum.data;

    if (buffer->accum.header.channelMask || buffer->accum.header.time_sec)
        if (Index != -1 || (tv.tv_sec + tv.tv_usec / 1000000.0) > (buffer->accum.header.time_sec + buffer->accum.header.time_usec / 1000000.0 + 1))
        {
            sendto_wrapper(buffer);
            buffer->position += 1;
            if (buffer->position == PACKET_BUFFER_LEN) buffer->position = 0;
            buffer->accum.header.time_sec = tv.tv_sec;
            buffer->accum.header.time_usec = tv.tv_usec;
        }

    if (pthread_mutex_unlock(&changeHandlerMutex) != 0) perror("pthread_mutex_unlock"), cancel=1;
    return 0;
}

static int InterfaceKitAttachHandler(CPhidgetHandle phid, void *userPtr)
{
    CPhidgetInterfaceKitHandle interfaceKit = (CPhidgetInterfaceKitHandle)phid;
    int i, sn;
    SensorPacketBuffer *buffer = (SensorPacketBuffer *)userPtr;

    CPhidget_getSerialNumber(phid, &sn);

    buffer->accum.header.time_sec = 0;
    buffer->accum.header.time_usec = 0;
    buffer->accum.header.packet_type = SP_TYPE_PHIDGET;
    buffer->accum.header.packet_cmd = SP_CMD_DATA;
    buffer->accum.header.packet_num = 0;
    buffer->accum.header.num_buffered_packets = PACKET_BUFFER_LEN;
    buffer->accum.header.serialNumber = sn;
    buffer->accum.header.deviceClass = PHIDCLASS_INTERFACEKIT;
    buffer->accum.header.channelMask = 0;
    buffer->accum.header.dataType = 0;
    buffer->accum.header.dataSize = 2;
    buffer->accum.header.dataLength = 0;

    for (i=0; i<PACKET_BUFFER_LEN; i++)
    {
        buffer->packets[i].header = buffer->accum.header;
        buffer->packets[i].header.packet_num = i;
    }

    CPhidgetInterfaceKit_set_OnSensorChange_Handler(interfaceKit, InterfaceKitChangeHandler, buffer);
    CPhidget_set_OnDetach_Handler((CPhidgetHandle)interfaceKit, DetachHandler, buffer);

    return 0;
}

static int ManagerAttach(CPhidgetHandle phid, void *unused)
{
    int i, serialNumber;
    const char *phidgetName;
    CPhidget_DeviceClass deviceClass;

    if (pthread_mutex_lock(&changeHandlerMutex) != 0) perror("pthread_mutex_lock"), cancel=1;
    if (cancel) return 0;
    CPhidget_getSerialNumber(phid, &serialNumber);
    CPhidget_getDeviceName(phid, &phidgetName);
    CPhidget_getDeviceClass(phid, &deviceClass);

    fprintf(stdout, "%s with SN %d attached", phidgetName, serialNumber);
    if (deviceClass == PHIDCLASS_TEMPERATURESENSOR)
    {
        for (i=0; i<numTempHandles; i++)
            if (tempPacketBuffers[i].accum.header.serialNumber == serialNumber) break;
        if (i == numTempHandles)
        {
            tempHandles = realloc(tempHandles, sizeof(CPhidgetTemperatureSensorHandle) * (numTempHandles+1));
            tempPacketBuffers = realloc(tempPacketBuffers, sizeof(SensorPacketBuffer) * (numTempHandles+1));
            memset(tempPacketBuffers + numTempHandles, 0, sizeof(SensorPacketBuffer));
            CPhidgetTemperatureSensor_create(tempHandles + numTempHandles);
            CPhidget_open((CPhidgetHandle)tempHandles[numTempHandles], serialNumber);
            CPhidget_set_OnAttach_Handler((CPhidgetHandle)tempHandles[numTempHandles], TemperatureAttachHandler, tempPacketBuffers + numTempHandles);
            numTempHandles++;
            fprintf(stdout, " (Supported)\n");
        }
        else
        {
            fprintf(stdout, " (Again)\n");
        }
    }
    else if (deviceClass == PHIDCLASS_SPATIAL)
    {
        for (i=0; i<numSpatialHandles; i++)
            if (spatialPacketBuffers[i].accum.header.serialNumber == serialNumber) break;
        if (i == numSpatialHandles)
        {
            spatialHandles = realloc(spatialHandles, sizeof(CPhidgetSpatialHandle) * (numSpatialHandles+1));
            spatialPacketBuffers = realloc(spatialPacketBuffers, sizeof(SensorPacketBuffer) * (numSpatialHandles+1));
            memset(spatialPacketBuffers + numSpatialHandles, 0, sizeof(SensorPacketBuffer));
            CPhidgetSpatial_create(spatialHandles + numSpatialHandles);
            CPhidget_open((CPhidgetHandle)spatialHandles[numSpatialHandles], serialNumber);
            CPhidget_set_OnAttach_Handler((CPhidgetHandle)spatialHandles[numSpatialHandles], SpatialAttachHandler, NULL);
            numSpatialHandles++;
            fprintf(stdout, " (Just Printing Out)\n");
        }
        else
        {
            fprintf(stdout, " (Again)\n");
        }
    }
    else if (deviceClass == PHIDCLASS_BRIDGE)
    {
        for (i=0; i<numBridgeHandles; i++)
            if (bridgePacketBuffers[i].accum.header.serialNumber == serialNumber) break;
        if (i == numBridgeHandles)
        {
            bridgeHandles = realloc(bridgeHandles, sizeof(CPhidgetBridgeHandle) * (numBridgeHandles+1));
            bridgePacketBuffers = realloc(bridgePacketBuffers, sizeof(SensorPacketBuffer) * (numBridgeHandles+1));
            memset(bridgePacketBuffers + numBridgeHandles, 0, sizeof(SensorPacketBuffer));
            CPhidgetBridge_create(bridgeHandles + numBridgeHandles);
            CPhidget_open((CPhidgetHandle)bridgeHandles[numBridgeHandles], serialNumber);
            CPhidget_set_OnAttach_Handler((CPhidgetHandle)bridgeHandles[numBridgeHandles], BridgeAttachHandler, bridgePacketBuffers + numBridgeHandles);
            numBridgeHandles++;
            fprintf(stdout, " (Supported)\n");
        }
        else
        {
            fprintf(stdout, " (Again)\n");
        }
    }
    else if (deviceClass == PHIDCLASS_INTERFACEKIT)
    {
        for (i=0; i<numInterfaceKitHandles; i++)
            if (interfaceKitPacketBuffers[i].accum.header.serialNumber == serialNumber) break;
        if (i == numInterfaceKitHandles)
        {
            interfaceKitHandles = realloc(interfaceKitHandles, sizeof(CPhidgetInterfaceKitHandle) * (numInterfaceKitHandles+1));
            interfaceKitPacketBuffers = realloc(interfaceKitPacketBuffers, sizeof(SensorPacketBuffer) * (numInterfaceKitHandles+1));
            memset(interfaceKitPacketBuffers + numInterfaceKitHandles, 0, sizeof(SensorPacketBuffer));
            CPhidgetInterfaceKit_create(interfaceKitHandles + numInterfaceKitHandles);
            CPhidget_open((CPhidgetHandle)interfaceKitHandles[numInterfaceKitHandles], serialNumber);
            CPhidget_set_OnAttach_Handler((CPhidgetHandle)interfaceKitHandles[numInterfaceKitHandles], InterfaceKitAttachHandler, interfaceKitPacketBuffers + numInterfaceKitHandles);
            numInterfaceKitHandles++;
            fprintf(stdout, " (Supported)\n");
        }
        else
        {
            fprintf(stdout, " (Again)\n");
        }
    }
    else
    {
        fprintf(stdout, " (Not supported yet)\n");
    }

    if (pthread_mutex_unlock(&changeHandlerMutex) != 0) perror("pthread_mutex_unlock"), cancel=1;
    return 0;
}

int main(int argc, char* argv[])
{
    CPhidgetManagerHandle pMan;
    int bad_command_line=0, sendto_port=1027;
    char *sendto_address;
    char recv_buf[UDP_BUFFER_LEN];
    struct sockaddr_in fromADDR;
    socklen_t sizeOfFromADDR = sizeof(fromADDR);
    ssize_t recv_len;
    MicroStrainBS uStrain;

    signal(SIGINT, &sighandle);
    pthread_mutex_init(&changeHandlerMutex, NULL);

    if (argc < 2)
    {
        bad_command_line++;
    }
    else
    {
        sendto_address = argv[1];
        if (inet_addr(sendto_address) == INADDR_NONE)
            fprintf(stderr, "Bad IP Address\n"), bad_command_line++;
    }
    if (bad_command_line)
    {
        fprintf(stderr, "Usage: %s <ip_address>\n", argv[0]);
        return 1;
    }
    if (argc == 3)
        uStrain = MicroStrainBS_create(argv[2], sendto_address, sendto_port);
    else
        uStrain = NULL;

    if ((socketFD = socket(AF_INET,SOCK_DGRAM,0)) <  0)
    {
        perror("Creating UDP Socket");
        exit(1);
    }
    if (fcntl(socketFD, F_SETFL, O_NONBLOCK))
    {
        perror("fcntl set socket to nonblocking");
        exit(1);
    }
    memset(&socketADDR,0,sizeof(socketADDR));
    socketADDR.sin_family = AF_INET;
    socketADDR.sin_addr.s_addr = inet_addr(sendto_address);
    socketADDR.sin_port = htons(sendto_port);

    CPhidgetManager_create(&pMan);
    CPhidgetManager_set_OnAttach_Handler(pMan, ManagerAttach, NULL);
    CPhidgetManager_open(pMan);

    while (!cancel)
    {
        int i;
        usleep(10000);

        for (i=0; i<numTempHandles; i++)
            TemperatureChangeHandler(tempHandles[i], tempPacketBuffers + i, -1, 0);
        for (i=0; i<numInterfaceKitHandles; i++)
            InterfaceKitChangeHandler(interfaceKitHandles[i], interfaceKitPacketBuffers + i, -1, 0);
        for (i=0; i<numBridgeHandles; i++)
            BridgeChangeHandler(bridgeHandles[i], bridgePacketBuffers + i, -1, 0);
        while ((recv_len = recvfrom(socketFD, recv_buf, UDP_BUFFER_LEN, 0, (struct sockaddr *)&fromADDR, &sizeOfFromADDR)) > 0)
        {
            SensorPacket *packet = (SensorPacket *)recv_buf;
            SensorPacket_FromNetworkOrder(packet, recv_len);
            if (packet->header.dataLength + sizeof(SensorPacketHeader) != recv_len || recv_len > sizeof(SensorPacket))
            {
                char AddrStr[32];
                inet_ntop(AF_INET, &(fromADDR.sin_addr), AddrStr, 32);
                fprintf(stderr, "Throwing out bad length packet from %s\n", AddrStr);
                continue;
            }
            if (packet->header.packet_type == SP_TYPE_PHIDGET)
            {
                int i;
                if (packet->header.num_buffered_packets != PACKET_BUFFER_LEN)
                {
                    char AddrStr[32];
                    inet_ntop(AF_INET, &(fromADDR.sin_addr), AddrStr, 32);
                    fprintf(stderr, "Throwing out packet with different buffer length from %s\n", AddrStr);
                    continue;
                }
                if (packet->header.deviceClass == PHIDCLASS_TEMPERATURESENSOR)
                {
                    for (i=0; i<numTempHandles; i++)
                        if (tempPacketBuffers[i].accum.header.serialNumber == packet->header.serialNumber) break;
                    if (i != numTempHandles)
                    {
                        if (packet->header.packet_cmd == SP_CMD_SET_TYPE)
                        {
                            int j;
                            CPhidgetTemperatureSensor_ThermocoupleType t;

                            if (packet->header.dataType == 'E')
                                t = PHIDGET_TEMPERATURE_SENSOR_E_TYPE;
                            else if (packet->header.dataType == 'K')
                                t = PHIDGET_TEMPERATURE_SENSOR_K_TYPE;
                            else if (packet->header.dataType == 'J')
                                t = PHIDGET_TEMPERATURE_SENSOR_J_TYPE;
                            else if (packet->header.dataType == 'T')
                                t = PHIDGET_TEMPERATURE_SENSOR_T_TYPE;
                            else
                            {
                                char AddrStr[32];
                                inet_ntop(AF_INET, &(fromADDR.sin_addr), AddrStr, 32);
                                fprintf(stderr, "Throwing out packet with bad thermocouple type from %s\n", AddrStr);
                                continue;
                            }

                            for (j=0; j<16; j++)
                                if (packet->header.channelMask & (1<<j))
                                    CPhidgetTemperatureSensor_setThermocoupleType(tempHandles[i], j, t);
                        }
                        else
                        {
                            char AddrStr[32];
                            inet_ntop(AF_INET, &(fromADDR.sin_addr), AddrStr, 32);
                            fprintf(stderr, "Throwing out packet with bad command from %s\n", AddrStr);
                            continue;
                        }
                    }
                    else
                    {
                        char AddrStr[32];
                        inet_ntop(AF_INET, &(fromADDR.sin_addr), AddrStr, 32);
                        fprintf(stderr, "Throwing out packet with unknown serialNumber from %s\n", AddrStr);
                        continue;
                    }
                }
                else if (packet->header.deviceClass == PHIDCLASS_BRIDGE)
                {
                    for (i=0; i<numBridgeHandles; i++)
                        if (bridgePacketBuffers[i].accum.header.serialNumber == packet->header.serialNumber) break;
                    if (i != numBridgeHandles)
                    {
                        if (packet->header.packet_cmd == SP_CMD_SET_RATE)
                        {
                            if (packet->header.channelMask)
                                CPhidgetBridge_setDataRate(bridgeHandles[i], 1000.0 / packet->header.channelMask);
                        }
                        else
                        {
                            char AddrStr[32];
                            inet_ntop(AF_INET, &(fromADDR.sin_addr), AddrStr, 32);
                            fprintf(stderr, "Throwing out packet with bad command from %s\n", AddrStr);
                            continue;
                        }
                    }
                    else
                    {
                        char AddrStr[32];
                        inet_ntop(AF_INET, &(fromADDR.sin_addr), AddrStr, 32);
                        fprintf(stderr, "Throwing out packet with unknown serialNumber from %s\n", AddrStr);
                        continue;
                    }
                }
                else if (packet->header.deviceClass == PHIDCLASS_SPATIAL)
                {
                }
                else if (packet->header.deviceClass == PHIDCLASS_INTERFACEKIT)
                {
                }
                else
                {
                    char AddrStr[32];
                    inet_ntop(AF_INET, &(fromADDR.sin_addr), AddrStr, 32);
                    fprintf(stderr, "Throwing out packet with unknown deviceClass from %s\n", AddrStr);
                    continue;
                }
            }
            else
            {
                char AddrStr[32];
                inet_ntop(AF_INET, &(fromADDR.sin_addr), AddrStr, 32);
                fprintf(stderr, "Throwing out unknown packet type from %s\n", AddrStr);
                continue;
            }
        }
    }

    CPhidgetManager_close(pMan);
    CPhidgetManager_delete(pMan);
    if (uStrain)
        MicroStrainBS_destroy(uStrain);

    fprintf(stdout, "\nClosed %s cleanly\n", argv[0]);

    return 0;
}

