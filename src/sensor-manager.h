#ifndef SENSOR_MANAGER_H
#define SENSOR_MANAGER_H

#include <sys/time.h>

#ifdef __cplusplus
extern "C" {
#endif 

typedef struct _SensorManager *SensorManager;

SensorManager SensorManager_create(unsigned short port);
void          SensorManager_destroy(SensorManager sensorManager);
void          SensorManager_getNamesAndUnits(SensorManager sensorManager, char ***names, char ***units, int *count);
void          SensorManager_setCallback(SensorManager sensorManager, void(*fptr)(struct timeval timestamp, float *values, int count));
void          SensorManager_clearCallback(SensorManager sensorManager);
void          SensorManager_floodCallback(SensorManager sensorManager);
void          SensorManager_setRate(SensorManager sensorManager, int index, unsigned short Hz);
void          SensorManager_setThermocoupleType(SensorManager sensorManager, int index, unsigned char ThermocoupleType);
void          SensorManager_nullSensor(SensorManager sensorManager, int index);
void          SensorManager_nullSensors(SensorManager sensorManager, char *commaSeparatedNames);
void          SensorManager_setEUfunction(SensorManager sensorManager, int index, char *newUnits, float(*fptr)(float in, void *parameters), void *parameters);

#ifdef __cplusplus
}
#endif 

#endif  // SENSOR_MANAGER_H
