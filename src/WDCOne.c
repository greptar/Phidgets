/* This single-file program is the first preliminary code for the WDC Bridge Node.

Created by KCM at Schafer Corp, May 2012

Does not send any real data!  Just fabricated data for the time being.

Spawns two threads:
1) UDPFunction just waits for the CS Server to ask for an IP address
2) TCPFunction responds to all the commands from the CS

the main program will run in an infinite loop, sending data to the CS as requested.
The TCP function, in response to CS commands, sets flags for the main program.

Timing is just based on the local clock.

All the printfs are just for the programmer to know that things are working.
*/

#define MS 1000					// milliseconds per second
#define USPERMS 1000			// microseconds per millisecond
#define BUFSIZE 256 			// for character buffers
#define MINDATASTEP 100 		// Fastest ms interval for sending data
#define DEFDATASTEP 2000		// Default ms interval for sending data
#define MAXDATASTEP 100000		// Slowest ms interval for sending data
#define UDPINPORT 6551			// Port to listen for "GetDC" from the CS
#define UDPOUTPORT 6552			// Port to reply to "GetDC"
#define TCPPORT 6553			// Port for all TCP Communication
#define WDCID 300				// Should be in a config file - THREE DIGITS for recording file name
static char *LOCALIP="192.168.52.104";	// ditto
#define PADSTR "                         "	// 25 blanks
#define BYTESPERINT 4			// Bytes in an integer, for messaging.

#define DUMMYSENSOR "W_Sensor_"
#define NUMSENSORS 3
#define DUMMYUNIT "mho"

// These are the messages from the CS:
#define GET_WDC_NAME 0
#define START_SCANNING 1
#define STOP_SCANNING 2
#define SCAN_DATA 3
#define LOAD_SCANLIST 4
#define ERROR_Renamed 5
#define WARNING 6
#define SET_SCAN_RATE 7
#define ARM_SYNCHRONOUS_START 8
#define GET_UNITS 9
#define SET_WDC_RECORDING 10
#define SET_WDC_RECORDING_RATE 11
#define SET_WDC_PICKOFF_RATE 12
#define SET_WDC_OVER_SAMPLING_RATE 13
#define NULL_SENSORS 14

#define BOOL_STR(b) ((b)?"true":"false")

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <sys/time.h>
#include <time.h>
#include <string.h>
#include <pthread.h>
#include <ctype.h>
//naw #include <iostream>

int UDP_sockfd = 0;
int TCP_sockfd = 0;
int SC_sockfd = 0;
socklen_t UDP_len = 0;
socklen_t TCP_len = 0;
socklen_t SC_len = 0;
struct sockaddr_in UDP_addr = {0};
struct sockaddr_in TCP_addr = {0};
struct sockaddr_in SC_addr = {0};
struct sockaddr_in SELF_addr = {0};
char readbuffer[BUFSIZE];
char sendbuffer[BUFSIZE];
char timebuffer[BUFSIZE];
char errorbuffer[BUFSIZE];
char AddrStr[INET_ADDRSTRLEN];
char RecordingFile[BUFSIZE];
int scanbuffer[BUFSIZE];

// These struct used for time stamp
struct timeval  tv;
struct timezone tz;
struct tm *tm;
unsigned long DataTime = 0;		// Will be loop starting time (ms)
unsigned long BaseSecs = 0;		// Starting reference (sec)
unsigned long NowTime = 0;		// Milliseconds since BaseTime

int ScanStep = DEFDATASTEP;		// Will accept CS-commanded change
int RecordStep = 1;				// Not Implemented yet.
int PickOffStep = 1;			// Not Implemented yet.
int OverSampleStep = 1;			// Not Implemented yet.

// For the threads.
pthread_t UDPthread, TCPthread, DCThread;	// DCThread not used yet...
pthread_attr_t tattr;		// Thread attributes

// These are all inter-thread flags
int UDPWait = 1;
int TCPWait = 1;
int Scan = 0;
// Notice: WDC is currently not capable of being the "Master"
int Master = 0;		// Set to 1 if Master, 0 if Slave
// Notice: Recording is not yet implemented"
int Record = 0;		// Set to 1 to record, 0 otherwise


static void hexdump(void *data, int size)
{
	/* dumps size bytes of *data to stdout. Looks like:
	* [0000] 75 6E 6B 6E 6F 77 6E 20
	*                  30 FF 00 00 00 00 39 00 unknown 0.....9.
	* (in a single line of course)
	*/

	unsigned char *p = (unsigned char *) data;
	unsigned char c;
	int n;
	char bytestr[4] = {0};
	char addrstr[10] = {0};
	char hexstr[ 16*3 + 5] = {0};
	char charstr[16*1 + 5] = {0};
	for(n=1;n<=size;n++) {
		if (n%16 == 1) {
			/* store address for this line */
			snprintf(addrstr, sizeof(addrstr), "%.4x",
				((unsigned int)p-(unsigned int)data) );
		}

		c = *p;
		if (isalnum(c) == 0) {
			c = '.';
		}

		/* store hex str (for left side) */
		snprintf(bytestr, sizeof(bytestr), "%02X ", *p);
		strncat(hexstr, bytestr, sizeof(hexstr)-strlen(hexstr)-1);

		/* store char str (for right side) */
		snprintf(bytestr, sizeof(bytestr), "%c", c);
		strncat(charstr, bytestr, sizeof(charstr)-strlen(charstr)-1);

		if(n%16 == 0) { 
			/* line completed */
			printf("[%4.4s]   %-50.50s  %s\n", addrstr, hexstr, charstr);
			hexstr[0] = 0;
			charstr[0] = 0;
		} else if(n%8 == 0) {
			/* half line: add whitespaces */
			strncat(hexstr, "  ", sizeof(hexstr)-strlen(hexstr)-1);
			strncat(charstr, " ", sizeof(charstr)-strlen(charstr)-1);
		}
		p++; /* next byte */
	}

	if (strlen(hexstr) > 0) {
		/* print rest of buffer if not empty */
		printf("[%4.4s]   %-50.50s  %s\n", addrstr, hexstr, charstr);
	}
}

// An Error handling routine
void error(const char *msg)
{
	perror(msg);
	exit(1);
}

char * GetTimeStr()
{
	gettimeofday(&tv, &tz);
	tm = localtime(&tv.tv_sec);
	sprintf(timebuffer,"%02d/%02d/%04d %02d:%02d:%02d.%03d: ",
		tm->tm_mon, tm->tm_mday, tm->tm_year + 1900,
		tm->tm_hour, tm->tm_min, tm->tm_sec, (int)tv.tv_usec/1000); 
	return timebuffer;
}

char * CreateRecordingFileName()
{
	gettimeofday(&tv, &tz);
	tm = localtime(&tv.tv_sec);
	sprintf(RecordingFile,"WDC%3d_Recording_File_%02d%02d%04d_%02d%02d%02d.txt",WDCID,
		tm->tm_mon, tm->tm_mday, tm->tm_year + 1900,
		tm->tm_hour, tm->tm_min, tm->tm_sec); 
	return timebuffer;
}

// UDP function will run forever as a thread.
void *UDPFunction(void *UDParam)
{
	char UDPbuffer[BUFSIZE];
	int uReturn = 0;
	// Open the UDP Socket
	bzero(UDPbuffer,BUFSIZE);
	gethostname(UDPbuffer,BUFSIZE-1);
	printf("\n%sStarting Server on %s...\n",GetTimeStr(),UDPbuffer);
	UDP_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
	if (UDP_sockfd < 0) error("ERROR opening UDP socket");
	UDP_len = sizeof(UDP_addr);
	bzero((char *) &UDP_addr, UDP_len);
	UDP_addr.sin_family = AF_INET;
	UDP_addr.sin_addr.s_addr = INADDR_ANY;
	UDP_addr.sin_port = htons(UDPINPORT);
	if (bind(UDP_sockfd, (struct sockaddr *) &UDP_addr, UDP_len) < 0)
		error("ERROR on binding UDP Socket");		  
	printf("%sSuccessfully opened UDP Socket on port #%d\n",GetTimeStr(),UDPINPORT);

	UDPWait = 0;		// This flag tells the main program to continue

	// Will respond to repeated UDP messages from the CS.
	while (1) {
		bzero(UDPbuffer,BUFSIZE);
		usleep(10*MS);
		printf("%sWaiting for a UDP message on port %d\n",GetTimeStr(),UDPINPORT);
		// Wait here until we receive any message at all...
		uReturn = recvfrom(UDP_sockfd, UDPbuffer, BUFSIZE-1,MSG_WAITALL, (struct sockaddr *) &UDP_addr, &UDP_len);
		if (uReturn < 0) error("ERROR on Receiving on UDP Socket");  	 
		// Report the IP address of the CS
		inet_ntop(AF_INET, &(UDP_addr.sin_addr), AddrStr, INET_ADDRSTRLEN);
		printf("%sReceived UDP message from CS, IP Address = %s on Port %d\n",GetTimeStr(),AddrStr,UDPINPORT);	   
		// Report what the UDP Message said
		printf("%sThis is the UDP message:  %s\n",PADSTR,UDPbuffer);
		strcpy(UDPbuffer,LOCALIP);
		UDP_addr.sin_port = htons(UDPOUTPORT);
		uReturn = sendto(UDP_sockfd, UDPbuffer, strlen(UDPbuffer), 0, (struct sockaddr *) &UDP_addr,  UDP_len);
		if (uReturn < 0) error("ERROR Sending on UDP Socket");  	 
		printf("%sSent UDP Reply To CS: %s\n",GetTimeStr(),UDPbuffer);
	}
	return UDParam;
}

// This eternal thread is activated after the UDPFunction.
// It will wait for the CS to connect,
// then it will continuously respond to TCP Commands from the CS.
void *TCPFunction(void *TCParam)
{
	int TCPbuffer[BUFSIZE];
	//	int Holdbuffer[BUFSIZE];
	int TCPReply[BUFSIZE];
	char *TCPPointer = NULL;
	int ReadByteCount = 0;
	int tReturn = 0;
	int rit,sit = 0;
	
	TCP_sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (TCP_sockfd < 0) error("ERROR opening TCP socket");
	TCP_len = sizeof(TCP_addr);
	bzero((char *) &TCP_addr, TCP_len);
	TCP_addr.sin_family = AF_INET;
	TCP_addr.sin_addr.s_addr = INADDR_ANY;
	TCP_addr.sin_port = htons(TCPPORT);
	tReturn = bind(TCP_sockfd, (struct sockaddr *) &TCP_addr, TCP_len);
	if (tReturn < 0) error("ERROR on binding TCP Socket");		  
	printf("%sSuccessfully opened TCP Socket on port #%d\n",GetTimeStr(),TCPPORT);

	TCPWait = 0;	// This flag tells the main program to continue

	printf("%sListening for TCP connect. %s\n",GetTimeStr(),AddrStr);
	listen(TCP_sockfd,5);		// "5" is the maximum # connections allowed by most systems
	// Now that a message has been received, get an IP address, and establish a connection
	SC_sockfd = accept(TCP_sockfd, (struct sockaddr *)&TCP_addr, &TCP_len);
	if (SC_sockfd < 0) error("ERROR accepting TCP Socket");
	printf("%sTCP Socket Connected. %s\n",GetTimeStr(),AddrStr);

	bzero(TCPbuffer,BUFSIZE);
	// Forever respond to CS Messages.
	while (1) {
		printf("%sWaiting for a TCP message on port %d\n",GetTimeStr(),TCPPORT);
		ReadByteCount = read(SC_sockfd,TCPbuffer,BUFSIZE);
		if (ReadByteCount < 0) error("ERROR reading from TCP socket");
		if (ReadByteCount == 0) return TCParam;
		//		 hexdump(TCPbuffer,16);
		rit = 0;
		while (ReadByteCount > 0) {
			bzero(TCPReply,BUFSIZE);
			switch(TCPbuffer[rit]) {
			case GET_WDC_NAME:
				TCPReply[0] = GET_WDC_NAME;
				TCPReply[1] = BYTESPERINT; 				// Number of Bytes to come...
				TCPReply[2] = WDCID;
				tReturn = send(SC_sockfd,TCPReply,3*BYTESPERINT,0);	// This message is always 3 ints.
				if (tReturn < 0) error("ERROR Sending WDC NAME");
				printf("%sReceived GET_WDC_NAME from CS, then sent: %d %d %d\n",
					GetTimeStr(),TCPReply[0],TCPReply[1],TCPReply[2]);
				break;
			case START_SCANNING:
				Scan = 1;	// flag looked at by main
				printf("%sReceived START_SCANNING from CS\n",GetTimeStr());
				break;
			case STOP_SCANNING:
				Scan = 0;	// flag looked at by main
				printf("%sReceived STOP_SCANNING from CS\n",GetTimeStr());
				break;
			case SCAN_DATA:
				printf("%sReceived SCAN_DATA from CS\n",GetTimeStr());
				break;
			case LOAD_SCANLIST:
				printf("%sReceived LOAD_SCANLIST from CS\n",GetTimeStr());
				// Reply with a sensor list.
				TCPReply[0] = LOAD_SCANLIST;
				TCPReply[1] = 0;
				TCPPointer = (char *) &(TCPReply[2]);  // So we can put in several strings.
				for (sit=0;sit<NUMSENSORS;sit++) {
					sprintf(sendbuffer,"%s%d",DUMMYSENSOR,sit+1);		// This string identifies a sensor
					printf("%s%s\n",PADSTR,sendbuffer);					// This is just for display
					strcpy(TCPPointer,sendbuffer);						// Put the string into the reply buffer
					TCPPointer += strlen(sendbuffer);					// Bump the pointer
					*TCPPointer++ = ',';								// Comma delimiter
					TCPReply[1] += strlen(sendbuffer) + 1; 				// Bump the byte counter.
				}
				*TCPPointer = 0;	// String delimiter
				TCPReply[1] -= 1;	// Decrement the byte count
				tReturn = send(SC_sockfd,TCPReply,2*BYTESPERINT + TCPReply[1],0);
				//				hexdump(TCPReply,TCPReply[1] + 2*BYTESPERINT);
				if (tReturn < 0) error("ERROR Sending LOAD_SCANLIST Message!");				
				break;
			case ERROR_Renamed:
				printf("%sReceived ERROR_Renamed from CS\n",GetTimeStr());
				break;
			case WARNING:
				printf("%sReceived WARNING from CS\n",GetTimeStr());
				break;
			case SET_SCAN_RATE:
				printf("%sReceived SET_SCAN_RATE %d from CS\n",GetTimeStr(),TCPbuffer[rit+2]);
				ScanStep = (int) (TCPbuffer[rit+2]/USPERMS);	// Convert from int microseconds to int milliseconds
				if (ScanStep < MINDATASTEP) {
					ScanStep = MINDATASTEP;
					sprintf(errorbuffer,"Setting Scan Step to minimum allowable %d ms",ScanStep);
					printf("%s%s\n",PADSTR,errorbuffer);
					TCPReply[0] = WARNING;
					TCPReply[1] = strlen(errorbuffer); 				// Number of Bytes to come...
					strcpy(((char*)TCPReply) + 2*BYTESPERINT,errorbuffer);
					tReturn = send(SC_sockfd,TCPReply,2*BYTESPERINT + strlen(errorbuffer),0);
					if (tReturn < 0) error("ERROR Sending SET_SCAN_RATE Error Message!");
				}
				if (ScanStep > MAXDATASTEP) {
					ScanStep = MAXDATASTEP;
					sprintf(errorbuffer,"Setting Scan Step to maximum allowable %d ms",ScanStep);
					printf("%s%s\n",PADSTR,errorbuffer);
					TCPReply[0] = ERROR_Renamed;
					TCPReply[1] = strlen(errorbuffer); 				// Number of Bytes to come...
					strcpy(((char*)TCPReply) + 2*BYTESPERINT,errorbuffer);
					tReturn = send(SC_sockfd,TCPReply,2*BYTESPERINT + strlen(errorbuffer),0);
					if (tReturn < 0) error("ERROR Sending SET_SCAN_RATE Error Message!");
				}
				// Echo Reply to the CS (I thought this was protocol, but now I think not -KCM)
//				TCPReply[0] = SET_SCAN_RATE;
//				TCPReply[1] = BYTESPERINT;
//				TCPReply[2] = ScanStep*USPERMS;
//				tReturn = send(SC_sockfd,TCPReply,3*BYTESPERINT,NULL);
//				if (tReturn < 0) error("ERROR Sending SET_SCAN_RATE Message!");
//				printf("%sSent SET_SCAN_RATE %d To CS\n",GetTimeStr(),TCPReply[2]);
				break;
			case ARM_SYNCHRONOUS_START:
				printf("%sReceived ARM_SYNCHRONOUS_START from CS\n",GetTimeStr());
				Master = (TCPbuffer[rit+2] != 0);	// zero for false, non-zero otherwise
				if (0 != Master) {
					sprintf(errorbuffer,"Set to be Master, but can't be!");
					printf("%s%s\n",GetTimeStr(),errorbuffer);
					TCPReply[0] = ERROR_Renamed;
					TCPReply[1] = strlen(errorbuffer); 				// Number of Bytes to come...
					strcpy(((char*)TCPReply) + 2*BYTESPERINT,errorbuffer);
					tReturn = send(SC_sockfd,TCPReply,2*BYTESPERINT + strlen(errorbuffer),0);
					if (tReturn < 0) error("ERROR Sending ARM_SYNCHRONOUS_START Error Message!");
				}				
				break;
			case GET_UNITS:
				printf("%sReceived GET_UNITS from CS\n",GetTimeStr());
				// Reply with a Units list.
				TCPReply[0] = GET_UNITS;
				TCPReply[1] = 0;
				TCPPointer = (char *) &(TCPReply[2]);  // So we can put in several strings.
				for (sit=0;sit<NUMSENSORS;sit++) {
					sprintf(sendbuffer,"%s%d",DUMMYUNIT,sit+1);			// This string identifies a unit
					printf("%s%s\n",PADSTR,sendbuffer);					// This is just for display
					strcpy(TCPPointer,sendbuffer);						// Put the string into the reply buffer
					TCPPointer += strlen(sendbuffer);					// Bump the pointer
					*TCPPointer++ = ',';								// Comma delimiter
					TCPReply[1] += strlen(sendbuffer) + 1; 				// Bump the byte counter.
				}
				*TCPPointer = 0;	// String delimiter
				TCPReply[1] -= 1;	// Decrement the byte count
				tReturn = send(SC_sockfd,TCPReply,2*BYTESPERINT + TCPReply[1],0);
				if (tReturn < 0) error("ERROR Sending GET_UNITS Message!");				
				break;
			case SET_WDC_RECORDING:
				Record = (TCPbuffer[rit+2] != 0);	// zero for false, non-zero otherwise
				if (Record) {
					CreateRecordingFileName();
					printf("%sRecording set to %s, %s\n",GetTimeStr(),BOOL_STR(Record),RecordingFile);
					}
				else {
					printf("%sRecording set to %s\n",GetTimeStr(),BOOL_STR(Record));
					}
				break;
			case SET_WDC_RECORDING_RATE:
				RecordStep = TCPbuffer[rit+2];
				printf("%sSET_WDC_RECORDING_RATE %d\n",GetTimeStr(),RecordStep);
				break;
			case SET_WDC_PICKOFF_RATE:
				PickOffStep = TCPbuffer[rit+2];
				printf("%sSET_WDC_PICKOFF_RATE %d\n",GetTimeStr(),PickOffStep);
				break;
			case SET_WDC_OVER_SAMPLING_RATE:
				OverSampleStep = TCPbuffer[rit+2];
				printf("%sSET_WDC_OVER_SAMPLING_RATE %d\n",GetTimeStr(),OverSampleStep);
				break;
			case NULL_SENSORS:
				printf("%sReceived NULL_SENSORS from CS\n",GetTimeStr());
				break;
			default:
				printf("%sReceived UNRECOGNIZED MESSAGE from CS: %d\n",GetTimeStr(),TCPbuffer[rit]);
				break;
			}
//			printf("Before rit = %d, ByteCount = %d\n",rit,ReadByteCount);
			ReadByteCount = ReadByteCount - 8 - TCPbuffer[rit + 1];
			rit = rit + 2 + TCPbuffer[rit + 1]/4;
//			printf("After rit = %d, ByteCount = %d\n",rit,ReadByteCount);
//			usleep(10*MS);	// sleep for 10 milliseconds			
		}
	
	}
	return TCParam;
}

// main will spawn the other two threads
// then it will forever collect and send data,
// as long as CS wants it.
int main(int argc, char *argv[])
{
	int nReturn = 0;	// Used for checking return values for errors
	int it = 0, nit = 0;	// iterators
	float *FloatPointer = NULL;			// Only used because I am converting iterator to float data....
	
// Have tried messing with threads to get rid of hangup on 2nd UDP message.  To no avail -KCM
	/* initialized thread attributes with default values */
	nReturn = pthread_attr_init(&tattr);
	/* alter default thread attributes */
//	nReturn = pthread_attr_setdetachstate(&tattr,PTHREAD_CREATE_DETACHED);
//    int policy;
//   struct sched_param param;

	int UDParam = 0, bad_command_line=0;

        if (argc != 2)
        {
                bad_command_line++;
        }
        else
        {
                LOCALIP=argv[1];
        }
        if (bad_command_line)
        {
                fprintf(stderr, "Usage: %s <My IP>\n", argv[0]);
                return 1;
        }

	nReturn = pthread_create( &UDPthread, &tattr, UDPFunction, &UDParam);
 //   nReturn = pthread_getschedparam(UDPthread, &policy, &param);
//	policy = SCHED_RR;
//	nReturn = pthread_setschedparam(UDPthread, policy, &param);
	while (UDPWait) usleep(10*MS);
	int TCParam = 0;
	nReturn = pthread_create( &TCPthread, &tattr, TCPFunction, &TCParam);
	while (TCPWait) usleep(10*MS);

	gettimeofday(&tv, &tz);
	tm = localtime(&tv.tv_sec);
	printf("DATE %02d/%02d/%04d TIME %02d:%02d:%02d.%03d\n",
		tm->tm_mon, tm->tm_mday, tm->tm_year + 1900,
		tm->tm_hour, tm->tm_min, tm->tm_sec, (int)tv.tv_usec/1000); 

	// initialize the time counters
	gettimeofday(&tv, &tz);
	BaseSecs = tv.tv_sec;
	NowTime = MS*(tv.tv_sec - BaseSecs) + tv.tv_usec/MS;
	DataTime = NowTime + ScanStep;
	printf("NowTime = %lu     DataTime = %lu\n",NowTime,DataTime);

	it = 1;
	//	Report data as long as CS wants it.
	while (1)
	{
		usleep(10*MS);	// sleep for 10 milliseconds
		gettimeofday(&tv, &tz);
		if (BaseSecs > tv.tv_sec)
			error("sec Clock Roll Over!");
		tm = localtime(&tv.tv_sec);
		NowTime = MS*(tv.tv_sec - BaseSecs) + tv.tv_usec/MS;

		if ( NowTime >= DataTime)		// is it time for data?
		{
			if (Scan)
			{
				scanbuffer[0] = SCAN_DATA;
				scanbuffer[1] = BYTESPERINT + sizeof(float)*NUMSENSORS;
				scanbuffer[2] = it;			// Scan Count
				FloatPointer = (float *) &(scanbuffer[3]);
				for (nit=0; nit < NUMSENSORS; nit++) {
					*FloatPointer = (float)it, FloatPointer++;
					}
				nReturn = send(SC_sockfd,scanbuffer,scanbuffer[1]+2*BYTESPERINT,0);
				if (nReturn < 0) error("ERROR Sending Data");
				//				SendMsg(sendbuffer);

				// Here, output time to the console for my own debugging.
				gettimeofday(&tv, &tz);
				tm = localtime(&tv.tv_sec);
				printf("DATE %02d/%02d/%04d TIME %02d:%02d:%02d.%03d   %f   %f   %f\n",
					tm->tm_mon, tm->tm_mday, tm->tm_year + 1900,
					tm->tm_hour, tm->tm_min, tm->tm_sec, (int)tv.tv_usec/1000, (float)it, (float)it, (float)it);
					DataTime = DataTime + ScanStep;	// for next time
//				fprintf(stdout,"PrevTime = %d     NextTime = %d\n",NowTime,DataTime);
				it++;
			}
		}
	}

	close(TCP_sockfd);
	close(UDP_sockfd);
	
	return 0; 
}
