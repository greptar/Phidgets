//=============================================================================
// (c) Copyright 2009 Diamond Systems Corporation. Use of this source code
// is subject to the terms of Diamond Systems' Software License Agreement.
// Diamond Systems provides no warranty of proper performance if this
// source code is modified.
//
// File: DSCADSampleInt.c   v1.00
//
// Depedency :	Universal Driver version 6.02
//				DSCUD.H - Include file.
//				DSCUDBCL.LIB - Library file for MS-DOS
//						( USE BC 5.02 compiler )
//				LIBDSCUD_6.02_1.a	- Library file for Linux 2.6.23
//						( USE GCC 3.xx compiler )
//				DSCUD.LIB	- Static library file for Windows XP
//						( USE VC++ 6.0 or Visual Studio 2005 )
//				DSCUD_API.LIB	- Static library for Windows CE 6.0
//						( Use Visual Studio 2005 with Platform Builder 6.0 )
// Description: This sample code demonstrates the usage of UD function
//				dscADSampleInt(). This sample code describes the usage
//				for Helios SBC. The driver will use the incumbent OS
//              in conjunction with the interrupt selected to transfer many
//              values from the A/D converter to user space, and once stopped,
//              will display a single set of those channel values,
//              interpretting them based on the mode given by the user.
// ************************* History ********************
// Version		Engineer		Date		Description
// 2.0                  Nathan Wharton          12/12/11        Schafer version
// 1.0			James Moore		07/31/09	Created.
//=============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <sys/time.h>
#include <signal.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>

#include "dscud.h"

#define HELIOS_DEFAULT_BASE_ADDRESS 0x280
#define HELIOS_DEFAULT_IRQ 5
#define HELIOS_VOLTAGE_RANGE RANGE_10

#define NUM_PHIDGETS_TEMPS 4
#define MAX_PHIDGETS_RATE 50

#define FIFO_MINIMUM 1
#define FIFO_NOFACTOR_MAX 48
#define FIFO_FACTOR_MAX 1024
#define FIFO_FACTOR 8

// command line variables.  Set to example values
static int   polarity = 0;
static int   differ = 1;
static int   gain = 0;
static int   scan_int = 20;
static int   num_chan = 8;
static int   hz = 1000;
static int   fifo_size = 2000;

static DSCB dscb;                   // handle used to refer to the board
static DSCADSETTINGS dscadsettings; // structure containing A/D conversion settings
static DSCAIOINT dscaioint;         // structure containing interrupt based acquire settings

int main(int argc, char **argv)
{
	DSCCB dsccb;                 // structure containing board settings (IRQ and Address)
	ERRPARAMS errorParams;       // structure for returning error code and error string
	DSCSAMPLE *scan_buffer;
	DSCADCALPARAMS dscadcalparams;

        //=========================================================================
	// 0.1 Calculations from command line
        //=========================================================================

	scan_buffer = calloc(fifo_size, sizeof(DSCSAMPLE));
        if (!scan_buffer)
        {
                fprintf(stderr, "Could not allocate memory.\n");
                return 1;
        }

	//=========================================================================
	// I. DRIVER INITIALIZATION
	//
	//    Initializes the DSCUD library.
	//
	//=========================================================================

	if (dscInit(DSC_VERSION) != DE_NONE)
	{
		dscGetLastError(&errorParams);
		fprintf(stderr, "dscInit error: %s %s\n", dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
		return 1;
	}

	//=========================================================================
	// II. BOARD INITIALIZATION
	//
	//	   Initialize the HELIOS board. This function passes the various
	//	   hardware parameters to the driver and resets the hardware.
	//
	//=========================================================================

	dsccb.base_address = HELIOS_DEFAULT_BASE_ADDRESS;
        dsccb.int_level = HELIOS_DEFAULT_IRQ;

	if (dscInitBoard(DSC_HELIOS, &dsccb, &dscb) != DE_NONE)
	{
		dscGetLastError(&errorParams);
		fprintf(stderr, "dscInitBoard error: %s %s\n",
			dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
		return 1;
	}

	//=========================================================================
	// III. AD SETTINGS INITIALIZATION
	//
	//	    Initialize the structure containing the AD conversion settings and
	//		then pass it to the driver.
	//
	//=========================================================================

	memset(&dscadsettings, 0, sizeof(DSCADSETTINGS));

        // printf ( "Enter the polarity (0 for BIPOLAR, 1 for UNIPOLAR; default: 0): " );
    	dscadsettings.polarity = polarity;
	// The Helios SBC only has a 10V physical range.
	dscadsettings.range = HELIOS_VOLTAGE_RANGE;
	// printf ( "Enter the gain\n(0 for GAIN 1, 1 for GAIN 2, 2 for GAIN 4, 3 for GAIN 8; default: 0): " );
    	dscadsettings.gain = gain;

	dscadsettings.load_cal = 0;
	dscadsettings.current_channel = 0;

	if (scan_int == 4) dscadsettings.scan_interval = SCAN_INTERVAL_4;
	if (scan_int == 5) dscadsettings.scan_interval = SCAN_INTERVAL_5;
	if (scan_int == 9) dscadsettings.scan_interval = SCAN_INTERVAL_9;
	if (scan_int == 10) dscadsettings.scan_interval = SCAN_INTERVAL_10;
	if (scan_int == 15) dscadsettings.scan_interval = SCAN_INTERVAL_15;
	if (scan_int == 20) dscadsettings.scan_interval = SCAN_INTERVAL_20;
	dscadsettings.addiff = differ;

	if (dscADSetSettings(dscb, &dscadsettings) != DE_NONE)
	{
		dscGetLastError(&errorParams);
		fprintf(stderr, "dscADSetSettings error: %s %s\n",
			dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
		return 1;
        }

	//=========================================================================
	// IV. I/O INTERRUPT SETTINGS INITIALIZATION
	//
	//	   Initialize the structure containing the analog I/O interrupt
	//	   settings.
	//
	//	   NOTE: You must allocate space for the buffer holding the returned
	//		     sample values. Also, be generous in allocating storage.
	//			 Allocating insufficient memory to hold sample data will result
	//			 in improper behavior of the driver, such as hanging interrupt
	//			 operations or assertion errors.
	//
	//=========================================================================

	memset(&dscaioint, 0, sizeof(DSCAIOINT));

        dscaioint.num_conversions = fifo_size;
    	dscaioint.conversion_rate = hz;
    	dscaioint.cycle = 1;
    	dscaioint.internal_clock  = 1;
    	dscaioint.low_channel = 0;
    	dscaioint.high_channel = num_chan - 1;

	dscaioint.external_gate_enable = 0; // can enable it if need be
	dscaioint.internal_clock_gate = 0;   // can enable it if need be
	dscaioint.fifo_enab = 1;

    	dscaioint.fifo_depth = fifo_size;
    	dscaioint.dump_threshold = fifo_size;
        dscaioint.sample_values = scan_buffer;

	//=========================================================================
	// V. SAMPLING AND OUTPUT
	//
	//    Perform the actual sampling and then output the results. To calculate
	//	  the actual input voltages, we must convert the sample code (which
	//	  must be cast to a short to get the correct code) and then plug it
	//	  into one of the formulas located in the manual for your board (under
	//	  "A/D Conversion Formulas").
	//=========================================================================

	dscadcalparams.adrange = 255;
	dscadcalparams.boot_adrange = 8;
	if (dscADAutoCal(dscb, &dscadcalparams) != DE_NONE)
	{
		dscGetLastError(&errorParams);
		fprintf(stderr, "dscADAutoCal error: %s %s\n",
			dscGetErrorString(errorParams.ErrCode), errorParams.errstring);
	}

	free(scan_buffer);
	dscFree();
	return 0;
}
