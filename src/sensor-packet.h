#ifndef SENSOR_PACKET_H
#define SENSOR_PACKET_H

#define SP_TYPE_PULSE 0
#define SP_TYPE_PHIDGET 1
#define SP_TYPE_USTRAIN 2
#define SP_TYPE_XBEE 3
#define SP_TYPE_SYNAPSE 4
#define SP_TYPE_LEICA 5

#define SP_CMD_DATA 0
#define SP_CMD_RESEND 1
#define SP_CMD_NULL 2
#define SP_CMD_ENABLE 3
#define SP_CMD_DISABLE 4
#define SP_CMD_SET_RATE 5
#define SP_CMD_SET_TYPE 6
#define SP_CMD_SET_GAIN 7

typedef struct SensorPacketHeader // Need to keep this aligned.  Don't change unless you know how.
{
	int            time_sec;
	int            time_usec;
        unsigned char  packet_type;           // See SP_TYPEs above
        unsigned char  packet_cmd;            // See SP_CMDs above
        unsigned char  packet_num;            // Starts at 0, goes to num_buffered_packets - 1
        unsigned char  num_buffered_packets;  // number of packets kept in a circular buffer
	unsigned int   serialNumber;
	unsigned short deviceClass;           // device class, if SP_TYPE_PHIDGETS or model number if SP_TYPE_USTRAIN
	unsigned short channelMask;           // 1's in the bit positions of the channels this packet is about
	unsigned char  dataType;	      // 0 for unsigned, 1 for float
	unsigned char  dataSize;	      // number of bytes per value
	unsigned short dataLength;	      // Should equal num channels * size
} SensorPacketHeader;

typedef struct SensorPacket
{
        SensorPacketHeader header;
        union
        {
                unsigned short uShorts[16]; // 16 is the most it can be.  See channelMask above for actual length
                float floats[16];
        } data;
} SensorPacket;

void SensorPacket_ToNetworkOrder(SensorPacket *packet);
void SensorPacket_FromNetworkOrder(SensorPacket *packet, int length);

#endif
