#include <stdio.h>
#include <string.h> /* memset */
#include <unistd.h> /* usleep */
#include <stdlib.h> /* exit */
#include <sys/io.h> /* inb, outb */
#include <assert.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define IO_BASE 0x300

#define RECV_ON_PORT 1367
#define RECV_ON_IP "224.0.0.100"
#define SEND_TO_IP "224.0.0.100"
#define SEND_TO_PORT 1365

int main(int argc, char **argv)
{
	int i, sd, rd;
	char buf[256];
        struct sockaddr_in saddr, raddr;
	struct ip_mreq imreq;

	unsigned long base = IO_BASE;
	if (ioperm(base, 8, 1) == -1)
	{
		fprintf(stderr, "Unable to get I/O Permissions\n");
		exit(1);
	}

        /* set up send to address */
        if ((sd = socket(AF_INET,SOCK_DGRAM,0)) <  0) {
                perror("send socket");
                exit(1);
        }
        memset(&saddr,0,sizeof(saddr));
        saddr.sin_family = AF_INET;
        saddr.sin_addr.s_addr = inet_addr(SEND_TO_IP);
        saddr.sin_port = htons(SEND_TO_PORT);

        /* set up recv from address */
        if ((rd = socket(AF_INET,SOCK_DGRAM, IPPROTO_IP)) <  0) {
                perror("recv socket");
                exit(1);
        }
        memset(&raddr,0,sizeof raddr);
        raddr.sin_family = AF_INET;
        raddr.sin_addr.s_addr = htonl(INADDR_ANY);
        raddr.sin_port = htons(RECV_ON_PORT);
        if (bind(rd, (struct sockaddr *)&raddr, sizeof(struct sockaddr_in)) < 0) {
                perror("recv socket");
                exit(1);
        }

        imreq.imr_multiaddr.s_addr = inet_addr(RECV_ON_IP);
        imreq.imr_interface.s_addr = INADDR_ANY; // use DEFAULT interface
        // JOIN multicast group on default interface
        if(setsockopt(rd, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                (const void *)&imreq, sizeof(struct ip_mreq)) < 0) {
                        perror("recv socket");
                        exit(1);
        }

	int fromlen = sizeof raddr;
	for (i=0;i<4;i++) buf[i] = '0';
	while (recvfrom(rd, buf, 256, 0, (struct sockaddr *)&raddr, (socklen_t *)&fromlen) > 0)
	{
		if ((buf[0] == '0' || buf[0] == '1') &&
                    (buf[1] == '0' || buf[1] == '1') &&
                    (buf[2] == '0' || buf[2] == '1') &&
                    (buf[3] == '0' || buf[3] == '1') )
		{
			unsigned char io_out = 0;
			io_out |= 1 << 3; // Turn the ground on
			for (i=0;i<4;i++)
				if (buf[i] == '1')
					io_out |= 1 << (4 + i);
			outb(~io_out, base+4);
			usleep(500000);
			io_out = 0;
			outb(~io_out, base+4);
		}
		else if (buf[0] == '?')
		{
			unsigned char io_out = 0;
			unsigned char io_in;
			char outbuf[5];
			static char inc = 0;
			for (i=0;i<4;i++)
			{
				io_out = 1 << (4 + i);
				outb(~io_out, base+4);
				usleep(10000);
				io_in = inb(base+5) & (1 << 3);
				if (io_in)
					outbuf[i] = '1';
				else
					outbuf[i] = '0';
			}
			io_out = 0;
			outb(~io_out, base+4);
			inc++;
			outbuf[4] = inc;
			sendto(sd,outbuf,5,0,(struct sockaddr *)&saddr, sizeof saddr);
		}
	}
	return 0;
}
