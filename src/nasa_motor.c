#include <stdio.h>
#include <string.h> /* memset */
#include <unistd.h> /* usleep */
#include <stdlib.h> /* exit */
#include <sys/io.h> /* inb, outb */
#include <assert.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>

#define IO_BASE 0x300
#define NUM_IO_BOARDS 1
#define NUM_IO_BYTES_PER_BOARD 2
#define FIRST_GROUND_IO 0
#define NUM_GROUNDS 6
#define FIRST_POWER_IO 6
#define NUM_POWERS 1

#define SEND_TO_PORT 1368
#define SEND_TO_IP "224.0.0.100"
#define RECV_ON_PORT 1369
#define RECV_ON_IP "224.0.0.100"

int main(int argc, char **argv)
{
	unsigned int inc=0;
	int b,i,j,sd,rd;
	unsigned char io_out[NUM_IO_BYTES_PER_BOARD*NUM_IO_BOARDS];
	unsigned char io_in[NUM_IO_BYTES_PER_BOARD*NUM_IO_BOARDS];
	unsigned char motor_presence[NUM_GROUNDS][NUM_POWERS*NUM_IO_BOARDS];
	char buf[256];
       struct sockaddr_in saddr,raddr;
	struct ip_mreq imreq;

	unsigned long base = IO_BASE;
	if (ioperm(base, NUM_IO_BOARDS * 8, 1) == -1)
	{
		fprintf(stderr, "Unable to get I/O Permissions\n");
		exit(1);
	}

        /* set up send to address */
        if ((sd = socket(AF_INET,SOCK_DGRAM,0)) <  0) {
                perror("send socket");
                exit(1);
        }
        memset(&saddr,0,sizeof(saddr));
        saddr.sin_family = AF_INET;
        saddr.sin_addr.s_addr = inet_addr(SEND_TO_IP);
        saddr.sin_port = htons(SEND_TO_PORT);

        /* set up recv from address */
        if ((rd = socket(AF_INET,SOCK_DGRAM, IPPROTO_IP)) <  0) {
                perror("recv socket");
                exit(1);
        }
        memset(&raddr,0,sizeof raddr);
        raddr.sin_family = AF_INET;
        raddr.sin_addr.s_addr = htonl(INADDR_ANY);
        raddr.sin_port = htons(RECV_ON_PORT);
        if (bind(rd, (struct sockaddr *)&raddr, sizeof(struct sockaddr_in)) < 0) {
                perror("recv socket");
                exit(1);
        }

        imreq.imr_multiaddr.s_addr = inet_addr(RECV_ON_IP);
        imreq.imr_interface.s_addr = INADDR_ANY; // use DEFAULT interface
        // JOIN multicast group on default interface
        if(setsockopt(rd, IPPROTO_IP, IP_ADD_MEMBERSHIP,
                (const void *)&imreq, sizeof(struct ip_mreq)) < 0) {
                        perror("recv socket");
                        exit(1);
        }

	int fromlen = sizeof raddr;
	while (recvfrom(rd, buf, 256, 0, (struct sockaddr *)&raddr, (socklen_t *)&fromlen) > 0)
	{
		int t, tground, tpower, offsetFactor, indexOffset, ioOffset;

		if (!strncmp(buf, "0*:", 3))
		{
			memset(motor_presence, 1, sizeof(motor_presence));;
			for (b=0;b<NUM_IO_BOARDS;b++)
        		{
				int offset = b * 8;
				for (i=0;i<NUM_POWERS;i++)
				{
					memset(io_out, 0, sizeof(io_out));
					io_out[(FIRST_POWER_IO+i)/8] = 1 << (FIRST_POWER_IO+i) % 8;
					outb(~io_out[0], base+offset);
					outb(~io_out[1], base+offset+4);
					usleep(10000);
					io_in[0] = inb(base+offset+1);
					io_in[1] = inb(base+offset+5);
					for (j=0;j<NUM_GROUNDS;j++)
					if (((io_in[(FIRST_GROUND_IO+j)/8]) & (1 << (FIRST_GROUND_IO+j) % 8)) == 0)
					{
						motor_presence[j][(b*NUM_POWERS) + i] = 0;
					}
					outb(~0, base+offset);
					outb(~0, base+offset+4);
				}
			}
			t = 0;
			for (i=0;i<NUM_GROUNDS;i++)
			{
				for (b=0;b<NUM_IO_BOARDS;b++)
				{
					for (j=0;j<NUM_POWERS;j++)
					{
						buf[t++] = motor_presence[i][(b*NUM_POWERS)+j] + '0';
					}
				}
			}
			buf[t++] = inc % 256;
			buf[t] = 0;
			fprintf(stderr, "inc:%d, result:%d\n", inc, sendto(sd,buf,8,0,(struct sockaddr *)&saddr, sizeof saddr));
			inc = inc + 1;
		}
		else if (!strncmp(buf, "1::", 3))
		{
			memset(io_out, 0, sizeof(io_out));
			tground = 1;
			tpower = 1;
			offsetFactor = tpower/NUM_POWERS;
			tpower = tpower%NUM_POWERS;
			indexOffset = 2 * offsetFactor;
			ioOffset = 8 * offsetFactor;
			//io_out[indexOffset + (FIRST_POWER_IO+tpower)/8]   |= 1 << (FIRST_POWER_IO+tpower)%8;
			//io_out[indexOffset + (FIRST_GROUND_IO+tground)/8] |= 1 << (FIRST_GROUND_IO+tground)%8;
			while (tground <= 6)
			{
				tground++;
				offsetFactor = tpower/NUM_POWERS;
				tpower = tpower%NUM_POWERS;
				indexOffset = 2 * offsetFactor;
				ioOffset = 8 * offsetFactor;
				io_out[indexOffset + (FIRST_POWER_IO+tpower)/8]   |= 1 << (FIRST_POWER_IO+tpower)%8;
				io_out[indexOffset + (FIRST_GROUND_IO+tground)/8] |= 1 << (FIRST_GROUND_IO+tground)%8;
			}
			outb(~io_out[0], base);
			outb(~io_out[1], base+4);
			usleep(1000);

			memset(motor_presence, 1, sizeof(motor_presence));;
			for (b=0;b<NUM_IO_BOARDS;b++)
        		{
				int offset = b * 8;
				for (i=0;i<NUM_POWERS;i++)
				{
					io_in[0] = inb(base+offset+1);
					io_in[1] = inb(base+offset+5);
					for (j=0;j<NUM_GROUNDS;j++)
					if (((io_in[(FIRST_GROUND_IO+j)/8]) & (1 << (FIRST_GROUND_IO+j) % 8)) == 0)
					{
						motor_presence[j][(b*NUM_POWERS) + i] = 0;
					}
				}
			}
			t = 0;
			for (i=0;i<NUM_GROUNDS;i++)
			{
				for (b=0;b<NUM_IO_BOARDS;b++)
				{
					for (j=0;j<NUM_POWERS;j++)
					{
						buf[t++] = motor_presence[i][(b*NUM_POWERS)+j] + '0';
					}
				}
			}
			buf[t++] = inc % 256;
			buf[t] = 0;
			fprintf(stderr, "inc:%d, result:%d\n", inc, sendto(sd,buf,8,0,(struct sockaddr *)&saddr, sizeof saddr));
			inc = inc + 1;
		}
		else if (!strncmp(buf, "0::", 3))
		{
			memset(io_out, 0, sizeof(io_out));
			outb(~io_out[0], base);
			outb(~io_out[1], base+4);
		}
	}
	return 0;
}
