#include <stdio.h>
#include <string.h> /* memset */
#include <unistd.h> /* usleep */
#include <stdlib.h> /* exit */
#include <sys/io.h> /* inb, outb */
#include <assert.h>
#include <sched.h> /* realtime scheduling */
#include <sys/times.h> /* times */
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define IO_BASE 0x300
#define NUM_IO_BOARDS 1
#define NUM_IO_BYTES_PER_BOARD 2
#define FIRST_GROUND_IO 0
#define NUM_GROUNDS 6
#define FIRST_POWER_IO 6
#define NUM_POWERS 10

#define MAX_LINE_LENGTH 64
#define MAX_FILE_LENGTH 256

static unsigned char motor_presence[NUM_GROUNDS][NUM_POWERS*NUM_IO_BOARDS];

static clock_t times_wrapper(void)
{
       clock_t ret;
       int save_errno = errno;
       errno   = 0;
       ret     = times(NULL);
       if (errno != 0) {
               ret = (clock_t) (-errno);
       }
       errno = save_errno;
       return ret;
}


static float get_current_voltage(void)
{
        static int fd=-1;
        static int send_req = 1;
        char linebuf[8]; /* 0.00V\r\n\0 */
        float v;
        int numread = 0;
        linebuf[7] = 0;
        if (fd == -1)
        {
                fd=open("/dev/ttyUSB0", O_RDWR);
                if (fd == -1)
                {
                        fprintf(stderr, "Cannot open /dev/ttyUSB0\n");
                        exit(1);
                }
        }
        if (send_req)
        {
                send_req = 0;
                write(fd, "Z", 1);
                return 0;
        }
        else
        {
                send_req = 1;
                while (numread < 7)
                {
                        int ret = read(fd, linebuf + numread, 7 - numread);
                        if (ret == -1)
                        {
                                perror("Reading from /dev/ttyUSB0");
                                exit(1);
                        }
                        else
                        {
                                numread += ret;
                        }
                }
                if (sscanf(linebuf, "%f", &v) == 1)
                {
                        return v;
                }
                else
                {
                        fprintf(stderr, "Cannot understand data :%s: from /dev/ttyUSB0\n", linebuf);
                        exit(1);
                }
        }
}

int main(int argc, char **argv)
{
	FILE *fp;
	int b,i,j,line_num;
	unsigned char io_out[NUM_IO_BYTES_PER_BOARD*NUM_IO_BOARDS];
	unsigned char io_in[NUM_IO_BYTES_PER_BOARD*NUM_IO_BOARDS];
	char linebuf[MAX_LINE_LENGTH];
	int ft_grounds[MAX_FILE_LENGTH];
	int ft_powers[MAX_FILE_LENGTH];
	long ft_times[MAX_FILE_LENGTH];
	struct sched_param sched_param;
	clock_t current_time;

	int num_lines_in_ft = 0;
	unsigned long base = IO_BASE;
	int have_perms = 1;
	long jiffies_from_start = 0;

	sched_param.sched_priority = 5;
	if (ioperm(base, NUM_IO_BOARDS * 8, 1) == -1)
	{
		fprintf(stderr, "Unable to get I/O Permissions\n");
		have_perms = 0;
	}
	if (sched_setscheduler(0, SCHED_RR, &sched_param) == -1)
	{
		fprintf(stderr, "Unable to get Realtime Permissions\n");
	}

	if (argc != 2)
	{
		fprintf(stderr, "Wrong usage\n");
		exit(1);
	}
	fp = fopen(argv[1], "r");
	if (!fp)
	{
		fprintf(stderr, "Bad File\n");
		exit(1);
	}
	while (fgets(linebuf, MAX_LINE_LENGTH, fp))
	{
		int a;
		char *c;
		if (linebuf[strlen(linebuf)-1]==10) linebuf[strlen(linebuf)-1]=0;
		if (num_lines_in_ft == MAX_FILE_LENGTH)
		{
			fprintf(stderr, "Ignoring everything after line %d\n", MAX_FILE_LENGTH);
			break;
		}
		if (sscanf(linebuf, "%d,%d,%d.", ft_grounds+num_lines_in_ft,
					         ft_powers+num_lines_in_ft,
                                                 &a ) != 3)
		{
			fprintf(stderr, "Bad line format on line %d\n", num_lines_in_ft+1);
			exit(1);
		}
		ft_times[num_lines_in_ft] = a * 1000;
		c = strchr(linebuf, '.');
		a = strlen(++c);
		while (a>3) a--, c[a]=0;
		while (a<3) c[a]='0', a++, c[a]=0;
		ft_times[num_lines_in_ft] += atoi(c);
		if (ft_grounds[num_lines_in_ft] < 1 || ft_grounds[num_lines_in_ft] > NUM_GROUNDS )
		{
			fprintf(stderr, "Bad ring on line %d\n", num_lines_in_ft+1);
			exit(1);
		}
		if (ft_powers[num_lines_in_ft] < 1 || ft_powers[num_lines_in_ft] > NUM_POWERS * NUM_IO_BOARDS )
		{
			fprintf(stderr, "Bad motor on line %d\n", num_lines_in_ft+1);
			exit(1);
		}
		if (num_lines_in_ft > 0 && ft_times[num_lines_in_ft] < ft_times[num_lines_in_ft-1])
		{
			fprintf(stderr, "Time out of order on line %d\n", num_lines_in_ft+1);
			exit(1);
		}
		num_lines_in_ft++;
	}
	fclose(fp);
        for (i=0;i<num_lines_in_ft;i++)
        {
                for (j=i+1;j<num_lines_in_ft;j++)
                {
                        if (ft_powers[i] == ft_powers[j] && ft_grounds[i] == ft_grounds[j])
                        {
                                fprintf(stderr, "Duplicate motor on line %d.\n", j+1);
                                exit(1);
                        }
                }
        }
	memset(motor_presence, 1, sizeof(motor_presence));;
        for (b=0;b<NUM_IO_BOARDS && have_perms;b++)
        {
                int offset = b * 8;
                for (i=0;i<NUM_POWERS;i++)
                {
                        memset(io_out, 0, sizeof(io_out));
                        io_out[(FIRST_POWER_IO+i)/8] = 1 << (FIRST_POWER_IO+i) % 8;
                        outb(~io_out[0], base+offset);
                        outb(~io_out[1], base+offset+4);
                        usleep(10000);
                        io_in[0] = inb(base+offset+1);
                        io_in[1] = inb(base+offset+5);
                        for (j=0;j<NUM_GROUNDS;j++)
                                if (((io_in[(FIRST_GROUND_IO+j)/8]) & (1 << (FIRST_GROUND_IO+j) % 8)) == 0)
                                {
                                        motor_presence[j][(b*NUM_POWERS) + i] = 0;
                                        //fprintf(stderr, "Motor missing at %d %d\n", j+1, (b*NUM_POWERS) + i+1);
                                }
                        outb(~0, base+offset);
                        outb(~0, base+offset+4);
                }
        }
	fprintf(stdout, "File:\n");
	for (line_num=0;line_num<num_lines_in_ft;line_num++)
	{
		fprintf(stdout, "%d,%d,%ld.%03ld\n", ft_grounds[line_num], ft_powers[line_num], ft_times[line_num] / 1000, ft_times[line_num] % 1000);
	}
        fprintf(stdout, "Motors:\n");
        for (i=0;i<NUM_GROUNDS;i++)
        {
                for (b=0;b<NUM_IO_BOARDS;b++)
                {
                        for (j=0;j<NUM_POWERS;j++)
                        {
                                fprintf(stderr, "%d", motor_presence[i][(b*NUM_POWERS)+j]);
                        }
                }
                fprintf(stderr, "\n");
        }

	fprintf(stdout, "Hit enter to continue, ^C to abort->");
	fflush(stdout);
	fgets(linebuf, MAX_LINE_LENGTH, stdin);
	fprintf(stdout, "Firing in...");
	for (i=5;i>0;i--)
	{
		fprintf(stdout, "%d...", i);
		fflush(stdout);
		sleep(1);
	}
	fprintf(stdout, "\n");

	current_time = times_wrapper();
	while (current_time == times_wrapper());
	for (line_num=0;line_num<num_lines_in_ft && have_perms;line_num++)
	{
		int tground, tpower, offsetFactor, indexOffset, ioOffset;
		while (ft_times[line_num]/10 > jiffies_from_start)
		{
			current_time = times_wrapper();
			while (current_time == times_wrapper());
			jiffies_from_start += 1;
		}
		memset(io_out, 0, sizeof(io_out));
		tground = ft_grounds[line_num]-1;
		tpower = ft_powers[line_num]-1;
		offsetFactor = tpower/NUM_POWERS;
		tpower = tpower%NUM_POWERS;
		indexOffset = 2 * offsetFactor;
		ioOffset = 8 * offsetFactor;
		io_out[indexOffset + (FIRST_POWER_IO+tpower)/8]   |= 1 << (FIRST_POWER_IO+tpower)%8;
		io_out[indexOffset + (FIRST_GROUND_IO+tground)/8] |= 1 << (FIRST_GROUND_IO+tground)%8;
		while ((line_num+1) < num_lines_in_ft && ft_times[line_num+1] == ft_times[line_num])
		{
			line_num++;
			tground = ft_grounds[line_num]-1;
			tpower = ft_powers[line_num]-1;
			offsetFactor = tpower/NUM_POWERS;
			tpower = tpower%NUM_POWERS;
			indexOffset = 2 * offsetFactor;
			ioOffset = 8 * offsetFactor;
			io_out[indexOffset + (FIRST_POWER_IO+tpower)/8]   |= 1 << (FIRST_POWER_IO+tpower)%8;
			io_out[indexOffset + (FIRST_GROUND_IO+tground)/8] |= 1 << (FIRST_GROUND_IO+tground)%8;
		}
		outb(~io_out[0], base);
		outb(~io_out[1], base+4);
#if NUM_IO_BOARDS > 1
		outb(~io_out[2], base+8);
		outb(~io_out[3], base+12);
#endif
		current_time = times_wrapper();
		while (current_time == times_wrapper());
		jiffies_from_start += 1;
		outb(~0, base);
		outb(~0, base+4);
#if NUM_IO_BOARDS > 1
		outb(~0, base+8);
                outb(~0, base+12);
#endif
	}
	return 0;
}
