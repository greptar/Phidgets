#include <stdio.h>
#include <unistd.h>     /* exit */
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>
#include <string.h>
#include <sys/time.h>

#include "sensor-packet.h"

static char *sendto_address = "127.0.0.1";
static int sendto_port=1027;
static int sleepSecs = 0;
static int socketFD;
static struct sockaddr_in socketADDR;
static SensorPacketHeader packet;

static int cancel = 0;
static void sighandle(int signum)
{
        cancel = 1;
}

int main(int argc, char *argv[])
{
	int bad_command_line=0;

	signal(SIGINT, &sighandle);
        if (argc != 3)
        {
                bad_command_line++;
        }
        else
        {
                sendto_address = argv[1];
                if (inet_addr(sendto_address) == INADDR_NONE)
                        fprintf(stderr, "Bad IP Address\n"), bad_command_line++;
		sleepSecs = atoi(argv[2]);
		if (sleepSecs < 1 || sleepSecs > 60)
			fprintf(stderr, "Bad Sleep Time.  Must be 1 to 60.\n"), bad_command_line++;
        }
        if (bad_command_line)
        {
                fprintf(stderr, "Usage: %s <IP> <seconds per trigger>\n", argv[0]);
                return 1;
        }

        if ((socketFD = socket(AF_INET,SOCK_DGRAM,0)) <  0)
        {
                perror("Creating UDP Socket");
                exit(1);
        }
        memset(&socketADDR,0,sizeof(socketADDR));
        socketADDR.sin_family = AF_INET;
        socketADDR.sin_addr.s_addr = inet_addr(sendto_address);
        socketADDR.sin_port = htons(sendto_port);

        packet.packet_type = SP_TYPE_PULSE;
        packet.packet_cmd = SP_CMD_DATA;
        packet.packet_num = 0;
        packet.num_buffered_packets = 1;
        packet.deviceClass = 0;
        packet.dataLength = 0;

	while(!cancel) 
	{
		struct timeval tv;
		gettimeofday(&tv, NULL);
		packet.time_sec = tv.tv_sec;
		packet.time_usec = tv.tv_usec;
		sendto(socketFD, &packet, sizeof(SensorPacketHeader), 0, (struct sockaddr *)&socketADDR, sizeof(socketADDR));
		sleep(sleepSecs);
	}
	return 0;
}
