#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <string.h>
#include <values.h>
#include <fcntl.h>
#include <termios.h>

#include "sensor-manager.h"

static int cancel = 0;
static void sighandle(int signum)
{
    cancel = 1;
}

static FILE *lf = NULL;
static char **units;

static float BridgeEU(float in, void *parameters)
{
    double val = (in/1000.0);
    double sF = *(double*)parameters;
    return (4*val/(sF*(1+2*val))) * 1000000;
}
static float CtoF(float in, void *unused)
{
    return in * 1.8 + 32;
}
#if 0
static float DisplacementEU(float in, void *parameters)
{
    double S = 0.026252;
    double O = -330;
    return S*(in+O);
}
#endif
static void printData(FILE *fp, struct timeval timestamp, float *values, int count)
{
    int i;
    fprintf(fp, "%d.%06d s ", (int)timestamp.tv_sec, (int)timestamp.tv_usec);
    for (i=0; i<count; i++)
        if (values[i] == MAXFLOAT)
            fprintf(fp, "(missing) %s ", units[i]);
        else
            fprintf(fp, "%f %s ", values[i], units[i]);
    fprintf(fp, "\n");

}

static void callBack(struct timeval timestamp, float *values, int count)
{
    printData(stdout, timestamp, values, count);
    if (lf)
        printData(lf, timestamp, values, count);
}

int main(int argc, char **argv)
{
    SensorManager sm;
    char **names = NULL;
    int i, count=0, kbd;
    char *logFilename;
    struct termios orig,now;

    signal(SIGINT, &sighandle);
    signal(SIGTERM, &sighandle);
    setvbuf(stdout, NULL, _IONBF ,0);
    tcgetattr(0, &orig);
    now=orig;
    now.c_lflag &= ~(ISIG|ICANON|ECHO);
    now.c_cc[VMIN]=1;
    now.c_cc[VTIME]=2;
    tcsetattr(0, TCSANOW, &now);
    sm = SensorManager_create(1027);
    if (!sm) exit(1);

    if (argc == 2)
        logFilename = argv[1];
    else
        logFilename = "/tmp/data.txt";

    fprintf(stdout, "Press 'h' for help.\n");
    fcntl(0, F_SETFL, O_NONBLOCK);
    while (!cancel)
    {
        usleep(10000);
        kbd = getchar();
        if (kbd < 1) continue;
        if (kbd == 'h')
        {
            fprintf(stdout, "g - getNamesAndUnits (Resets, Lists Current Sensors)\n");
            fprintf(stdout, "s - setCallback (Starts data)\n");
            fprintf(stdout, "c - clearCallback (Stops data)\n");
            fprintf(stdout, "n - Null Sensors\n");
            fprintf(stdout, "l - Log data to %s\n", logFilename);
            fprintf(stdout, "f - Flood data ignoring trigger\n");
            fprintf(stdout, "1 - Hz\n");
            fprintf(stdout, "2 - Hz\n");
            fprintf(stdout, "4 - Hz\n");
            fprintf(stdout, "5 - Hz\n");
            fprintf(stdout, "0 - 10 Hz\n");
            fprintf(stdout, "e - Turn on bridge engineering units\n");
            fprintf(stdout, "t - Change Thermocouple Types on channels 2 and 3 to K\n");
            fprintf(stdout, "q - Quit\n");
        }
        else if (kbd == 'g')
        {
            SensorManager_clearCallback(sm);
            SensorManager_getNamesAndUnits(sm, &names, &units, &count);
            fprintf(stdout, "Current Sensors are:\n");
            for (i=0; i<count; i++)
                fprintf(stdout, "%s %s\n", names[i], units[i]);
        }
        else if (kbd == 's')
        {
            if (!names)
                fprintf(stdout, "Need to get the names first.\n");
            else
            {
                SensorManager_setCallback(sm, callBack);
                fprintf(stdout, "Callback Set.\n");
            }
        }
        else if (kbd == 'c')
        {
            SensorManager_clearCallback(sm);
            fprintf(stdout, "Callback Cleared.\n");
        }
        else if (kbd == 'n')
        {
            for (i=0; i<count; i++)
                SensorManager_nullSensor(sm, i);
            if (!count) fprintf(stdout, "Need to get the names first.\n");
        }
        else if (kbd == 'l')
        {
            lf = fopen(logFilename, "w");
            fprintf(stdout, "Logging to %s.\n", logFilename);
        }
        else if (kbd == 'f')
        {
            SensorManager_floodCallback(sm);
            fprintf(stdout, "Trigger pulses ignored.  Data coming as seen.\n");
        }
        else if (kbd == '1')
        {
            for (i=0; i<count; i++)
                SensorManager_setRate(sm, i, 1);
            if (!count) fprintf(stdout, "Need to get the names first.\n");
        }
        else if (kbd == '2')
        {
            for (i=0; i<count; i++)
                SensorManager_setRate(sm, i, 2);
            if (!count) fprintf(stdout, "Need to get the names first.\n");
        }
        else if (kbd == '4')
        {
            for (i=0; i<count; i++)
                SensorManager_setRate(sm, i, 4);
            if (!count) fprintf(stdout, "Need to get the names first.\n");
        }
        else if (kbd == '5')
        {
            for (i=0; i<count; i++)
                SensorManager_setRate(sm, i, 5);
            if (!count) fprintf(stdout, "Need to get the names first.\n");
        }
        else if (kbd == '0')
        {
            for (i=0; i<count; i++)
                SensorManager_setRate(sm, i, 10);
            if (!count) fprintf(stdout, "Need to get the names first.\n");
        }
        else if (kbd == 'e')
        {
            double sF = 2.11; // Needs to be looked up per gage
            for (i=0; i<count; i++)
            {
                if (!strcmp(units[i], "mV/V"))
                    SensorManager_setEUfunction(sm, i, "me", BridgeEU, &sF);
                if (!strcmp(units[i], "degC"))
                    SensorManager_setEUfunction(sm, i, "degF", CtoF, NULL);
            }
            if (!count)
                fprintf(stdout, "Need to get the names first.\n");
        }
        else if (kbd == 't')
        {
            if (!count) fprintf(stdout, "Need to get the names first.\n");
            for (i=0; i<count; i++)
            {
                char c = names[i][strlen(names[i])-1];
                if (!strncmp(units[i], "deg", 3) && (c == '2' || c == '3'))
                    SensorManager_setThermocoupleType(sm, i, 'K');
            }
        }
        else if (kbd == 'q' || kbd == 3)
        {
            cancel = 1;
        }
        else if (kbd == 10 || kbd == 13)
        {
            fprintf(stdout, "\n");
        }
        else
        {
            fprintf(stdout, "Unknown command.  Press 'h' for help.\n");
        }
    }
    tcsetattr(0, TCSANOW, &orig);

    SensorManager_destroy(sm);
    if (lf) fclose(lf);

    fprintf(stdout, "Exiting %s cleanly\n", argv[0]);
    return 0;
}
