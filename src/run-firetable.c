#include <stdio.h>
#include <string.h> /* memset */
#include <unistd.h> /* usleep */
#include <stdlib.h> /* exit */
#include <sys/io.h> /* inb, outb */
#include <assert.h>
#include <sched.h> /* realtime scheduling */

#define NUM_IO_BOARDS 1 /* Can have up to 3 */
#define FIRST_RING_IO 0
#define NUM_RINGS 4
#define FIRST_MOTOR_IO 4
#define MOTORS_PER_RING 12
#define MAX_FT_ENTRIES 64
#define MAX_LINE_LENGTH 64
#define IO_BASE 0x300
#define DEFAULT_USECS_TO_FIRE 10000

#define NUM_IO_PORTS NUM_IO_BOARDS * 16
#define NUM_IO_BYTES NUM_IO_BOARDS * 2

#if MOTORS_PER_RING + NUM_RINGS > NUM_IO_PORTS
#error Not enough IO Ports for Matrix
#endif

#if FIRST_RING_IO == FIRST_MOTOR_IO
#error FIRST_RING_IO == FIRST_MOTOR_IO
#endif

#if FIRST_RING_IO < FIRST_MOTOR_IO
#if FIRST_RING_IO + NUM_RINGS > FIRST_MOTOR_IO
#error FIRST_MOTOR_IO too low given FIRST_RING_IO and NUM_RINGS
#endif
#else
#if FIRST_MOTOR_IO + MOTORS_PER_RING > FIRST_RING_IO
#error FIRST_RING_IO too low given FIRST_MOTOR_IO and MOTORS_PER_RING
#endif
#endif

int main(int argc, char **argv)
{
	FILE *fp;
	int i,j,line_num;
	long curtime;
	unsigned char io_out[NUM_IO_BYTES];
	unsigned char io_in[NUM_IO_BYTES];
	int io_of_ring[NUM_RINGS + 1]; /* Needs to be one based */
	int io_of_motor[MOTORS_PER_RING + 1]; /* Needs to be one based */
	char linebuf[MAX_LINE_LENGTH];
	int ft_rings[MAX_FT_ENTRIES];
	int ft_motors[MAX_FT_ENTRIES];
	long ft_times[MAX_FT_ENTRIES];
	struct sched_param sched_param;

	int usleeptime = DEFAULT_USECS_TO_FIRE;
	int num_lines_in_ft = 0;
	unsigned long base = IO_BASE;
	int have_perms = 1;

	sched_param.sched_priority = 5;
	assert(sizeof(curtime)==8);
	if (ioperm(base, NUM_IO_BOARDS * 8, 1) == -1)
	{
		fprintf(stderr, "Unable to get I/O Permissions\n");
		have_perms = 0;
	}
	if (sched_setscheduler(0, SCHED_RR, &sched_param) == -1)
	{
		fprintf(stderr, "Unable to get Realtime Permissions\n");
	}

	/* For now a simple in order mapping is used.  In case
	   mappings get more complex, hold them in arrays */
	for (i=1;i<=NUM_RINGS;i++)
		io_of_ring[i] = FIRST_RING_IO + i - 1;
	for (j=1;j<=MOTORS_PER_RING;j++)
		io_of_motor[j] = FIRST_MOTOR_IO + j - 1;

	if (argc != 2 && argc != 3)
	{
		fprintf(stderr, "Wrong usage\n");
		exit(1);
	}
	fp = fopen(argv[1], "r");
	if (!fp)
	{
		fprintf(stderr, "Bad File\n");
		exit(1);
	}
	if (argc == 3)
	{
		if (sscanf(argv[2], "%d", &usleeptime) != 1)
		{
			fprintf(stderr, "Bad sleep time\n");
			exit(1);
		}
		usleeptime *= 1000;
	}
	while (fgets(linebuf, MAX_LINE_LENGTH, fp))
	{
		int a;
		char *c;
		if (linebuf[strlen(linebuf)-1]==10) linebuf[strlen(linebuf)-1]=0;
		if (num_lines_in_ft == MAX_FT_ENTRIES)
		{
			fprintf(stderr, "Ignoring everything after line %d\n", MAX_FT_ENTRIES);
			break;
		}
		if (sscanf(linebuf, "%d,%d,%d.", ft_rings+num_lines_in_ft,
					         ft_motors+num_lines_in_ft,
                                                 &a ) != 3)
		{
			fprintf(stderr, "Bad line format on line %d\n", num_lines_in_ft+1);
			exit(1);
		}
		ft_times[num_lines_in_ft] = a * 1000;
		c = strchr(linebuf, '.');
		a = strlen(++c);
		while (a>3) a--, c[a]=0;
		while (a<3) c[a]='0', a++, c[a]=0;
		ft_times[num_lines_in_ft] += atoi(c);
		if (ft_rings[num_lines_in_ft] < 1 || ft_rings[num_lines_in_ft] > NUM_RINGS )
		{
			fprintf(stderr, "Bad ring on line %d\n", num_lines_in_ft+1);
			exit(1);
		}
		if (ft_motors[num_lines_in_ft] < 1 || ft_motors[num_lines_in_ft] > MOTORS_PER_RING )
		{
			fprintf(stderr, "Bad motor on line %d\n", num_lines_in_ft+1);
			exit(1);
		}
		if (num_lines_in_ft > 0 && ft_times[num_lines_in_ft] < ft_times[num_lines_in_ft-1])
		{
			fprintf(stderr, "Time out of order on line %d\n", num_lines_in_ft+1);
			exit(1);
		}
		if (io_of_ring[ft_rings[num_lines_in_ft]] < 0 ||
		    io_of_ring[ft_rings[num_lines_in_ft]] > (NUM_IO_PORTS-1))
		{
			fprintf(stderr, "Ring on line %d maps to bad IOPORT\n", num_lines_in_ft+1);
			exit(1);
		}
		if (io_of_motor[ft_motors[num_lines_in_ft]] < 0 ||
		    io_of_motor[ft_motors[num_lines_in_ft]] > (NUM_IO_PORTS-1))
		{
			fprintf(stderr, "Motor on line %d maps to bad IOPORT\n", num_lines_in_ft+1);
			exit(1);
		}
		num_lines_in_ft++;
	}
	fclose(fp);
        for (i=0;i<num_lines_in_ft;i++)
        {
                for (j=i+1;j<num_lines_in_ft;j++)
                {
                        if (ft_motors[i] == ft_motors[j] && ft_rings[i] == ft_rings[j])
                        {
                                fprintf(stderr, "Duplicate motor on line %d.\n", j+1);
                                exit(1);
                        }
                }
        }
        for (i=1;i<=MOTORS_PER_RING && have_perms;i++)
        {
                memset(io_out, 0, sizeof(io_out));
                io_out[io_of_motor[i]/8] = 1 << io_of_motor[i] % 8;
                outb(~io_out[0], base);
                outb(~io_out[1], base+4);
		#if NUM_IO_BOARDS > 1
                outb(~io_out[2], base+8);
                outb(~io_out[3], base+12);
		#endif
		#if NUM_IO_BOARDS > 2
                outb(~io_out[4], base+16);
                outb(~io_out[5], base+20);
		#endif
                usleep(1000);
                io_in[0] = inb(base+1);
                io_in[1] = inb(base+5);
		#if NUM_IO_BOARDS > 1
                io_in[2] = inb(base+9);
                io_in[3] = inb(base+13);
		#endif
		#if NUM_IO_BOARDS > 2
                io_in[4] = inb(base+17);
                io_in[5] = inb(base+21);
		#endif
                for (j=1;j<=NUM_RINGS;j++)
                        if (((io_in[io_of_ring[j]/8]) & (1 << io_of_ring[j] % 8)) == 0)
				for (line_num=0;line_num<num_lines_in_ft;line_num++)
					if ((ft_rings[line_num]==j) && (ft_motors[line_num]==i))
                               			fprintf(stderr, "Motor missing at %d %d\n", j, i);
                outb(~0, base);
                outb(~0, base+4);
		#if NUM_IO_BOARDS > 1
                outb(~0, base+8);
                outb(~0, base+12);
		#endif
                #if NUM_IO_BOARDS > 2
                outb(~0, base+16);
                outb(~0, base+20);
		#endif
        }

	fprintf(stdout, "Firing table:\n");
	curtime = 0;
	for (line_num=0;line_num<num_lines_in_ft;line_num++)
	{
		int num_ft_lines_this_round = 1;
		int num_rings_this_round = 1;
		int num_motors_this_round = 1;
                curtime = ft_times[line_num];
                memset(io_out, 0, sizeof(io_out));
                io_out[io_of_ring[ft_rings[line_num]]/8] |= 1 << io_of_ring[ft_rings[line_num]] % 8;
                io_out[io_of_motor[ft_motors[line_num]]/8] |= 1 << io_of_motor[ft_motors[line_num]] % 8;
		fprintf(stdout, "%d,%d,%ld.%03ld\n", ft_rings[line_num], ft_motors[line_num], ft_times[line_num] / 1000, ft_times[line_num] % 1000);
                while ((line_num+1) < num_lines_in_ft && ft_times[line_num+1] == ft_times[line_num])
                {
                        line_num++;
			num_ft_lines_this_round++;
			if ((io_out[io_of_ring[ft_rings[line_num]]/8] & (1 << io_of_ring[ft_rings[line_num]] % 8)) == 0)
			{
                        	io_out[io_of_ring[ft_rings[line_num]]/8] |= 1 << io_of_ring[ft_rings[line_num]] % 8;
				num_rings_this_round++;
			}
			if ((io_out[io_of_motor[ft_motors[line_num]]/8] & (1 << io_of_motor[ft_motors[line_num]] % 8)) == 0)
			{
                        	io_out[io_of_motor[ft_motors[line_num]]/8] |= 1 << io_of_motor[ft_motors[line_num]] % 8;
				num_motors_this_round++;
			}
			fprintf(stdout, "%d,%d,%ld.%03ld\n", ft_rings[line_num], ft_motors[line_num], ft_times[line_num] / 1000, ft_times[line_num] % 1000);
                }
		/* Now check to see if num bits in rings * num bits in motors > num_ft_lines_this_round */
		/* To keep unmentioned motors from firing */
		if (num_rings_this_round * num_motors_this_round > num_ft_lines_this_round)
		{
			fprintf(stderr, "Will not do implicit motor firings.  Please list all motors to be fired.\n");
			exit(1);
		}
		curtime += usleeptime / 1000 ;
	}

	fprintf(stdout, "Hit enter to continue, ^C to abort->");
	fflush(stdout);
	fgets(linebuf, MAX_LINE_LENGTH, stdin);
	fprintf(stdout, "Firing in...");
	for (i=5;i>0;i--)
	{
		fprintf(stdout, "%d...", i);
		fflush(stdout);
		sleep(1);
	}
	fprintf(stdout, "\n");

	curtime = 0;
	for (line_num=0;line_num<num_lines_in_ft && have_perms;line_num++)
	{
		if (ft_times[line_num] > curtime)
		{
			usleep((ft_times[line_num] - curtime)*1000);
		}
		curtime = ft_times[line_num];
		memset(io_out, 0, sizeof(io_out));
		io_out[io_of_ring[ft_rings[line_num]]/8] |= 1 << io_of_ring[ft_rings[line_num]] % 8;
		io_out[io_of_motor[ft_motors[line_num]]/8] |= 1 << io_of_motor[ft_motors[line_num]] % 8;
		while ((line_num+1) < num_lines_in_ft && ft_times[line_num+1] == ft_times[line_num])
		{
			line_num++;
			io_out[io_of_ring[ft_rings[line_num]]/8] |= 1 << io_of_ring[ft_rings[line_num]] % 8;
			io_out[io_of_motor[ft_motors[line_num]]/8] |= 1 << io_of_motor[ft_motors[line_num]] % 8;
		}
		outb(~io_out[0], base);
		outb(~io_out[1], base+4);
		#if NUM_IO_BOARDS > 1
		outb(~io_out[2], base+8);
		outb(~io_out[3], base+12);
		#endif
		#if NUM_IO_BOARDS > 2
		outb(~io_out[4], base+16);
		outb(~io_out[5], base+20);
		#endif
		usleep(usleeptime);
		curtime += usleeptime / 1000 ;
		outb(~0, base);
		outb(~0, base+4);
		#if NUM_IO_BOARDS > 1
		outb(~0, base+8);
                outb(~0, base+12);
		#endif
		#if NUM_IO_BOARDS > 2
                outb(~0, base+16);
                outb(~0, base+20);
		#endif
	}
	return 0;
}
